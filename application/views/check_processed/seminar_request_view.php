<?php if(
    $user['position_id'] == 8 &&
    $sbrf[0]['is_check_processed'] == 1
):
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>check_processed">Check Processed</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $sbrf[0]['sbrf_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="ui padded grid" style="background-color:#FFFFFF;">
    <div class="row">
        <div class="eight wide column">
            <div class="ui dividing header">Sales Budget Request - Seminar</div>
        </div>
    </div>
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui blue ribbon label">seminar request info</div>
        </div>
    </div>
    <?php foreach($sbrf as $sbrf_key => $sbrf_request): ?>
    <div class="row">
        <div class="font-bolder four wide column">Control No</div>
        <div class="four wide column sbrf-no"><?php echo $sbrf_request['sbrf_no']; ?></div>
        <div class="font-bolder four wide column">Date Created</div>
        <?php $formatted_date = date_create($sbrf_request['date_created']); ?>
        <div class="four wide column"><?php echo date_format($formatted_date,'m/d/Y --- g:i:s A'); ?></div>
    </div>
    <div class="row">
        <div class="font-bolder four wide column">EMA Code</div>
        <div class="four wide column"><?php echo $sbrf_request['ema_code']; ?></div>
        <div class="font-bolder four wide column">EMA Name</div>
        <div class="four wide column"><?php echo $sbrf_request['ema_name']; ?></div>

        <div class="font-bolder four wide column">RSM</div>
        <div class="four wide column"><?php echo $rsm['rsm_name']; ?></div>

        <div class="font-bolder four wide column">Region</div>
        <div class="four wide column"><?php echo $region['region']; ?></div>

    </div>

    <div class="row">        
        <?php if($sbrf_request['erf_file'] != '') : ?>
        <div class="font-bolder four wide column">Attached ERF Document file:</div>
        <div class="four wide column"><a href='<?php echo base_url();?>assets/erf_files/<?php echo $sbrf_request['erf_file'];?>' download><i class="file icon"></i><?php echo $sbrf_request['erf_file'];?></a></div>
        <?php endif; ?>
    </div>

    <?php endforeach; ?>
    <div class="row">
        <div class="sixteen wide column">
            <table class="ui celled table">
                <thead>
                    <th>school code</th>
                    <th>school name</th>
                    <th>donation amount</th>
                    <th>evaluate/process</th>
                    <th>payee</th>
                    <th>designation</th>
                    <th>remarks</th>
                    <th>SOA file</th>
                </thead>
                <tbody>
                <?php foreach($sbrf_item as $sbrf_item_key => $sbrf_items) : ?>
                    <tr>
                        <td><?php echo $sbrf_items['customer_code']; ?></td>
                        <td><?php echo $sbrf_items['customer_name']; ?></td>
                        <td class="right aligned"><?php echo number_format($sbrf_items['seminar'],2); ?></td>

                        <?php if($sbrf_items['evaluate_process'] == 1) : ?>
                            <td>process</td>
                        <?php endif; ?>

                        <?php if($sbrf_items['evaluate_process'] == 0) : ?>
                            <td>evaluate</td>
                        <?php endif; ?>


                        <td><?php echo $sbrf_items['payee']; ?></td>
                        <td><?php echo $sbrf_items['designation']; ?></td>

                        <?php if($sbrf_items['evaluate_process'] == 1) : ?>
                            <td><?php echo $sbrf_items['process_remarks'] ;?></td>
                        <?php endif; ?>

                        <?php if($sbrf_items['evaluate_process'] == 0) : ?>
                            <td><?php echo $sbrf_items['evaluate_remarks'] ;?></td>
                        <?php endif; ?>
                        <td><a href="<?php echo base_url().'assets/soa_files/'.$sbrf_items['soa_file'];?>" download><?php echo $sbrf_items['soa_file'];?></a></td>
                    </tr>
                <?php endforeach; ?>
                <?php foreach($seminar_total as $seminar_total_key => $seminar_total_request): ?>
                    <tr>                        
                        <td class="font-bolder right aligned" colspan=3><?php echo number_format($seminar_total_request['total_seminar'],2); ?></td>
                        <td class="right aligned" colspan=5></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>            
        </div>        
    </div>

    <!-- Cap Budget Info Modal -->

    <div class="row">
        <div class="sixteen wide column">
            <div class="right floated ui tiny primary button cap-budget-info-modal">Cap Budget Info</div>
        </div>
    </div>

    <div class="ui long fullscreen modal cap-budget-info">
        <div class="header">Cap Budget Info</div>
        <div class="content">
            <div class="ui grid">
                <?php foreach($sbrf as $sbrf_key => $sbrf_request): ?>
                <div class="row">
                    <div class="font-bolder two wide column">EMA ID: </div>                    
                    <div class="two wide column ema_id"><?php echo $sbrf_request['ema_id']; ?></div>
                    <div class="font-bolder two wide column">Request Type: </div>                    
                    <div class="two wide column request_type">1</div>
                    <div class="eight wide column"></div>
                    <div class="font-bolder two wide column">EMA Code: </div>
                    <div class="two wide column ema"><?php echo $sbrf_request['ema_code']; ?></div>
                    <div class="font-bolder two wide column">EMA Name: </div>
                    <div class="four wide column ema_name"><?php echo $sbrf_request['ema_name']; ?></div>
                    <div class="six wide column"></div>
                    <div class="font-bolder two wide column">RSM Name: </div>
                    <div class="two wide column rsm_name"><?php echo $rsm['rsm_name']; ?></div>
                    <div class="font-bolder two wide column">Region: </div>
                    <div class="two wide column region_name"><?php echo $region['region']; ?></div>
                    <div class="eight wide column"></div>
                </div>
                <?php endforeach; ?>
                <div class="row">
                    <div class="eight wide column">
                        <table class="ui very compact small celled table">
                            <thead>
                                <tr>
                                    <th>Seminar Budget</th>
                                    <th>Seminar Total</th>
                                    <th>Seminar Remaining</th>
                                <tr>
                            </thead>
                            <tbody>
                                <?php foreach($total_remaining_seminar as $total_remaining_seminar_key => $total_remain_seminar): ?>
                                <tr>
                                    <td class="right aligned font-bolder seminar_budget"><?php echo number_format($total_remain_seminar['seminar_budget'],2); ?></td>
                                    <td class="right aligned font-bolder seminar_total"><?php echo number_format($total_remain_seminar['seminar_total'],2); ?></td>
                                    <td class="right aligned font-bolder seminar_budget"><?php echo number_format($total_remain_seminar['remaining_budget'],2); ?></td>
                                <tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>                    
                    </div>
                    <div class="eight wide column"></div>                    
                </div>
                
                <div class="row">
                    <table class="ui very compact small celled table cap-budget-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>School Code</th>
                                <th>School Name</th>
                                <th>payee</th>                                
                                <th>Voucher No</th>
                                <th>1% Disc Incentive</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($cap_seminar as $cap_seminar_key => $cap_seminars) : ?>
                            <tr>
                                <td><?php echo $cap_seminars['date']?></td>
                                <td><?php echo $cap_seminars['seminar']?></td>
                                <td><?php echo $cap_seminars['school_code']?></td>
                                <td><?php echo $cap_seminars['school_name']?></td>
                                <td><?php echo $cap_seminars['payee']?></td>
                                <td><?php echo $cap_seminars['voucher_no']?></td>

                                <?php if($cap_seminars['one_perc_disc'] == 1){
                                    echo "<td>yes</td>";
                                }else{
                                    echo "<td>no</td>";
                                }
                                ?>

                                
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

                
            </div>
        </div>
        <div class="actions">            
            <div class="ui ok button">ok</div>
        </div>
    </div>

    <form class="ui form seminar_request_approvals">
        <div class="field">
            <div class="font-bolder">Prepared by:</div>
            <br />
            <div class="font-bolder"><?php echo $sa_info[0]['firstname']." ".$sa_info[0]['lastname']; ?></div>
            <div><?php echo $sa_info[0]['position'];?></div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label">document status</div>
            </div>
        </div>        
        <div class="fields">        

            <!-- status field for SA -->
            <?php if($sbrf[0]['is_certified_correct_ar'] == 0) : ?>
                <div class="field">
                    <div>on pending request</div>
                </div>
            <?php endif; ?>

            <!-- checked status message from AR -->
            <?php if($sbrf[0]['is_certified_correct_ar'] == 1):?>
            <div class="field">
                <div class="font-bolder">Certified Correct:</div>
                <br />
                <div class="font-bolder"><?php echo $ar_info[0]['firstname']." ".$ar_info[0]['lastname']; ?></div>
                <div><?php echo $ar_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_ar_approval'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from RSM -->
            <?php if($sbrf[0]['is_rsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $rsm_info[0]['firstname']." ".$rsm_info[0]['lastname']; ?></div>
                <div><?php echo $rsm_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_rsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from NSM -->
            <?php if($sbrf[0]['is_nsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $nsm_info[0]['firstname']." ".$nsm_info[0]['lastname']; ?></div>
                <div><?php echo $nsm_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_nsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- approved status from COO -->
            <?php 
            if($sbrf[0]['is_coo_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Approved By:</div>
                <br />
                <div class="font-bolder">Don Timothy Buhain</div>
                <div>Chief Operating Officer</div>
                <?php echo $sbrf[0]['date_coo_approval'];?>
            </div>
            <?php endif;?>

        </div>
        <?php 
            if(
                $sbrf[0]['is_coo_approved'] == 2 ||
                $sbrf[0]['is_rsm_approved'] == 2 ||
                $sbrf[0]['is_nsm_approved'] == 2 
            ) : 
        ?>
        <div class="field">
            <div class="sixteen wide column">
                <br />
                <div class="font-bolder">Disapprove Remarks</div>
                
                <div><?php echo $sbrf[0]['remarks'];?></div>
            </div>
        </div>
        <?php endif; ?>

         <!-- check process remarks with voucher and check no -->

         <?php
            if(
                $sbrf[0]['is_check_processed'] == 1
            ) :
        ?>
        <div class="field">        
            <br />
            <div class="sixteen wide column">
                <div class="ui blue ribbon label">check process info</div>
            </div>   
            <br />
            <div class="sixteen wide column">                
                <div class="font-bolder">Check No: <?php echo $sbrf[0]['check_no']; ?></div>
                <div class="font-bolder">Voucher No: <?php echo $sbrf[0]['voucher_no']; ?></div>     
            </div>
        </div>
        
        <?php endif; ?>

        <!-- for accounts receivable -->

        <?php 
            if(
                $user['position_id'] == 7 && 
                $sbrf[0]['is_certified_correct_ar'] == 0 &&
                $sbrf[0]['is_rsm_approved'] == 0 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label accounts-receivable-label">verification from Accounts Receivable</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                
                <div class="ui red tiny button ar-cert-correct-btn"><i class="icon check"></i>certified correct</div>
            </div>
        </div>


        <?php endif; ?>

        <!-- end -->

        <!-- for rsm -->

        <?php 
            if(                
                $user['position_id'] == 3 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 0 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label rsm-approval-label">verification from RSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right rsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!-- end -->

        <!-- for nsm -->

        <?php 
            if(                
                $user['position_id'] == 5 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 1 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label nsm-approval-label">verification from NSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right nsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!-- end -->

        <!-- for coo -->

        <?php 
            if(                
                $user['position_id'] == 11 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 1 &&
                $sbrf[0]['is_nsm_approved'] == 1 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label coo-approval-label">verification from COO</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right coo-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        

        <!-- end -->

        <div class="field text-remarks" style="display:none;">

            <div class="sixteen wide column">
                <label>Remarks</label>
                <input class="remarks" type="text" name="remarks">                
            </div>

            <br />

            <?php if($user['position_id'] == 11): ?>
            <div class="ui red tiny button coo-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 3): ?>
            <div class="ui red tiny button rsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 5): ?>
            <div class="ui red tiny button nsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <div class="ui orange tiny button cancel-disapprove-btn"><i class="icon circle"></i>cancel</div>

        </div>

        <!-- for accounting check processing -->

        <?php 
            if(
                $user['position_id'] == 8 &&                 
                $sbrf[0]['is_check_processed'] == 0
            ):
        ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label accounting-label">check processing</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">                
                <div class="ui green tiny button process-check"><i class="icon check"></i>process check</div>
            </div>
        </div>

        <div class="field process-check-textfield" style="display:none;">

            <div class="sixteen wide column">
                <label>Check No.</label>
                <input class="check_no" type="text" name="check_no">                
            </div>

            <div class="sixteen wide column">
                <label>Voucher No.</label>
                <input class="voucher_no" type="text" name="voucher_no">                
            </div>

            <br />

            <div class="ui green tiny button confirm-process-check"><i class="icon check"></i>confirm process</div>
            <div class="ui orange tiny button cancel-process-check"><i class="icon circle"></i>cancel process</div>

        </div>


        <?php endif; ?>

        <!-- end -->

        <br />
    </form>

</div>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.seminar) == 'undefined') {
		sbrf.seminar = new CSeminar;
	}

	sbrf.seminar.bind_events();

</script>

