<div class="ui dividing header">Check Processed</div>

<div class="ui top attached tabular menu">
  <a class="item active" data-tab="mark_up">Mark Up Budget</a>  
  <a class="item" data-tab="incentive">Incentives</a>
  <a class="item" data-tab="donation">Donation</a>
  <a class="item" data-tab="seminar">Seminar</a>
</div>

<div class="ui bottom attached tab segment active" data-tab="mark_up">  
  <div class="ui link four stackable cards">
	<?php foreach($mark_up_budget as $mark_up_budget_key => $mub_request) : ?>

		<!-- viewing for SA  -->
		<?php 
			if(
				($user['position_id'] == 6 || $user['position_id'] == 8)
				&&
				$mub_request['is_cert_correct'] == 1 && 
				$mub_request['is_rsm_approved'] == 1 &&
				$mub_request['is_nsm_approved'] == 1 &&
				$mub_request['is_approved'] == 1 &&
				$mub_request['is_check_processed'] == 1
				) : 
		?>
			<div class="card">
				<div class="content">
					<div class="header">Mark up budget</div>
					<div class="meta"><?php echo $mub_request['date_created']?></div>
					</div>
					<div class="content">
					<h4 class="ui sub header">AICP No: <?php echo $mub_request['aicp_no']?></h4>
					<div class="ui small feed">
					
						<div class="event">
							<div class="content">
									<div class="summary">
										EMA Code: <?php echo $mub_request['ema_code']?>
									</div>
							</div>
						</div>     

					</div>
				</div>
				<div class="extra content">
					<button class="ui mini blue fluid button mark_up_view_details" data-id="<?php echo $mub_request['aicp_no']?>">view details</button>
				</div>

				<?php 
				if(
					$mub_request['is_cert_correct'] == 1 &&
					$mub_request['is_rsm_approved'] == 1 &&
					$mub_request['is_nsm_approved'] == 1 &&
					$mub_request['is_approved'] == 1 &&
					$mub_request['is_check_processed'] == 1
					) : 
				?>
				<div class="extra content">
					<span class="right floated">
						accts receivable
					</span>
					<span>
						<i class="green check icon"></i>
						certified
					</span>
				</div>
				<div class="extra content">
					<span class="right floated">
						rsm
					</span>
					<span>
						<i class="green check icon"></i>
						approved
					</span>
				</div>
				<div class="extra content">
					<span class="right floated">
						nsm
					</span>
					<span>
						<i class="green check icon"></i>
						approved
					</span>
				</div>
				<div class="extra content">
					<span class="right floated">
						coo
					</span>
					<span>
						<i class="green check icon"></i>
						approved
					</span>
				</div>
				<div class="extra content">
					<span class="right floated">
						acctg
					</span>
					<span>
						<i class="green check icon"></i>
						check processed
					</span>
				</div>
				<?php endif; ?>			

			</div>
		<?php endif;?>

	<?php endforeach; ?>

</div>

</div>

<div class="ui bottom attached tab segment" data-tab="incentive">

	<!-- document status cards -->

	<div class="ui link four stackable cards">

		<?php foreach($incentive as $incentive_key => $incentive_request) : ?>

			<!-- incentive request type -->

			<?php if($incentive_request['request_type'] == 2): ?>

				<!-- viewing for SA  -->
				<?php 
					if(
						($user['position_id'] == 6 || $user['position_id'] == 8)
						&&
						$incentive_request['is_certified_correct_ar'] == 1 && 
						$incentive_request['is_rsm_approved'] == 1 &&
						$incentive_request['is_nsm_approved'] == 1 &&
						$incentive_request['is_coo_approved'] == 1 &&
						$incentive_request['is_check_processed'] == 1
						) : 
				?>
					<div class="card">
						<div class="content">
							<div class="header">Incentives</div>
							<div class="meta"><?php echo $incentive_request['date_created']?></div>
							</div>
							<div class="content">
							<h4 class="ui sub header">SBRF No: <?php echo $incentive_request['sbrf_no']?></h4>
							<div class="ui small feed">
							
								<div class="event">
									<div class="content">
											<div class="summary">
												EMA Code: <?php echo $incentive_request['ema_code']?>
											</div>
									</div>
								</div>     

							</div>
						</div>

						<div class="extra content">
							<button class="ui mini blue fluid button incentive_view_details" data-id="<?php echo $incentive_request['sbrf_no']?>">view details</button>
						</div>

						<?php 
						if(
							$incentive_request['is_certified_correct_ar'] == 1 &&
							$incentive_request['is_rsm_approved'] == 1 &&
							$incentive_request['is_nsm_approved'] == 1 &&
							$incentive_request['is_coo_approved'] == 1 &&
							$incentive_request['is_check_processed'] == 1
							) : 
						?>
						<div class="extra content">
							<span class="right floated">
								accts receivable
							</span>
							<span>
								<i class="green check icon"></i>
								certified
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								rsm
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								nsm
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								coo
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								acctg
							</span>
							<span>
								<i class="green check icon"></i>
								check processed
							</span>
						</div>						
						<?php endif; ?>					

					</div>
				<?php endif; ?>
			
			<?php endif; ?>

		<?php endforeach; ?>
	</div>
  
</div>

<div class="ui bottom attached tab segment" data-tab="donation">
	
	<!-- document status cards -->

  	<div class="ui link four stackable cards">
	<?php foreach($donation as $donation_key => $donation_request) : ?>
	
		<!-- donation request type -->

		<?php if($donation_request['request_type'] == 3): ?>

			<!-- viewing for SA  -->
			<?php 
				if(
					($user['position_id'] == 6 || $user['position_id'] == 8)
					&&
					$donation_request['is_certified_correct_ar'] == 1 && 
					$donation_request['is_rsm_approved'] == 1 &&
					$donation_request['is_nsm_approved'] == 1 &&
					$donation_request['is_coo_approved'] == 1 &&
					$donation_request['is_check_processed'] == 1
					) : 
			?>
				<div class="card">
					<div class="content">
						<div class="header">Donation</div>
						<div class="meta"><?php echo $donation_request['date_created']?></div>
						</div>
						<div class="content">
						<h4 class="ui sub header">SBRF No: <?php echo $donation_request['sbrf_no']?></h4>
						<div class="ui small feed">
						
							<div class="event">
								<div class="content">
										<div class="summary">
											EMA Code: <?php echo $donation_request['ema_code']?>
										</div>
								</div>
							</div>     

						</div>
					</div>

					<div class="extra content">
						<button class="ui mini blue fluid button donation_view_details" data-id="<?php echo $donation_request['sbrf_no']?>">view details</button>
					</div>

					<?php 
					if(
						$donation_request['is_certified_correct_ar'] == 1 &&
						$donation_request['is_rsm_approved'] == 1 &&
						$donation_request['is_nsm_approved'] == 1 &&
						$donation_request['is_coo_approved'] == 1 &&
						$donation_request['is_check_processed'] == 1
						) : 
					?>
					<div class="extra content">
						<span class="right floated">
							accts receivable
						</span>
						<span>
							<i class="green check icon"></i>
							certified
						</span>
					</div>
					<div class="extra content">
						<span class="right floated">
							rsm
						</span>
						<span>
							<i class="green check icon"></i>
							approved
						</span>
					</div>
					<div class="extra content">
						<span class="right floated">
							nsm
						</span>
						<span>
							<i class="green check icon"></i>
							approved
						</span>
					</div>
					<div class="extra content">
						<span class="right floated">
							coo
						</span>
						<span>
							<i class="green check icon"></i>
							approved
						</span>
					</div>
					<div class="extra content">
						<span class="right floated">
							acctg
						</span>
						<span>
							<i class="green check icon"></i>
							check processed
						</span>
					</div>
					<?php endif; ?>		

			</div>
			<?php endif;?>

		<?php endif;?>

	<?php endforeach; ?>
	</div>

</div>

<div class="ui bottom attached tab segment" data-tab="seminar">

	<!-- document status cards -->

	<div class="ui link four stackable cards">
		<?php foreach($seminar as $seminar_key => $seminar_request) : ?>

			<!-- seminar request type -->

			<?php if($seminar_request['request_type'] == 4): ?>
			
				<!-- viewing for SA  -->
				<?php 
					if(
						($user['position_id'] == 6 || $user['position_id'] == 8)
						&&
						$seminar_request['is_certified_correct_ar'] == 1 && 
						$seminar_request['is_rsm_approved'] == 1 &&
						$seminar_request['is_nsm_approved'] == 1 &&
						$seminar_request['is_coo_approved'] == 1 &&
						$seminar_request['is_check_processed'] == 1
						) : 
				?>
					<div class="card">
						<div class="content">
							<div class="header">Seminar</div>
							<div class="meta"><?php echo $seminar_request['date_created']?></div>
							</div>
							<div class="content">
							<h4 class="ui sub header">SBRF No: <?php echo $seminar_request['sbrf_no']?></h4>
							<div class="ui small feed">
							
								<div class="event">
									<div class="content">
											<div class="summary">
												EMA Code: <?php echo $seminar_request['ema_code']?>
											</div>
									</div>
								</div>     

							</div>
						</div>

						<div class="extra content">
							<button class="ui mini blue fluid button seminar_view_details" data-id="<?php echo $seminar_request['sbrf_no']?>">view details</button>
						</div>

						<?php 
						if(
							$seminar_request['is_certified_correct_ar'] == 1 &&
							$seminar_request['is_rsm_approved'] == 1 &&
							$seminar_request['is_nsm_approved'] == 1 &&
							$seminar_request['is_coo_approved'] == 1 &&
							$seminar_request['is_check_processed'] == 1
							) : 
						?>
						<div class="extra content">
							<span class="right floated">
								accts receivable
							</span>
							<span>
								<i class="green check icon"></i>
								certified
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								rsm
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								nsm
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								coo
							</span>
							<span>
								<i class="green check icon"></i>
								approved
							</span>
						</div>
						<div class="extra content">
							<span class="right floated">
								acctg
							</span>
							<span>
								<i class="green check icon"></i>
								check processed
							</span>
						</div>
						<?php endif; ?>

					</div>
				<?php endif;?>

			<?php endif; ?>

		<?php endforeach; ?>
	</div>
</div>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.check_processed) == 'undefined') {
		sbrf.check_processed = new CCheck_processed;
	}

	sbrf.check_processed.bind_events();

</script>