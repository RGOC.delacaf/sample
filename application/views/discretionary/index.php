<div class="add-discretionary">
	<h2 class="ui dividing header">%1 Discretionary Fund</h2>

</div>



<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.discretionary) == 'undefined') {
		sbrf.discretionary = new CDiscretionary;
	}

	sbrf.discretionary.bind_events();

</script>