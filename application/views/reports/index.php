<div class="sbrf-reports">
    <div class="ui dividing header">
        Reports
    </div>

	<div class="ui grid">
		<div class="four wide column">			
			<div class="ui sub header">EMA</div>
			<div class="ui selection dropdown ema">			
				<input type="hidden" name="ema">
				<i class="dropdown icon"></i>
				<div class="default text">Select</div>
				<div class="menu">					
					<?php foreach($ema as $ema_key => $ema_list): ?>
					<div class="item" data-value="<?php echo $ema_list['ema_id'];?>"><?php echo $ema_list['ema_code']; ?></div>
					<?php endforeach;?>
				</div>
			</div>
		</div>				
	</div>

	<div class="ui grid ema-details">

		<div class="sixteen wide column">
			<div class="ui divider"></div>
		</div>		

		<div class="row">

			<div class="four wide column">
				<div class="ui sub header">EMA ID</div>
				<div class="ui input">
					<input class="ema_id" type="text" name="ema_id" readonly>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui sub header">Firstname</div>
				<div class="ui input">
					<input class="ema_firstname" type="text" name="ema_firstname" readonly>
				</div>
			</div>

			
			<div class="four wide column">
				<div class="ui sub header">Lastname</div>
				<div class="ui input">
					<input class="ema_lastname" type="text" name="ema_lastname" readonly>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="four wide column">
				<div class="ui sub header">Region ID</div>
				<div class="ui input">
					<input class="region_id" type="text" name="region_id" readonly>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui sub header">Region Name</div>
				<div class="ui input">
					<input class="region_name" type="text" name="region_name" readonly>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui sub header">RSM ID</div>
				<div class="ui input">
					<input class="rsm_id" type="text" name="rsm_id" readonly>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui sub header">RSM Name</div>
				<div class="ui input">
					<input class="rsm_name" type="text" name="rsm_name" readonly>
				</div>
			</div>

		</div>		

	</div>	

	<div class="ui grid report-request">

		<!-- <div class="sixteen wide column">
			<div class="ui divider"></div>
		</div> -->
		
		<div class="row">
			
			<div class="four wide column">
				<div class="ui sub header">reports</div>
				<div class="ui selection dropdown reports">			
					<input type="hidden" name="reports">
					<i class="dropdown icon"></i>
					<div class="default text">Select</div>
					<div class="menu">
						<div class="item" data-value="0"></div>
						<div class="item" data-value="1">Mark Up Budget</div>
						<div class="item" data-value="2">Incentive</div>
						<div class="item" data-value="3">Donation</div>
						<div class="item" data-value="4">Seminar</div>
					</div>
				</div>
			</div>

			<div class="four wide column one-percent-disc-selection">
				<div class="ui sub header">with 1% disc. incentive</div>
				<div class="ui selection dropdown one-percent-disc">
					<input type="hidden" name="one-percent-disc">
					<i class="dropdown icon"></i>
					<div class="defaut text">Select</div>
					<div class="menu">
						<div class="item" data-value="0">no</div>
						<div class="item" data-value="1">yes</div>
					</div>
				</div>
			</div>

			<div class="four wide column">
				<div class="ui sub header">from date</div>
				<div class="ui input left icon">
					<i class="calendar icon"></i>
					<input class="from-date" type="text" name="from-date">
				</div>	
			</div>

			<div class="four wide column">
				<div class="ui sub header">to date</div>
				<div class="ui input left icon">
					<i class="calendar icon"></i>
					<input class="to-date" type="text" name="to-date">
				</div>	
			</div>

		</div>

		<div class="row">

					

		</div>

		<div class="row">
			<div class="four wide column">
				<div class="ui button red generate-excel">
					<i class="file excel outline icon"></i>
					Generate File
				</div>
			</div>
		</div>

	</div>

	


</div>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.pending_request) == 'undefined') {
		sbrf.pending_request = new CReports;
	}

	sbrf.pending_request.bind_events();

</script>