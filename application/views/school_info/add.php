<div class="add-school">
    <div class="ui dividing header">Add School Information</div>
    <br />

    <form class="ui form add_school">
        <div class="four fields">
            <div class="field">
                <label>ema code</label>
                <div class="ui selection dropdown ema">
                    <input type="hidden" name="ema_id">
                    <i class="dropdown icon"></i>
                    <div class="default text">select an item</div>
                    <div class="menu">
                        <?php foreach($ema as $ema_key => $ema_list) : ?>
                        <div class="item" data-value="<?php echo $ema_list['ema_id'];?>"><?php echo $ema_list['ema_code']; ?></div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>ema id</label>
                <input class="ema_id" name="ema_id" type="text" readonly />
            </div>
            <div class="field">
                <label>firstname</label>
                <input class="firstname" name="firstname" type="text" readonly />
            </div>
            <div class="field">
                <label>lastname</label>
                <input class="lastname" name="lastname" type="text" readonly />
            </div>
            
        </div>

        <div class="two fields">
            <div class="field">
                <label>school code</label>
                <input class="school_code" name="school_code" type="text"/>
            </div>
            <div class="field">
                <label>school name</label>
                <input class="school_name" name="school_name" type="text"/>
            </div>
        </div>
        
        <div class="field">
            <label>address</label>
            <input class="address" name="address" type="text"/>
        </div>

        <div class="right floated ui red button add_school_save"><i class="save icon"></i>save form</div>
        
    </form>


</div>


<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.school_info) == 'undefined') {
		sbrf.school_info = new CSchool_info;
	}

	sbrf.school_info.bind_events();

</script>