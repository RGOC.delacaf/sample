<div class="school-info">
    <div class="ui dividing header">
        School Information    
    </div>
</div>

<br />

<form class="ui form school_info">
    <div class="two fields">        

        <div class="field">
            <label>School Name</label>
            <div class="ui selection search dropdown school">
                <input type="hidden" name="school">
                <i class="dropdown icon"></i>
                <div class="default text">select an item</div>
                <div class="menu">
                    <?php foreach($school_info as $school_info_key => $school): ?>
                    <div class="item" data-value="<?php echo $school['school_id'];?>"><?php echo $school['school_name'];?></div>
                    <?php endforeach; ?>
                </div>
            </div>            
        </div>
        
    </div>

    <div class="two fields">

        <div class="field">
            <label>School Id</label>
            <input class="customer_id" name="customer_id" type="text" readonly />
        </div>

        <div class="field">
            <label>School Code</label>
            <input class="customer_code" name="customer_code" type="text" readonly />
        </div>

    </div>

    
    <div class="field">
        <label>Address</label>
        <input class="customer_address" name="customer_address" type="text"/>
    </div>

    <div class="four fields">
        <div class="field">
            <label>evaluate / process</label>
            <input class="evaluate_process" name="evaluate_process" type="text" readonly/>
        </div>
        <div class="field">
            <label>&nbsp;</label>
            <div class="ui selection dropdown evaluate_process_selection">
                <input type="hidden" name="evaluate_process_remarks">
                <i class="dropdown icon"></i>
                <div class="default text">select an item</div>
                <div class="menu">
                    <div class="item" data-value="0">evaluate</div>
                    <div class="item" data-value="1">process</div>
                </div>
            </div>
        </div>
    </div>


    <div class="right floated ui red button save-changes"><i class="save icon"></i>save changes</div>

        

</form>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.school_info) == 'undefined') {
		sbrf.school_info = new CSchool_info;
	}

	sbrf.school_info.bind_events();

</script>