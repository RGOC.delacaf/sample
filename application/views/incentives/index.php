<div class="add-incentives">
	<h2 class="ui dividing header">Incentives</h2>
</div>
<br />

<form class="ui form add_sbrf">

    <div class="four fields">
    
        <div class="field">
            <label>Control No</label>
            <input class="sbrf_control_no" type="text" name="sbrf_control_no" value="<?php echo $sbrf_no; ?>" readonly>
        </div>

        <div class="field">
            <label>Process Status</label>
            <div class="ui selection dropdown process-status">
                <input type="hidden" name="process_status" class="process_status">
                <i class="dropdown icon"></i>
                <div class="default text">select process</div>
                <div class="menu">
                    <div class="item" data-value="1">new</div>
                    <div class="item" data-value="2">reprocess</div>
                    <div class="item" data-value="3">advance</div>
                </div>
            </div>
        </div>

    </div>

    <div class="four fields">

        <div class="field">
            <label>EMA Code</label>
            <div class="ui selection dropdown ema_code">
                <input type="hidden" name="ema_code">
                <i class="dropdown icon"></i>
                <div class="default text">select an item</div>
                <div class="menu">
                    <?php foreach($ema as $ema_key => $ema_code): ?>
                    <div class="item" data-value="<?php echo $ema_code['ema_id'];?>"><?php echo $ema_code['ema_code'];?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="field">
            <label>EMA Id</label>
            <input class="ema_id" name="ema_id">
        </div>        

        <div class="field">
            <label>Firstname</label>
            <input class="ema_firstname" name="ema_firstname">
        </div>

        <div class="field">
        <label>Lastname</label>
            <input class="ema_lastname" name="ema_lastname">
        </div>

    </div>

    <br />

    <div class="four fields">

        <div class="field">
        <label>RSM ID</label>
            <input class="rsm_id" name="rsm_id" readonly />
        </div>

        <div class="field">
            <label>RSM Nname</label>
            <input class="rsm_name" name="rsm_name" readonly />
        </div>

        <div class="field">
            <label>Region ID</label>
            <input class="region_id" name="region_id" readonly/>
        </div>

        <div class="field">
            <label>Region Name</label>
            <input class="region_name" name="region_name" readonly />
        </div>

    </div>    

    <div class="field">
        <div class="ui checkbox one_percent">
            <input type="checkbox" class="one_percent_discretionary_fund" name="one_percent_discretionary_fund" value="1"/>
            <label>one percent discretionary fund</label>
        </div>

        <?php echo form_open_multipart('incentives/save_incentive_request') ?>

        <input class="ps_file" name="ps_file" type="file" style="display:none;" />
        <div class="right floated ui button upload_ps_file"><i class="attach icon"></i>upload file</div>

    </div>

    <br />    

    <div class="ui divider"></div>

    <div class="two fields">
        
        <div class="field">
            <label>School name</label>
            <div class="ui selection dropdown search customer_name">
                <input type="hidden" name="customer_id">
                <i class="dropdown icon"></i>
                <div class="default text">select an item</div>
                <div class="menu">
                    
                    <!-- <?php foreach($customer as $customer_key => $customers): ?>
                        <div class="item" data-value="<?php echo $customers['customer_id'];?>"><?php echo $customers['customer_name'];?></div>                    
                    <?php endforeach; ?> -->
                    
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label>School id</label>
                <input type="text" class="customer_id" name="customer_id" readonly />
            </div>
            <div class="field">
                <label>School code</label>
                <input type="text" class="customer_code" name="customer_code" readonly />
            </div>
        </div>

        

    </div>

    <div class="field">
        <label>School Address</label>
        <input class="customer_address" name="customer_address" readonly>
    </div>

    <div class="field">
        <input class="soa_file" name="soa_file" type="file" style="display:none;" />
        <div class="right floated ui button upload_soa_file"><i class="attach icon"></i>upload SOA file</div>
    </div>

    <div class="field">

        <form class="ui form add-sbrf-item" >

            <div class="four fields">

                <!-- <div class="field">
                    <label>Sales Net Markup</label>
                    <input type="text" class="sales_net_markup" name="sales_net_markup" readonly />
                </div> -->

                <div class="field">
                    <label>Evaluate / Process</label>
                    <input type="text" class="evaluate_process" name="evaluate_process" readonly />
                </div>
                

            </div>

            <div class="ui divider"></div>

            <div class="four fields">
                <div class="field">
                    <label>Payee</label>
                    <input type="text" class="payee" name="payee" />
                </div>
                <div class="field">
                    <label>Designation</label>
                    <input type="text" class="designation" name="designation" />
                </div>
                <div class="field">
                    <label>Incentive Amount</label>
                    <input type="text" class="incentive" name="incentive" />            
                </div>
                <div class="field">
                    <label>Remarks</label>
                    <input type="text" class="remarks" name="remarks" />
                </div>
            </div>
            <div class="field">
                <div class="ui blue button add_incentive_item"><i class="ui add icon"></i>add item</div>
            </div>
            <div class="field">
                <table class="ui very compact small celled table incentive-sbrf-item-table">
                    <thead>
                        <tr> 
                            <th>item</th>                           
                            <th>school code</th>
                            <th>school name</th>
                            <th>incentive</th>
                            <th>payee</th>
                            <th>designation</th>
                            <th>remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>            
                </table>
            </div>

            <div class="ui red button remove_incentive_item"><i class="ui remove icon"></i>remove item</div>

        <?php echo form_close(); ?>
        </form>

    </div>    

    <br />

    <div class="right floated ui green button save_sbrf_form"><i class="ui save icon"></i>save form</div>

    

</form>

<div class="footer"></div>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.incentives) == 'undefined') {
		sbrf.incentives = new CIncentives;
	}

	sbrf.incentives.bind_events();

</script>