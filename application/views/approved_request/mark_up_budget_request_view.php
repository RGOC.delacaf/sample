<?php if(
    $user['position_id'] == 6 &&
    $mark_up_budget[0]['is_rsm_approved'] == 0 &&
    $mark_up_budget[0]['is_nsm_approved'] == 0 &&
    $mark_up_budget[0]['is_approved'] == 0
) :
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>pending_request">Pending Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $mark_up_budget[0]['aicp_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>
 
<?php if(
    $user['position_id'] == 6 &&
    $mark_up_budget[0]['is_rsm_approved'] == 2 ||
    $mark_up_budget[0]['is_nsm_approved'] == 2 ||
    $mark_up_budget[0]['is_approved'] == 2
) :
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>disapproved_request">Disapproved Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $mark_up_budget[0]['aicp_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>

<?php if(
    $user['position_id'] == 6 &&
    $mark_up_budget[0]['is_rsm_approved'] == 1 &&
    $mark_up_budget[0]['is_nsm_approved'] == 1 &&
    $mark_up_budget[0]['is_approved'] == 1
):
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>approved_request">Approved Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $mark_up_budget[0]['aicp_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="ui padded grid" style="background-color:#FFFFFF">

    <div class="row">
        <div class="eight wide column">
            <h4 class="ui dividing header">Mark Up Budget Request</h4>
        </div>        
    </div>    
    
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui blue ribbon label">mark up budget request info</div>
        </div>
    </div>
    <?php foreach($mark_up_budget as $mark_up_budget_key => $mub_request):?>
    <div class="row">
        <div class="font-bolder four wide column">process status</div>
        <div class="four wide column"><?php echo $mub_request['process_status']; ?></div>
    </div>
    <div class="row">
        <div class="font-bolder four wide column">Control No</div>
        <div class="four wide column aicp-no"><?php echo $mub_request['aicp_no']; ?></div>
        <div class="font-bolder four wide column">Date</div>
        <?php $formatted_date = date_create($mub_request['date_created']); ?>
        <div class="four wide column"><?php echo date_format($formatted_date,'m/d/Y --- g:i:s A'); ?></div>
    </div>
    <div class="row">
        <div class="font-bolder four wide column">EMA Code</div>
        <div class="four wide column"><?php echo $mub_request['ema_code']; ?></div>
        <div class="font-bolder four wide column">EMA Name</div>
        <div class="four wide column"><?php echo $mub_request['ema_name']; ?></div>

        <div class="font-bolder four wide column">RSM</div>
        <div class="four wide column"><?php echo $rsm['rsm_name']; ?></div>

        <div class="font-bolder four wide column">Region</div>
        <div class="four wide column"><?php echo $region['region']; ?></div>

        
    </div>  
    <div class="row">
        <div class="font-bolder four wide column">School Code</div>
        <div class="four wide column"><?php echo $mub_request['customer_code']; ?></div>
        <div class="font-bolder four wide column">School Name</div>
        <div class="four wide column"><?php echo $mub_request['customer_name']; ?></div>
        <div class="font-bolder four wide column">School Address</div>
        <div class="twelve wide column"><?php echo $mub_request['address']; ?></div>
    </div>
    
    <div class="row">        
        <?php if($mub_request['markup_file'] != '') : ?>
        <div class="font-bolder four wide column">Attached Supporting Document file:</div>
        <div class="four wide column"><a href='<?php echo base_url();?>assets/markup_files/<?php echo $mub_request['markup_file'];?>' download><i class="file icon"></i><?php echo $mub_request['markup_file'];?></a></div>
        <?php endif; ?>
    </div>

    <?php endforeach; ?>
    
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui blue ribbon label">summary of incentive distributions</div>
        </div>
    </div>
    <div class="row">        
        <div class="sixteen wide column">
            <table class="ui celled table">
            <thead>
                <tr>
                    <th class="center aligned">Payee</th>
                    <th class="center aligned">Position</th>                    
                    <th class="center aligned">Check Amount</th>                    
                    
                    <th class="center aligned">Remarks</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($mark_up_budget_item as $mark_up_budget_item_key => $mub_item):?>
                <tr>
                    <td><?php echo $mub_item['contact_person']?></td>
                    <td><?php echo $mub_item['position']?></td>                    
                    <td class="right aligned check_amt"><?php echo number_format($mub_item['check_amt'],2); ?></td>                    
                    
                    <td><?php echo $mub_item['remarks']?></td>
                </tr>
            <?php endforeach; ?>
            <?php foreach($mark_up_budget_grand_total_amt as $mark_up_budget_grand_total_amt_key => $mub_grand_total):?>
                <tr>
                    <td colspan=2></td>
                    <td class="font-bolder right aligned grand_total">
                        <?php echo number_format($mub_grand_total['grand_total'],2); ?>
                    </td>
                    <td></td>
                <tr>
            <?php endforeach;?>
            </tbody>
            </table>
        </div>        
    </div>    

    <form class="ui form approval-form" name="approval-form">
        
        <div class="field">
            <div class="font-bolder">Prepared by:</div>
            <br />
            <div class="font-bolder"><?php echo $sa_info[0]['firstname']." ".$sa_info[0]['lastname']; ?></div>
            <div><?php echo $sa_info[0]['position'];?></div>
        </div>
    
        <div class="field">        
            <div class="sixteen wide column">
                <div class="ui blue ribbon label">document status</div>
            </div>        
        </div>

        <div class="fields">

            <!-- status on the SA -->
            <?php if($mark_up_budget[0]['is_cert_correct'] == 0) : ?>
            <div class="field">                
                <div>On Pending Request</div>
            </div>
            <?php endif; ?>

            <!-- checked status message from AR -->
            <?php if($mark_up_budget[0]['is_cert_correct'] == 1):?>
            <div class="field">
                <div class="font-bolder">Certified Correct:</div>
                <br />
                <div class="font-bolder"><?php echo $ar_info[0]['firstname']." ".$ar_info[0]['lastname']; ?></div>
                <div><?php echo $ar_info[0]['position'];?></div>
                <?php echo $mark_up_budget[0]['date_corrected'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from RSM -->
            <?php if($mark_up_budget[0]['is_rsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $rsm_info[0]['firstname']." ".$rsm_info[0]['lastname']; ?></div>
                <div><?php echo $rsm_info[0]['position'];?></div>
                <?php echo $mark_up_budget[0]['date_rsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from NSM -->
            <?php if($mark_up_budget[0]['is_nsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $nsm_info[0]['firstname']." ".$nsm_info[0]['lastname']; ?></div>
                <div><?php echo $nsm_info[0]['position'];?></div>
                <?php echo $mark_up_budget[0]['date_nsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- approved status from COO -->
            <?php 
            if($mark_up_budget[0]['is_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Approved By:</div>
                <br />
                <div class="font-bolder">Don Timothy Buhain</div>
                <div>Chief Operating Officer</div>
                <?php echo $mark_up_budget[0]['date_approved'];?>
            </div>
            <?php endif;?>

        </div>

        <?php 
            if(
                $mark_up_budget[0]['is_approved'] == 2 ||
                $mark_up_budget[0]['is_rsm_approved'] == 2 ||
                $mark_up_budget[0]['is_nsm_approved'] == 2 
            ) : 
        ?>
        <div class="field">
            <div class="sixteen wide column">
                <br />
                <div class="font-bolder">Disapprove Remarks:</div>
                
                <div><?php echo $mark_up_budget[0]['remarks'];?></div>

                <?php if($mark_up_budget[0]['is_rsm_approved'] == 2) : ?>
                <br />
                <div class="font-bolder"><?php echo $rsm_info[0]['firstname']." ".$rsm_info[0]['lastname']; ?></div>
                <div><?php echo $rsm_info[0]['position'];?></div>                
                <?php endif; ?>

                <?php if($mark_up_budget[0]['is_nsm_approved'] == 2) : ?>
                <br />
                <div class="font-bolder"><?php echo $nsm_info[0]['firstname']." ".$nsm_info[0]['lastname']; ?></div>
                <div><?php echo $nsm_info[0]['position'];?></div>                
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if($mark_up_budget[0]['is_approved'] != 0) : ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label coo-remarks">
                    COO Remarks
                </div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <textarea row=5 readonly><?php echo $mark_up_budget[0]['coo_remarks'] ?></textarea>
            </div>
        </div>

        <?php endif; ?>

        <!-- for accounts receivable -->

        <?php 
            if(
                $user['position_id'] == 7 && 
                $mark_up_budget[0]['is_cert_correct'] == 0 &&
                $mark_up_budget[0]['is_rsm_approved'] == 0 &&
                $mark_up_budget[0]['is_nsm_approved'] == 0 &&
                $mark_up_budget[0]['is_approved'] == 0
            ):
        ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label accounts-receivable-label">verification from Accounts Receivable</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                
                <div class="ui red tiny button ar-cert-correct-btn"><i class="icon check"></i>certified correct</div>
            </div>
        </div>


        <?php endif; ?>

        <!-- end -->

        <!-- for rsm -->

        <?php 
            if(                
                $user['position_id'] == 3 &&
                $mark_up_budget[0]['is_cert_correct']  == 1 &&
                $mark_up_budget[0]['is_rsm_approved'] == 0 &&
                $mark_up_budget[0]['is_nsm_approved'] == 0 &&
                $mark_up_budget[0]['is_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label rsm-approval-label">verification from RSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right rsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!-- end -->

        <!-- for nsm -->

        <?php 
            if(                
                $user['position_id'] == 5 &&
                $mark_up_budget[0]['is_cert_correct']  == 1 &&
                $mark_up_budget[0]['is_rsm_approved'] == 1 &&
                $mark_up_budget[0]['is_nsm_approved'] == 0 &&
                $mark_up_budget[0]['is_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label nsm-approval-label">verification from NSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right nsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!--     end -->    

        <!-- for coo -->

        <?php 
            if(
                $user['position_id'] == 11 && 
                $mark_up_budget[0]['is_approved'] == 0 && 
                $mark_up_budget[0]['is_rsm_approved'] == 1 &&
                $mark_up_budget[0]['is_nsm_approved'] == 1 &&
                $mark_up_budget[0]['is_cert_correct'] == 1
            ) : 
        ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label coo-approval-label">approval from COO</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="ui green tiny button coo-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
            </div>
        </div>              

        <?php endif; ?>        

        <!-- end -->

        <div class="field text-remarks" style="display:none;">

            <div class="sixteen wide column">
                <label>Remarks</label>
                <input class="remarks" type="text" name="remarks">                
            </div>

            <br />

            <?php if($user['position_id'] == 11): ?>
            <div class="ui red tiny button coo-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 3): ?>
            <div class="ui red tiny button rsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 5): ?>
            <div class="ui red tiny button nsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <div class="ui orange tiny button cancel-disapprove-btn"><i class="icon circle"></i>cancel</div>

        </div>

    </form>

</div>

<div class="blank">
    <br />
</div>


<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.mu_budget) == 'undefined') {
		sbrf.mu_budget = new CMU_Budget;
	}

	sbrf.mu_budget.bind_events();

</script>