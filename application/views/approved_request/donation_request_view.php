<?php if(
    $user['position_id'] == 6 &&
    $sbrf[0]['is_rsm_approved'] != 2 &&
    $sbrf[0]['is_nsm_approved'] != 2 &&
    $sbrf[0]['is_coo_approved'] == 0
) :
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>pending_request">Pending Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $sbrf[0]['sbrf_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>
 
<?php if(
    $user['position_id'] == 6 &&
    $sbrf[0]['is_rsm_approved'] == 2 ||
    $sbrf[0]['is_nsm_approved'] == 2 ||
    $sbrf[0]['is_coo_approved'] == 2
) :
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>disapproved_request">Disapproved Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $sbrf[0]['sbrf_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>

<?php if(
    $user['position_id'] == 6 &&
    $sbrf[0]['is_rsm_approved'] == 1 &&
    $sbrf[0]['is_nsm_approved'] == 1 &&
    $sbrf[0]['is_coo_approved'] == 1
):
?>

<div class="ui padded grid">
    <div class="row">
        <div class="ui breadcrumb">
            <a class="section" href="<?php echo base_url()?>approved_request">Approved Request</a>
            <i class="right angle icon divider"></i>
            <div class="active section"><?php echo $sbrf[0]['sbrf_no']; ?></div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="ui padded grid" style="background-color:#FFFFFF;">
    <div class="row">
        <div class="eight wide column">
            <div class="ui dividing header">Sales Budget Request - Incentive</div>
        </div>
    </div>
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui blue ribbon label">incentive request info</div>
        </div>
    </div>
    <?php foreach($sbrf as $sbrf_key => $sbrf_request): ?>
    <div class="row">
        <div class="font-bolder four wide column">Control No</div>
        <div class="four wide column sbrf-no"><?php echo $sbrf_request['sbrf_no']; ?></div>
        <div class="font-bolder four wide column">Date Created</div>
        <?php $formatted_date = date_create($sbrf_request['date_created']); ?>
        <div class="four wide column"><?php echo date_format($formatted_date,'m/d/Y --- g:i:s A'); ?></div>
    </div>
    <div class="row">
        <div class="font-bolder four wide column">EMA Code</div>
        <div class="four wide column"><?php echo $sbrf_request['ema_code']; ?></div>
        <div class="font-bolder four wide column">EMA Name</div>
        <div class="four wide column"><?php echo $sbrf_request['ema_name']; ?></div>

        <div class="font-bolder four wide column">RSM</div>
        <div class="four wide column"><?php echo $rsm['rsm_name']; ?></div>

        <div class="font-bolder four wide column">Region</div>
        <div class="four wide column"><?php echo $region['region']; ?></div>

    </div>

    <div class="row">
        <div class="font-bolder four wide column">1% Discretionary Fund</div>
        <?php if($sbrf_request['is_one_percent_disc'] == 1) : ?>
            <div class="four wide column">yes</div>
        <?php endif; ?>
        <?php if($sbrf_request['is_one_percent_disc'] == 0) : ?>
            <div class="four wide column">no</div>
        <?php endif; ?>
        <?php if($sbrf_request['ps_file'] != '') : ?>
        <div class="font-bolder four wide column">attached project synopsis file</div>
        <div class="four wide column"><a href='<?php echo base_url();?>assets/ps_files/<?php echo $sbrf_request['ps_file'];?>' download><i class="file icon"></i><?php echo $sbrf_request['ps_file'];?></a></div>
        <?php endif; ?>
    </div>

    <div class="row">        
        <?php if($sbrf_request['donation_file'] != '') : ?>
        <div class="font-bolder four wide column">Attached Donation Document file:</div>
        <div class="four wide column"><a href='<?php echo base_url();?>assets/donation_files/<?php echo $sbrf_request['donation_file'];?>' download><i class="file icon"></i><?php echo $sbrf_request['donation_file'];?></a></div>
        <?php endif; ?>
    </div>

    <?php endforeach; ?>
    <div class="row">
        <div class="sixteen wide column">
            <table class="ui celled table">
                <thead>
                    <th>school code</th>
                    <th>school name</th>
                    <th>incentive amount</th>
                    <th>evaluate/process</th>
                    <th>payee</th>
                    <th>designation</th>
                    <th>remarks</th>
                    <th>SOA file</th>
                </thead>
                <tbody>
                <?php foreach($sbrf_item as $sbrf_item_key => $sbrf_items) : ?>
                    <tr>
                        <td><?php echo $sbrf_items['customer_code']; ?></td>
                        <td><?php echo $sbrf_items['customer_name']; ?></td>
                        <td class="right aligned"><?php echo number_format($sbrf_items['donation'],2); ?></td>

                        <?php if($sbrf_items['evaluate_process'] == 1) : ?>
                            <td>process</td>
                        <?php endif; ?>

                        <?php if($sbrf_items['evaluate_process'] == 0) : ?>
                            <td>evaluate</td>
                        <?php endif; ?>


                        <td><?php echo $sbrf_items['payee']; ?></td>
                        <td><?php echo $sbrf_items['designation']; ?></td>

                        <?php if($sbrf_items['evaluate_process'] == 1) : ?>
                            <td><?php echo $sbrf_items['process_remarks'] ;?></td>
                        <?php endif; ?>

                        <?php if($sbrf_items['evaluate_process'] == 0) : ?>
                            <td><?php echo $sbrf_items['evaluate_remarks'] ;?></td>
                        <?php endif; ?>
                        <td><a href="<?php echo base_url().'assets/soa_files/'.$sbrf_items['soa_file'];?>" download><?php echo $sbrf_items['soa_file'];?></a></td>
                    </tr>
                <?php endforeach; ?>
                <?php foreach($donation_total as $donation_total_key => $donations): ?>
                    <tr>                        
                        <td class="font-bolder right aligned" colspan=3><?php echo number_format($donations['total_donation'],2); ?></td>
                        <td class="right aligned" colspan=5></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>            
        </div>        
    </div>

    <form class="ui form incentive_request_approvals">
        <div class="field">
            <div class="font-bolder">Prepared by:</div>
            <br />
            <div class="font-bolder"><?php echo $sa_info[0]['firstname']." ".$sa_info[0]['lastname']; ?></div>
            <div><?php echo $sa_info[0]['position'];?></div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label">document status</div>
            </div>
        </div>        
        <div class="fields">        

            <!-- status field for SA -->
            <?php if($sbrf[0]['is_certified_correct_ar'] == 0) : ?>
                <div class="field">
                    <div>on pending request</div>
                </div>
            <?php endif; ?>

            <!-- checked status message from AR -->
            <?php if($sbrf[0]['is_certified_correct_ar'] == 1):?>
            <div class="field">
                <div class="font-bolder">Certified Correct:</div>
                <br />
                <div class="font-bolder"><?php echo $ar_info[0]['firstname']." ".$ar_info[0]['lastname']; ?></div>
                <div><?php echo $ar_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_ar_approval'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from RSM -->
            <?php if($sbrf[0]['is_rsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $rsm_info[0]['firstname']." ".$rsm_info[0]['lastname']; ?></div>
                <div><?php echo $rsm_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_rsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- noted status message from NSM -->
            <?php if($sbrf[0]['is_nsm_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Noted and approved by:</div>
                <br />
                <div class="font-bolder"><?php echo $nsm_info[0]['firstname']." ".$nsm_info[0]['lastname']; ?></div>
                <div><?php echo $nsm_info[0]['position'];?></div>
                <?php echo $sbrf[0]['date_nsm_approval'];?>
            </div>
            <?php endif;?>

            <!-- approved status from COO -->
            <?php 
            if($sbrf[0]['is_coo_approved'] == 1):?>
            <div class="field">
                <div class="font-bolder">Approved By:</div>
                <br />
                <div class="font-bolder">Don Timothy Buhain</div>
                <div>Chief Operating Officer</div>
                <?php echo $sbrf[0]['date_coo_approval'];?>
            </div>
            <?php endif;?>

        </div>
        <?php 
            if(
                $sbrf[0]['is_coo_approved'] == 2 ||
                $sbrf[0]['is_rsm_approved'] == 2 ||
                $sbrf[0]['is_nsm_approved'] == 2 
            ) : 
        ?>
        <div class="field">
            <div class="sixteen wide column">
                <br />
                <div class="font-bolder">Disapprove Remarks</div>
                
                <div><?php echo $sbrf[0]['remarks'];?></div>
            </div>
        </div>
        <?php endif; ?>

        <?php if($sbrf[0]['is_coo_approved'] != 0) : ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label coo-remarks">
                    COO Remarks
                </div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <textarea row=5 readonly><?php echo $sbrf[0]['coo_remarks'] ?></textarea>
            </div>
        </div>

        <?php endif; ?>

        <!-- for accounts receivable -->

        <?php 
            if(
                $user['position_id'] == 7 && 
                $sbrf[0]['is_certified_correct_ar'] == 0 &&
                $sbrf[0]['is_rsm_approved'] == 0 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>

        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label accounts-receivable-label">verification from Accounts Receivable</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                
                <div class="ui red tiny button ar-cert-correct-btn"><i class="icon check"></i>certified correct</div>
            </div>
        </div>


        <?php endif; ?>

        <!-- end -->

        <!-- for rsm -->

        <?php 
            if(                
                $user['position_id'] == 3 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 0 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label rsm-approval-label">verification from RSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right rsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!-- end -->

        <!-- for nsm -->

        <?php 
            if(                
                $user['position_id'] == 5 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 1 &&
                $sbrf[0]['is_nsm_approved'] == 0 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label nsm-approval-label">verification from NSM</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right nsm-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <!-- end -->

        <!-- for coo -->

        <?php 
            if(                
                $user['position_id'] == 11 &&
                $sbrf[0]['is_certified_correct_ar']  == 1 &&
                $sbrf[0]['is_rsm_approved'] == 1 &&
                $sbrf[0]['is_nsm_approved'] == 1 &&
                $sbrf[0]['is_coo_approved'] == 0
            ):
        ?>
        
        <div class="field">
            <div class="sixteen wide column">
                <div class="ui blue ribbon label coo-approval-label">verification from COO</div>
            </div>
        </div>

        <div class="field">
            <div class="sixteen wide column">
                <!-- <button class="ui red tiny fluid button rsm-check-btn"><i class="icon check"></i>check</button> -->
                <div class="four wide colunm right floated">
                <div class="ui green tiny button align right coo-approve-btn"><i class="icon checkmark"></i>approve</div>
                <div class="ui red tiny button disapprove-textarea"><i class="icon remove"></i>disapprove</div>
                </div>
            </div>
        </div>

        <?php endif;?>

        

        <!-- end -->

        <div class="field text-remarks" style="display:none;">

            <div class="sixteen wide column">
                <label>Remarks</label>
                <input class="remarks" type="text" name="remarks">                
            </div>

            <br />

            <?php if($user['position_id'] == 11): ?>
            <div class="ui red tiny button coo-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 3): ?>
            <div class="ui red tiny button rsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <?php if($user['position_id'] == 5): ?>
            <div class="ui red tiny button nsm-disapprove-btn"><i class="icon remove"></i>disapprove</div>
            <?php endif; ?>

            <div class="ui orange tiny button cancel-disapprove-btn"><i class="icon circle"></i>cancel</div>

        </div>

        <br />
    </form>

</div>

<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.donations) == 'undefined') {
		sbrf.donations = new CDonations;
	}

	sbrf.donations.bind_events();

</script>

