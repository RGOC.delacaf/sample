<div class="dashboard">
	<div class="ui sixteen wide column">
		<div class="row">
			<div class="ui dividing header">Dashboard</div>
			<table class="ui very compact small celled selectable table request_status">
				<thead>
					<tr>
						<th>Request Type</th>
						<th>Pending Request</th>
						<th>Approved Request</th>
						<th>Disapproved Request</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Mark up</td>
						<td><?php echo $pending_mark_up_request[0]['aicp_pending_requests']; ?></td>
						<td><?php echo $approved_mark_up_request[0]['aicp_approved_requests']; ?></td>
						<td><?php echo $disapproved_mark_up_request[0]['aicp_disapproved_requests']; ?></td>
					</tr>					
					<tr>
						<td>Donation</td>
						<td><?php echo $pending_donation_request[0]['sbrf_pending_requests']; ?></td>
						<td><?php echo $approved_donation_request[0]['sbrf_approved_requests']; ?></td>
						<td><?php echo $disapproved_donation_request[0]['sbrf_disapproved_requests']; ?></td>
					</tr>
					<tr>
						<td>Incentive</td>
						<td><?php echo $pending_incentive_request[0]['sbrf_pending_requests']; ?></td>
						<td><?php echo $approved_incentive_request[0]['sbrf_approved_requests']; ?></td>
						<td><?php echo $disapproved_incentive_request[0]['sbrf_disapproved_requests']; ?></td>
					</tr>
					<tr>
						<td>Seminar</td>
						<td><?php echo $pending_seminar_request[0]['sbrf_pending_requests']; ?></td>
						<td><?php echo $approved_seminar_request[0]['sbrf_approved_requests']; ?></td>
						<td><?php echo $disapproved_seminar_request[0]['sbrf_disapproved_requests']; ?></td>
					</tr>					
				</tbody>				
			</table>
		</div>
	</div>
</div>