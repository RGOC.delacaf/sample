<table class="ui very compact small celled table aicp-item-table">
    <thead>
        <tr>
            <th>item</th>
            <th>payee</th>
            <th>position</th>            
            <th>requesting amount</th>            
            <th>remarks</th>
            
        </tr>
    </thead>
    <tbody>    
        <?php foreach($aicp_item as $aicp_item_key => $aicp_items) : ?>
        <tr>
            <td>
                <div data-id="<?php echo $aicp_items['id']; ?>">
                    <input class="ui checkbox" type="checkbox" name="aicp_item" />
                </div>                
            </td>
            <td><?php echo $aicp_items['contact_person']; ?></td>
            <td><?php echo $aicp_items['position']; ?></td>            
            <td class="right aligned"><?php echo $aicp_items['check_amt'];?></td>            
            <td><?php echo $aicp_items['remarks'];?></td>

        </tr>
        <?php endforeach; ?>        
    </tbody>
    <!-- <tfoot>
        <?php foreach($aicp_item_total as $aicp_item_total_key => $aicp_item_total_amt) : ?>
        <tr>
            <th class="right aligned font-bolder" colspan="4"><?php echo $aicp_item_total_amt['grand_total']; ?></th>
            <th></th>
        </tr>        
        <?php endforeach; ?>
    </tfoot> -->
</table>