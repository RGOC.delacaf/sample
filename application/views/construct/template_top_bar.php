<div class="ui block header top segment right aligned middle aligned top-bar">

	<div class="display-none sidebar-toggle f-left default-cursor margin-top-10">
		<i class="content large icon"></i>   <?php echo $page_title ?>
	</div>
	
	<!-- <img class="ui mini  circular image" src="<?php echo base_url()?>/assets/images/logo.png">&nbsp -->
	<span><i class="user icon"></i></span>
	<span class="first-name"><?php echo $user['firstname']?></span>
	<span class="last-name"><?php echo $user['lastname']?></span>
	
	<!-- <div class="ui top right pointing dropdown inline header-notification">

		<div class="icon-notif">
			<i class="alarm outline blue big icon"></i>
			<div class="ui mini yellow circular label">99+</div>			
		</div>

		<div class="menu border">

			<div class="header">
				<i class="alarm icon"></i>
				Notifications
			</div>

			<div class="scrolling menu">

			

				<div data-id="1" class="item seen">
					<div class="ui feed">
						<div class="event">
							<div class="label">
								<img src="<?php echo base_url()?>assets/images/logo.png" alt='photo'>
							</div>
							<div class="content">
								<div class="summary">
									<div class="notification-title">
										test
									</div>
									<div class="clear"></div>
									<div class="date f-right margin-top-10 margin-right-10">
										1 hour ago
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			

			</div>

			
				<div class="header text-align-center default-cursor load-more">
					<i class="notched circle loading icon" style="display:none;"></i>
					<span>LOAD MORE</span>
				</div>			

		</div>

	</div> -->
		
	<div class="ui top right pointing dropdown header-settings">
		<i class="setting big icon"></i>
		<div class="menu">
			<div class="item my-account">
				<i class="user icon"></i>My Account
			</div>
			<div class="item sign-out">
				<i class="sign out icon"></i>Logout
			</div>
		</div>
	</div>

</div>

<div class="ui modal notifications">
	<div class="header">
	</div>

	<div class="content">

		<div class="ui negative message">
			<div class="header message-header">
				Oops! Something went wrong.
			</div>
			<p>Please refresh the page. If error still exist please contact the administrator.</p>
		</div>

		<div class="description">
			<div class="ui segment">
				<div class="ui active inverted dimmer modal-loader">
					<div class="ui text loader">Loading</div>
				</div>


				<div class="notification-logo-div">
					<img class="notification-logo ui middle aligned image" src="<?php echo base_url()?>assets/images/logo.png">
					<span>SBRF</span>
				</div>

				<div class="message">

				</div>

			</div>
		</div>
	</div>

	<div class="actions">
		<div class="ui close button">
			Close
		</div>
	</div>

</div>

<div class="item notification-template" style="display:none;">
	<div class="ui feed">
		<div class="event">
			<div class="label">
				<img/>
			</div>
			<div class="content">
				<div class="summary">
					<div class="notification-title">

					</div>
					<div class="clear"></div>
					<div class="date f-right margin-top-10 margin-right-10">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
