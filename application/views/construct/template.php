<!DOCTYPE html>
<html lang="en">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<!-- insert icon of the company here-->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo.png" type="image/x-icon" />

	<!-- insert title of page here upon construct -->
	<title>SBRF | <?php echo $page_title ?> </title>

	<!--core CSS -->
	<!-- <link href="<?php echo base_url()?>assets/semantic-ui/semantic.css" rel="stylesheet" type="text/css" /> -->
	<link href="<?php echo base_url()?>assets/semantic-ui/semantic.min.css" rel="stylesheet" type="text/css" />

	<!-- font -->
	<link href="https://fonts.googleapis.com/css?family=Lato|Montserrat" rel="stylesheet">
	
	<link href="<?php echo base_url()?>assets/css/basic_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/css/common.css" rel="stylesheet" type="text/css" />
	
	<!-- <link href="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" /> -->
	<link href="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>assets/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css" />
	
	<link href="<?php echo base_url() ?>assets/datatables/css/dataTables.semanticui.min.css" rel="stylesheet" type="text/css" />
	

	<!-- Custom styles for this template -->
	<?php if(isset($css) AND array_check($css)): ?>
		<?php foreach ($css as $css_value) : ?>
			<link href="<?php echo base_url() ."assets/css/".$css_value .".css" ?>" rel="stylesheet" type="text/css" />
		<?php endforeach;?>
	<?php endif;?>

	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery/jquery-3.2.1.min.js"></script>
	
	<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/semantic-ui/semantic.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/semantic-ui/semantic.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/popup/popup.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-datetimepicker/jquery.datetimepicker.full.min.js"></script>	

	<script type="text/javascript" src="<?php echo base_url()?>assets/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.dataTables.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url()?>assets/datatables/js/dataTables.semanticui.min.js"></script>
	
	

	<script type="text/javascript" src="<?php echo base_url()?>assets/angular/angular.js"></script>	
	
	<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.min.js"></script>
	<!-- <script type="text/javascript" src="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.common.js"></script> -->

	<script type="text/javascript">
		var sBaseURL = '<?php echo base_url(); ?>';
		var iUserID  = '<?php echo $this->session->userdata('portal_user_id'); ?>';		
	</script>

	<!-- Custom JS for this template -->
	<?php if(isset($javascripts) AND array_check($javascripts)): ?>
		<?php foreach ($javascripts as $javascript) : ?>
			<script type="text/javascript" src="<?php echo base_url() ."assets/js/".$javascript .".js" ?>"></script>
		<?php endforeach;?>
	<?php endif;?>

</head>

<body style="background: #EFEFEF !important;">

	<!--Side Bar-->
	<?php $this->load->view('construct/sidebar'); ?>

	<div class="pusher">

		<!--Top Navigation Bars-->
	 	<?php $this->load->view('construct/template_top_bar');?>

	 	<!--Main Content-->
		<div class="ui container" style="width:90%;">
			<?php echo $content; ?>
		</div>

		<!--Footer-->
		<?php $this->load->view('construct/template_footer');?>

	</div>

	<script type="text/javascript">

		var sBaseURL = '<?php echo base_url(); ?>';
		var iUserID  = '<?php echo $this->session->userdata('user_id'); ?>';

		$(document).ready(function(){
			mobile_desktop();

			var uiDesktop = $('.sidebar-desktop').html();


			$('.sidebar-menu').html(uiDesktop);

			$('.top-bar .ui.dropdown').dropdown();

			$(window).resize(function() {
				mobile_desktop();
			});


		}).on('click','.sidebar-toggle',function(e){
			$('.sidebar-menu').sidebar('toggle');
		});


		function mobile_desktop(){

			if ($(window).width() < 1200) {
				//mobile or tablet
				$('.pusher .sidebar-toggle').show();
				$('.sidebar-desktop').hide();

			}
			else {
				//desktop
				$('.pusher .sidebar-toggle').hide();
				$('.sidebar-desktop').show();

			}
		}

	</script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-cookie/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-form/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/basic.js"></script>	

</body>

</html>
