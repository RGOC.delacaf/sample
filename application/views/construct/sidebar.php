<div class="ui vertical inverted sticky menu fixed top sidebar-desktop sidebar-all">

	<div class="div-logo">		
		<img class="logo" src="<?php echo base_url()?>assets/images/logo.png">
		<label>SBRF</label>		
	</div>

	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'dashboard') ?>" href="<?php echo base_url() ?>dashboard">
		<i class="dashboard icon"></i>Dashboard
	</a>

	<!-- Accounting Receivables Menu -->

	<?php if($user['position_id'] == 7) : ?>
	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'pending_request') ?>" href="<?php echo base_url() ?>pending_request">
		<i class="inbox icon"></i>Pending Request
		<div class="ui blue label">
			<?php echo $pending_request_ar; ?>
		</div>
	</a>

	<div class="item main-menu-item<?php echo menu_toggle($main_page, 'school_info')?>">
		<i class="configure icon"></i>
		School Info
		<div class="menu school_info">
			<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'add') ?>" href="<?php echo base_url() ?>school_info/add">
				add
			</a>
		</div>
		<div class="menu school_info">
			<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'update') ?>" href="<?php echo base_url() ?>school_info/update">
				update
			</a>
		</div>

	</div>

	<?php endif; ?>

	<!-- Regional Sales Manager Menu -->

	<?php if($user['position_id'] == 3) : ?>

	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'pending_request') ?>" href="<?php echo base_url() ?>pending_request">
		<i class="inbox icon"></i>Pending Request
		<div class="ui blue label">
			<?php echo $pending_request_rsm; ?>
		</div>
	</a>
	
	<?php endif; ?>

	<!-- National Sales Manager Menu -->

	<?php if($user['position_id'] == 5) : ?>

	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'pending_request') ?>" href="<?php echo base_url() ?>pending_request">
		<i class="inbox icon"></i>Pending Request
		<div class="ui blue label">
			<?php echo $pending_request_nsm; ?>
		</div>
	</a>

	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'reports') ?>" href="<?php echo base_url() ?>reports">
		<i class="file text outline icon"></i>Reports
	</a>

	<?php endif; ?>

	<!-- Chief Operating Officer Menu -->

	<?php if($user['position_id'] == 11) : ?>
	<a class="item main-menu-item<?php echo menu_toggle($main_page, 'pending_request') ?>" href="<?php echo base_url() ?>pending_request">
		<i class="inbox icon"></i>Pending Request
		<div class="ui blue label">
			<?php echo $pending_request_coo; ?>
		</div>
	</a>
	<?php endif; ?>

	<!-- Sales Administrator Menu -->

	<?php if($user['position_id'] == 6): ?>

		<div class="item main-menu-item<?php echo menu_toggle($main_page, 'create_request')?>">
			<i class="add circle icon"></i>
			Create Request
			<div class="menu create_request_item">
				<a class="item <?php echo menu_toggle($sub_page, 'mark_up_budget') ?>" href="<?php echo base_url()?>mark_up/index">
					Mark-up Budget Request					
				</a>				
				<a class="item <?php echo menu_toggle($sub_page, 'incentives') ?>" href="<?php echo base_url()?>incentives/index">
					Incentive
				</a>
				<a class="item <?php echo menu_toggle($sub_page, 'donations') ?>" href="<?php echo base_url()?>donations/index">
					Donation
				</a>
				<a class="item <?php echo menu_toggle($sub_page, 'seminar') ?>" href="<?php echo base_url()?>seminar/index">
					Seminar
				</a>
			</div> 
		</div>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'pending_request') ?>" href="<?php echo base_url() ?>pending_request">
			<i class="inbox icon"></i>Pending Request
			<div class="ui blue label">
				<?php echo $pending_request_sa; ?>
			</div>
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'approved_request') ?>" href="<?php echo base_url() ?>approved_request">
			<i class="checkmark box icon"></i>Approved Request
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'disapproved_request') ?>" href="<?php echo base_url() ?>disapproved_request">
			<i class="dont icon"></i>Disapproved Request
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'check_processed') ?>" href="<?php echo base_url() ?>check_processed">
			<i class="checkmark box icon"></i>Check Processed
			<!-- <div class="ui blue label">
				<?php echo $check_processed; ?>
			</div> -->
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'reports') ?>" href="<?php echo base_url() ?>reports">
			<i class="file text outline icon"></i>Reports
		</a>

		<div class="item main-menu-item<?php echo menu_toggle($main_page, 'school_info')?>">
			<i class="configure icon"></i>
			School Info
			<div class="menu school_info">
				<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'add') ?>" href="<?php echo base_url() ?>school_info/add">
					add
				</a>
			</div>
			<div class="menu school_info">
				<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'update') ?>" href="<?php echo base_url() ?>school_info/update">
					update
				</a>
			</div>

		</div>

		

	<?php endif; ?>		

	<!-- Accounting Department Menu -->

	<?php if($user['position_id'] == 8) : ?>
	
		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'check_request') ?>" href="<?php echo base_url() ?>check_request">
			<i class="inbox icon"></i>Check Request
			<div class="ui blue label">
				<?php echo $check_request; ?>
			</div>
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'check_processed') ?>" href="<?php echo base_url() ?>check_processed">
			<i class="checkmark box icon"></i>Check Processed			
		</a>

	<?php endif; ?>

	<!-- Administrator Menu	 -->

	<?php if($user['position_id'] == 1): ?>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'reports') ?>" href="<?php echo base_url() ?>reports">
			<i class="file text outline icon"></i>Reports
		</a>

		<div class="item main-menu-item<?php echo menu_toggle($main_page, 'school_info')?>">
			<i class="configure icon"></i>
			School Info
			<div class="menu school_info">
				<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'add') ?>" href="<?php echo base_url() ?>school_info/add">
					add
				</a>
			</div>
			<div class="menu school_info">
				<a class="item main-menu-item<?php echo menu_toggle($sub_page, 'update') ?>" href="<?php echo base_url() ?>school_info/update">
					update
				</a>
			</div>

		</div>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'reports') ?>" href="<?php echo base_url() ?>reports">
			<i class="file text outline icon"></i>Reports
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'database') ?>" href="<?php echo base_url() ?>dashboard">
			<i class="database icon"></i>Database
		</a>

		<a class="item main-menu-item<?php echo menu_toggle($main_page, 'configurations') ?>" href="<?php echo base_url() ?>dashboard">
			<i class="settings icon"></i>Configuration
		</a>

	<?php endif; ?>

</div>

<div class="ui sidebar inverted vertical menu sidebar-menu sidebar-all">

</div>
