<div class="user-info">
    <div class="ui dividing header">
        User Information
    </div>
<div>


<br />

<form class="ui form user_info">

    <div class="four fields">

        <div class="field">
            <label>User ID</label>
            <input class="user_id" name="user_id" type="text" value="<?php echo $user['id']; ?>"readonly/>
        </div>

        <div class="field">
            <label>Firstname</label>
            <input class="firstname" name="firstname" type="text" value="<?php echo $user['firstname']; ?>"readonly/>
        </div>
        <div class="field">
            <label>Lastname</label>
            <input class="lastname" name="lastname" type="text" value="<?php echo $user['lastname']; ?>"readonly/>
        </div>

        <div class="field">
            <label>Position</label>
            <input class="position" name="position" type="text" value="<?php echo $user['position_name']; ?>"readonly/>
        </div>

    </div>

    <br />

    <div class="change_password_fields">
    
        <div class="ui dividing header">changing password</div>
        
        <div class="field">
            <div class="ui negative message">
                <div class="header"></div>
                <ul class="list"></ul>
            </div>
        </div>

        <div class="field">
            <div class="ui positive message">
                <div class="header"></div>
                <ul class="list"></ul>
            </div>
        </div>

        <div class="li-template" style="display:none">
            <li></li>
        </div>
        
        <div class="four fields">
                    
            <div class="field">
                <label>old password</label>
                <input class="old_password" name="old_password" type="password" />
            </div>

            <div class="field">
                <label>new password</label>
                <input class="new_password" name="new_password" type="password" />
            </div>

            <div class="field">
                <label>confirm new password</label>
                <input class="confirm_new_password" name="confirm_new_password" type="password" />
            </div>
        </div>

        <div class="right floated ui red button save_password"><i class="save icon"></i>change password</div>

    </div>

    

</form>


<script>
	if (typeof (sbrf) == 'undefined') {
		var sbrf = {};
	}

	if (typeof (sbrf.user) == 'undefined') {
		sbrf.user = new CUser;
	}

	sbrf.user.bind_events();

</script>