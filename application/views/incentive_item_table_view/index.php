<table class="ui very compact small celled table incentive-sbrf-item-table">
    <thead>
        <tr>
            <th>item</th>
            <th>school code</th>
            <th>school name</th>
            <th>incentive</th>
            <th>evaluate/process</th>
            <th>payee</th>
            <th>designation</th>
            <th>remarks</th>
            <th>SOA File</th>
        </tr>
    </thead>
    <tbody>    
        <?php foreach($sbrf_item as $sbrf_item_key => $sbrf_items) : ?>
        <tr>            
            <td>
                <div data-id="<?php echo $sbrf_items['id']; ?>">
                    <input class="ui checkbox sbrf_item" type="checkbox" name="sbrf_item">
                </div>                
            </td>
            <td><?php echo $sbrf_items['customer_code']; ?></td>
            <td><?php echo $sbrf_items['customer_name']; ?></td>
            <td><?php echo $sbrf_items['incentive']; ?></td>

            <td>
                <?php 
                    if($sbrf_items['evaluate_process'] == 1){
                        echo 'process';
                    }
                    if($sbrf_items['evaluate_process'] == 0){
                        echo 'evaluate';
                    }
                    
                ?>
            </td>


            <td><?php echo $sbrf_items['payee']; ?></td>
            <td><?php echo $sbrf_items['designation']; ?></td>

            <td>
                <?php 
                    if($sbrf_items['evaluate_process'] == 1){
                        echo $sbrf_items['process_remarks'];
                    }
                    if($sbrf_items['evaluate_process'] == 0){
                        echo $sbrf_items['evaluate_remarks'];
                    }
                    
                ?>
            </td>

            <td>
                <a href="<?php echo base_url().'assets/soa_files/'.$sbrf_items['soa_file']; ?>" download><?php echo $sbrf_items['soa_file']; ?></a>
            </td>
            
        </tr>
        <?php endforeach; ?>        
    </tbody>
</table>