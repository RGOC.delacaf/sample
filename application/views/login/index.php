
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- insert icon of the company here-->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo.png">

  <!-- font -->
  <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat" rel="stylesheet"> 

  <!-- Site Properties -->
  <title>SBRF | Login</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/reset.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/site.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/container.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/grid.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/header.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/image.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/menu.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/divider.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/segment.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/form.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/transition.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/input.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/button.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/list.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/message.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/semantic-ui/components/icon.css">

  

  <style type="text/css">

    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    .brand-title{
      color: #333333;	
	    font-family: 'Montserrat', sans-serif;
    }

  </style>
  
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui red image header">
      <img src="<?php echo base_url() ?>assets/images/logo.png" class="image">      
      <div class="content brand-title">
        SBRF
      </div>
    </h2>
    <form class="ui large form login">
      <div class="ui negative message display-none">
         <div class="header">          
         </div>
      </div>

        <?php if($referrer): ?>
          <div class="ui info message">
            <i class="close icon"></i>
            <div>
              Session has expired. Please login again.
            </div>
          </div>
        <?php endif; ?>
        
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="Username">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="ui fluid large red submit login-btn button"><i class="sign in icon"></i>Login</div>
      </div>

      <div class="ui error message"></div>

    </form>

   <!--  <div class="ui message">
      New to us? <a href="#">Sign Up</a>
    </div> -->
  </div>
</div>


<script src="<?php echo base_url() ?>assets/jquery/jquery-3.2.1.js"></script>
<script src="<?php echo base_url() ?>assets/jquery/jquery-3.2.1.min.js"></script>

<script src="<?php echo base_url() ?>assets/semantic-ui/semantic.js"></script>
<script src="<?php echo base_url() ?>assets/semantic-ui/semantic.min.js"></script>



<script src="<?php echo base_url() ?>assets/semantic-ui/components/site.js"></script>



<script src="<?php echo base_url() ?>assets/semantic-ui/components/form.js"></script>
<script src="<?php echo base_url() ?>assets/semantic-ui/components/transition.js"></script>




<script>
  var sBaseURL = '<?php echo base_url() ?>';
</script>

<script src="<?php echo base_url() ?>assets/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>assets/js/login.js"></script>


</body>

</html>
