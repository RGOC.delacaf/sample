<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	function __construct(){
        parent::__construct();
        assert_options(ASSERT_WARNING, 0);
    }

    function get_xss_clean_name_value_pairs(array $name_value_pairs) {
		$xss_xlean_name_value_pairs = array();

		foreach($name_value_pairs as $key => $value) {
			$xss_xlean_name_value_pairs[$key] = $this->security->xss_clean($value);
		}

		return $xss_xlean_name_value_pairs;
	}


	public function insert($table_name, array $name_value_pairs, $dont_clean = false){
		if($dont_clean === false){
			$name_value_pairs = $this->get_xss_clean_name_value_pairs($name_value_pairs);
		}
		$this->db->insert($table_name, $name_value_pairs);
		assert($this->db->insert_id() != FALSE || $this->db->insert_id() != NULL);
		return $this->db->insert_id();
	}



	public function update($table_name, $id, array $name_value_pairs,$set = array()){
		if(array_check($set)){
			foreach($set as $set_key => $set_value){
				//PRODUCES: ( SET `field` = field+1 ) if false
				//PRODUCES: ( SET `field` = "field+1" ) if true
				$this->db->set($set_key,$set_value,FALSE);
			}
		}
		$this->db->where('id', $id);
		$this->db->update($table_name, $this->get_xss_clean_name_value_pairs($name_value_pairs));
		return $this->db->affected_rows();
	}

	public function update_where($table_name, $column_name, $id, array $name_value_pairs,$set = array()){
		if(array_check($set)){
			foreach($set as $set_key => $set_value){
				//PRODUCES: ( SET `field` = field+1 ) if false
				//PRODUCES: ( SET `field` = "field+1" ) if true
				$this->db->set($set_key,$set_value,FALSE);
			}
		}
		$this->db->where($column_name, $id);
		$this->db->update($table_name, $this->get_xss_clean_name_value_pairs($name_value_pairs));
		return $this->db->affected_rows();
	}

	public function update_where_many($table_name, array $name_value_pairs, array $where,$set = array()){
		if(array_check($set) and count($set)>0){
			foreach($set as $set_key => $set_value){
				//PRODUCES: ( SET `field` = field+1 ) if false
				//PRODUCES: ( SET `field` = "field+1" ) if true
				$this->db->set($set_key,$set_value,FALSE);
			}
		}

		$this->db->where($where);
		$this->db->update($table_name,$this->get_xss_clean_name_value_pairs($name_value_pairs));
		return $this->db->affected_rows();
	}

	public function upload_markup_file(
		$file = '',
		$upload_path = './assets/markup_files',
		$allowed_types = 'jpg|png|gif|pdf|xls|xlsx|doc|docx',
		$max_size = '250000',
		$file_name = ''
	){
		$return = array();

		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;	
		$config['filename'] = $file_name;

		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($file)){
			$return['error'] = $this->upload->display_errors();
		}
		else{
			$return['data'] = $this->upload->data();
		}

		return $return;
	}

	public function upload_erf_file(
		$file = '',
		$upload_path = './assets/erf_files',
		$allowed_types = 'jpg|png|gif|pdf|xls|xlsx|doc|docx',
		$max_size = '250000',
		$file_name = ''
	){
		$return = array();

		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;	
		$config['filename'] = $file_name;

		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($file)){
			$return['error'] = $this->upload->display_errors();
		}
		else{
			$return['data'] = $this->upload->data();
		}

		return $return;
	}

	public function upload_donation_file(
		$file = '',
		$upload_path = './assets/donation_files',
		$allowed_types = 'jpg|png|gif|pdf|xls|xlsx|doc|docx',
		$max_size = '250000',
		$file_name = ''
	){
		$return = array();

		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;	
		$config['filename'] = $file_name;

		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($file)){
			$return['error'] = $this->upload->display_errors();
		}
		else{
			$return['data'] = $this->upload->data();
		}

		return $return;
	}

	public function upload_ps_file(
		$file = '',
		$upload_path = './assets/ps_files',
		$allowed_types = 'jpg|png|gif|pdf|xls|xlsx|doc|docx',
		$max_size = '250000',
		$file_name = ''
	){
		$return = array();

		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;	
		$config['filename'] = $file_name;

		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($file)){
			$return['error'] = $this->upload->display_errors();
		}
		else{
			$return['data'] = $this->upload->data();
		}

		return $return;
	}

	public function upload_soa_file(
		$file = '',
		$upload_path = './assets/soa_files',
		$allowed_types = 'jpg|png|gif|pdf|xls|xlsx|doc|docx',
		$max_size = '250000',
		$file_name = ''
	){
		$return = array();

		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;	
		$config['filename'] = $file_name;

		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($file)){
			$return['error'] = $this->upload->display_errors();
		}
		else{
			$return['data'] = $this->upload->data();
		}

		return $return;
	}

		
	
}
