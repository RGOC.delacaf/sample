<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	/**
	* This is for rendering the page in the template
	* @param 	array 	$options 	this are the view page and data to be put in the page
	* @param 	array 	$data 		this is pass a data for information needed on the page
    */
    
	public function render_page($options=array(), $data=array()){
        
        $user_id = $this->session->userdata('user_id');

        $position_id = $this->Users->get_position_by_user_id($user_id);
        
        if(isset($options['params'])) {
            $view_params = $options['params'];
        }
        else {
            $view_params = array();
        }
        
        if( isset($options['page_title']) ){
            $view_params['page_title'] = $options['page_title'];
        }
        
        if( isset($options['main_page']) ){
            $view_params['main_page'] = $options['main_page'];
        }
        
        if( isset($options['sub_page']) ){
            $view_params['sub_page'] = $options['sub_page'];
        }    
               
        // pending request notification

        $data['user_id'] = $user_id;

        $sbrf_sa = $this->SBRF->all_sbrf_pending_requests();            
        $aicp_sa = $this->AICP->all_aicp_pending_requests();

        $sbrf_ar = $this->SBRF->sbrf_request_for_approval_by_requestor(1);
        $aicp_ar = $this->AICP->aicp_request_for_approval_by_requestor(1);

        $sbrf_rsm = $this->SBRF->sbrf_request_for_rsm_approval_by_rsm_id($user_id);
        $aicp_rsm = $this->AICP->aicp_request_for_rsm_approval_by_rsm_id($user_id);

        // $sbrf_rsm = $this->SBRF->sbrf_request_for_approval_by_requestor(2);
        // $aicp_rsm = $this->AICP->aicp_request_for_approval_by_requestor(2);

        $sbrf_nsm = $this->SBRF->sbrf_request_for_approval_by_requestor(3);
        $aicp_nsm = $this->AICP->aicp_request_for_approval_by_requestor(3);

        $sbrf_coo = $this->SBRF->sbrf_request_for_approval_by_requestor(4);
        $aicp_coo = $this->AICP->aicp_request_for_approval_by_requestor(4);
        
        $data['pending_request_sa'] = $sbrf_sa[0]['sbrf_pending_requests'] + $aicp_sa[0]['aicp_pending_requests'];
        $data['pending_request_ar'] = $sbrf_ar[0]['sbrf_pending_requests'] + $aicp_ar[0]['aicp_pending_requests'];        
        $data['pending_request_rsm'] = $sbrf_rsm[0]['sbrf_pending_requests'] + $aicp_rsm[0]['aicp_pending_requests'];
        $data['pending_request_nsm'] = $sbrf_nsm[0]['sbrf_pending_requests'] + $aicp_nsm[0]['aicp_pending_requests'];
        $data['pending_request_coo'] = $sbrf_coo[0]['sbrf_pending_requests'] + $aicp_coo[0]['aicp_pending_requests'];

        // check process status

        $aicp_check_request = $this->AICP->get_aicp_check_requests();
        $sbrf_check_request = $this->SBRF->get_sbrf_check_requests();

        $data['check_request'] = $sbrf_check_request[0]['sbrf_check_requests'] + $aicp_check_request[0]['aicp_check_requests'];

        $aicp_check_processed = $this->AICP->get_aicp_check_processed();
        $sbrf_check_processed = $this->SBRF->get_sbrf_check_processed();

        $data['check_processed'] = $sbrf_check_processed[0]['sbrf_check_processed'] + $aicp_check_processed[0]['aicp_check_processed'];        

        // kprint($sbrf_check_request);exit;

        
        
                // $position = $this->users->get_user_role_by_user_id($user_id);
        
                // $ema_users = array();
                // $os_id = array();
                // if($position['position_id'] == 2 AND ($_SERVER['SERVER_ADDR'] == '::1' OR $_SERVER['SERVER_ADDR'] == '127.0.0.1')){
                //     $os_id = array(1);
                //     $ema_users = array($user_id);
                // }
                // elseif($position['position_id'] == 2){
                //     $ema_users = array($user_id);
                //     $os_id = array(1,2,3,4,5,6);
                // }elseif ($position['position_id'] == 3) {
                //     $under_ema = $this->users->get_emas_by_its_rsm_id($user_id);
        
                //     if(array_check($under_ema)){
                //         foreach ($under_ema as $ema) {
                //             array_push($ema_users, $ema['user_id']);
                //         }
                //     }
        
                //     $os_id = array(1,2,6);
                // }elseif ($position['position_id'] == 4) {
                //     $os_id = array(2, 6, 7);
        
                //     $under_ema = $this->users->get_emas_by_its_css_id($user_id);
        
                //     if(array_check($under_ema)){
                //         foreach ($under_ema as $ema) {
                //             array_push($ema_users, $ema['user_id']);
                //         }
                //     }
                // }elseif ($position['position_id'] == 1) {
                //     $os_id = array(1,2,4,5,6,7);
                // }elseif ($position['position_id'] == 5) {
                //     $under_ema = $this->users->get_emas_by_its_rsm_id($user_id);
        
                //     if(array_check($under_ema)){
                //         foreach ($under_ema as $ema) {
                //             array_push($ema_users, $ema['user_id']);
                //         }
                //     }
        
                //     $os_id = array(1,2,4,5,6);
                // }
        
                //for notification count top bar
                // $view_params['notification_count'] = $this->notifications->count_user_notifications($user_id);
        
                //get notifications
                // $view_params['notifications'] = $this->notifications->get_user_notifications($user_id);
        
        $view_params['copyright'] = $this->auto_copyright();
        
        $data['content'] = $this->load->view($options['page'], $view_params,TRUE);
        
        $this->load->view('construct/template',$data);
            
    }

    /**
    * This is used mostly in all pages except for login if user can access a specific page
    */
    
    public function _verify_user_authentication() {

        $user_id	= $this->session->userdata('user_id');
        
        if (isset($user_id) == FALSE || $user_id == '') {
            $this->session->set_flashdata('session_exp','Your session has expired. Please log in again.');
            $this->session->set_userdata('referrer', current_url());
            redirect(base_url());
        }

    }

    /**
    * This is for changing the copyright
    * @param 	string 	$year 	starting year/ if auto void.
    * @return 	string 			year
    */

    private function auto_copyright($year = 'auto') {
        
        if(intval($year) == 'auto'){
            return date('Y');
        }

        if(intval($year) == date('Y')){
            return intval($year);
        }

        if(intval($year) < date('Y')){
            return intval($year) . ' - ' . date('Y');
        }

        if(intval($year) > date('Y')){
            return date('Y');
        }
    }


	
}
