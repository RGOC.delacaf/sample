<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('menu_active')) {
	function menu_toggle($page_name = '', $this = '') {
		if(!empty($page_name) && !empty($this)) {
			if($page_name === $this) {
				return ' active"';
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}