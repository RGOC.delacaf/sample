<?php

	class Login_Model extends MY_Model {
		
		function __construct(){
			parent::__construct();
			
		}
		/**
		* This is to check if the email to be regitered is already existing.
		* @param 	$email 	string 	This is the email to be checked
		* @return 	$return array 	
		*/
		public function check_if_username_exist($username = ''){

			$return = array();

			$email = $this->db->escape_str($email);

			$query_string = "SELECT id, name, username
							 FROM users
							 WHERE email = '{$username}'
							 ";

			$query = $this->db->query($query_string);

			if($query->num_rows() > 0)
	        {
	        	$return = $query->result_array();
	        }

			return $return;

		}

		public function get_user_authentication($email, $password, $is_password_md5=TRUE) {

			$record = array();
			
			if($is_password_md5 === TRUE){
				$md5_password = md5($password);
			}
			else{							
				$md5_password = $password;
			}

			$email = $this->db->escape_str($email);
			
			$query_string = "SELECT * 
							FROM users
							WHERE username = '{$email}' AND password = '{$md5_password}'";
			
			$query = $this->db->query($query_string);

			if($query->num_rows() > 0)
	        {
	        	$record = $query->row_array();
	        }
           
            
            return $record;
		}
	}