<?php

    class SBRF_Model extends MY_Model {
        
        function __construct(){
            parent::__construct();            
        }

        public function get_count_sbrf_non_deleted_items_by_sbrf_no($sbrf_no = 0){
            $record = array();
            $query_string = "
            SELECT 
                COUNT(*) as count_items
            FROM
                sbrf_items si 
            WHERE 1 = 1                 
                AND si.is_deleted = 0
                AND si.sbrf_no = '{$sbrf_no}' 
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_total_sbrf_listing_for_cap_by_ema_id($id = 0,$request_type = 0){
            $record = array();

            if($request_type == '2'){
                $query_string = "
                SELECT     
                    s.date_check_process AS date,
                    si.incentive AS incentive,
                    si.school_code AS school_code,
                    cl.customer_name AS school_name,
                    si.payee AS payee,
                    s.voucher_no AS voucher_no,
                    s.is_one_percent_disc AS one_perc_disc 
                FROM
                    sbrf_items si 
                    LEFT JOIN client_list cl 
                        ON cl.id = si.school_id 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                AND si.is_deleted = 0
                AND s.ema_id = '{$id}'
                AND s.request_type = '{$request_type}'
                AND s.is_check_processed = 1
                AND YEAR(s.date_check_process) = YEAR(NOW())
                ";
            }

            if($request_type == '3'){
                $query_string = "
                SELECT     
                    s.date_check_process AS date,
                    si.donation AS donation,
                    si.school_code AS school_code,
                    cl.customer_name AS school_name,
                    si.payee AS payee,
                    s.voucher_no AS voucher_no,
                    s.is_one_percent_disc AS one_perc_disc 
                FROM
                    sbrf_items si 
                    LEFT JOIN client_list cl 
                        ON cl.id = si.school_id 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                AND s.ema_id = '{$id}'
                AND s.request_type = '{$request_type}'
                AND s.is_check_processed = 1
                AND YEAR(s.date_check_process) = YEAR(NOW())
                ";
            }

            if($request_type == '4'){
                $query_string = "
                SELECT     
                    s.date_check_process AS date,
                    si.seminar AS seminar,
                    si.school_code AS school_code,
                    cl.customer_name AS school_name,
                    si.payee AS payee,
                    s.voucher_no AS voucher_no,
                    s.is_one_percent_disc AS one_perc_disc 
                FROM
                    sbrf_items si 
                    LEFT JOIN client_list cl 
                        ON cl.id = si.school_id 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                AND s.ema_id = '{$id}'
                AND s.request_type = '{$request_type}'
                AND s.is_check_processed = 1
                AND YEAR(s.date_check_process) = YEAR(NOW())
                ";
            }            

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_total_sbrf_with_budget_with_remaining_by_ema_id($id = 0,$request_type = 0){
            $record = array();

            if($request_type == '2'){
                $query_string = "
                SELECT 
                (SELECT 
                    SUM(si.incentive) 
                FROM
                    sbrf_items si 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                    AND si.is_deleted = 0
                    AND s.request_type = '{$request_type}' 
                    AND s.status = 1 
                    AND s.is_check_processed = 1 
                    AND s.ema_id = {$id}
                    AND YEAR(s.date_check_process) = YEAR(NOW())) AS incentive_total,
                (SELECT 
                    cb.budget_amount 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}'
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS incentive_budget,
                (SELECT 
                    cb.remaining_budget 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}' 
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS remaining_budget 
                ";
            }

            if($request_type == '3'){
                $query_string = "
                SELECT 
                (SELECT 
                    SUM(si.donation) 
                FROM
                    sbrf_items si 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                    AND s.request_type = '{$request_type}' 
                    AND s.status = 1 
                    AND s.is_check_processed = 1 
                    AND s.ema_id = {$id}
                    AND YEAR(s.date_check_process) = YEAR(NOW())) AS donation_total,
                (SELECT 
                    cb.budget_amount 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}'
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS donation_budget,
                (SELECT 
                    cb.remaining_budget 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}' 
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS remaining_budget 
                ";
            }

            if($request_type == '4'){
                $query_string = "
                SELECT 
                (SELECT 
                    SUM(si.seminar) 
                FROM
                    sbrf_items si 
                    INNER JOIN sbrf s 
                        ON s.sbrf_no = si.sbrf_no 
                WHERE 1 = 1
                    AND s.request_type = '{$request_type}' 
                    AND s.status = 1 
                    AND s.is_check_processed = 1 
                    AND s.ema_id = {$id}
                    AND YEAR(s.date_check_process) = YEAR(NOW())) AS seminar_total,
                (SELECT 
                    cb.budget_amount 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}'
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS seminar_budget,
                (SELECT 
                    cb.remaining_budget 
                FROM
                    cap_budget cb 
                WHERE 1 = 1 
                    AND cb.budget_type = '{$request_type}' 
                    AND cb.ema_id = {$id}
                    AND cb.year = YEAR(NOW())) AS remaining_budget 
                ";
            }

            

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_total_amount_by_sbrf_no($sbrf_no = 0,$request_type = 0){
            $record = array();

            if($request_type == 2){
                $query_string = "
                SELECT 
                    SUM(incentive) AS total_amount 
                FROM
                    sbrf_items si 
                WHERE 1 = 1
                    AND si.is_deleted = 0
                    AND si.sbrf_no = '{$sbrf_no}'
                ";
            }

            if($request_type == 3){
                $query_string = "
                SELECT 
                    SUM(donation) AS total_amount 
                FROM
                    sbrf_items si 
                WHERE sbrf_no = '{$sbrf_no}'
                ";
            }

            if($request_type == 4){
                $query_string = "
                SELECT 
                    SUM(seminar) AS total_amount 
                FROM
                    sbrf_items si 
                WHERE sbrf_no = '{$sbrf_no}'
                ";
            }

            $query = $this->db->query($query_string);
            $result = $query->row_array();

            return $result;
            
        }

        public function get_sbrf_check_processed(){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_check_processed
            FROM
                sbrf s 
            WHERE (
                s.is_certified_correct_ar = 1 AND
                s.is_rsm_approved = 1 AND
                s.is_nsm_approved = 1 AND
                s.is_coo_approved = 1 AND
                s.is_check_processed = 1
            )
            ";

            $query = $this->db->query($query_string);
            $result = $query->result_array();

            return $result;

        }

        public function get_sbrf_check_requests(){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_check_requests
            FROM
                sbrf s 
            WHERE (
                s.is_certified_correct_ar = 1 AND
                s.is_rsm_approved = 1 AND
                s.is_nsm_approved = 1 AND
                s.is_coo_approved = 1 AND
                s.is_check_processed = 0
            )
            ";

            $query = $this->db->query($query_string);
            $result = $query->result_array();

            return $result;

        }

        // data that will be used on my_controller core class

        public function all_sbrf_pending_requests(){
            $record = array();
            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_pending_requests
            FROM
                sbrf s 
            WHERE
                (                    
                    s.is_rsm_approved != 2 AND 
                    s.is_nsm_approved != 2 AND 
                    s.is_coo_approved = 0
                )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        // data for sbrf pending requests by request type

        public function sbrf_pending_requests_by_type($request_type = 0){
            $record = array();

            $where = "";

            $where = " AND s.request_type = ".$request_type."";

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_pending_requests
            FROM
                sbrf s 
            WHERE
                1 = 1
                AND
                (
                    s.is_rsm_approved != 2 AND 
                    s.is_nsm_approved != 2 AND 
                    s.is_coo_approved = 0
                )
                ".$where."
            ";            

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        // data for sbrf approved requests by request type

        public function sbrf_approved_requests_by_type($request_type){
            $record = array();

            $where = "";

            $where = " AND s.request_type =".$request_type."";

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_approved_requests
            FROM
                sbrf s 
            WHERE
                (
                    s.is_rsm_approved = 1 AND 
                    s.is_nsm_approved = 1 AND 
                    s.is_coo_approved = 1
                )
                ".$where."
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        // data for sbrf disapproved requests by request type

        public function sbrf_disapproved_requests_by_type($request_type = 0){
            $record = array();

            $where = "";

            // $where = " AND s.request_type = {$request_type}" ;

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_disapproved_requests
            FROM
                sbrf s 
            WHERE
                (
                    s.is_rsm_approved = 2 OR 
                    s.is_nsm_approved = 2 OR 
                    s.is_coo_approved = 2
                )
                AND s.request_type = {$request_type}
            ";            

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function sbrf_request_for_rsm_approval_by_rsm_id($rsm_id = 0){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS sbrf_pending_requests 
            FROM
                sbrf s 
            WHERE (
                s.is_certified_correct_ar = 1
                AND s.is_rsm_approved = 0
                )
                AND s.rsm_id = '{$rsm_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function sbrf_request_for_approval_by_requestor($requestor = 0){
            $record = array();
            
            // for ar approval

            if($requestor == 1 ){
                $query_string = "
                SELECT 
                    COUNT(*) AS sbrf_pending_requests 
                FROM
                    sbrf s 
                WHERE (
                    s.is_certified_correct_ar = 0
                    )                
                ";
            }

            // for rsm approval

            if($requestor == 2 ){
                $query_string = "
                SELECT 
                    COUNT(*) AS sbrf_pending_requests 
                FROM
                    sbrf s 
                WHERE (
                    s.is_certified_correct_ar = 1
                    AND s.is_rsm_approved = 0                     
                    )                
                ";
            }

            // for nsm approval

            if($requestor == 3 ){
                $query_string = "
                SELECT 
                    COUNT(*) AS sbrf_pending_requests 
                FROM
                    sbrf s 
                WHERE (
                    s.is_certified_correct_ar = 1
                    AND s.is_rsm_approved = 1 
                    AND s.is_nsm_approved = 0                     
                    )                
                ";
            }            

            // for coo approval

            if($requestor == 4 ){
                $query_string = "
                SELECT 
                    COUNT(*) AS sbrf_pending_requests 
                FROM
                    sbrf s 
                WHERE (
                    s.is_certified_correct_ar = 1
                    AND s.is_rsm_approved = 1 
                    AND s.is_nsm_approved = 1 
                    AND s.is_coo_approved = 0
                    )                
                ";
            }            

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_userinfo_by_id($user_id = 0){
            $record = array();

            $query_string = "
            SELECT
                i.firstname as firstname,
                i.lastname as lastname,
                p.name as position
            FROM
                users u
            LEFT JOIN info i ON i.user_id = u.id
            LEFT JOIN positions p ON u.position_id = p.id
            WHERE
            1 = 1
            AND u.id = '{$user_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_incentives(){
            $record = array();
            
            $query_string = "
            SELECT 
                    s.id AS id,
                    s.sbrf_no AS sbrf_no,
                    s.ema_id AS ema_id,
                    u.username AS ema_code,
                    s.revenue_net_markup AS revenue_net_markup,
                    s.selling_expenses AS selling_expenses,
                    s.sales_budget_guide AS sales_bugdet_guide,
                    s.unused_balance AS unused_balance,
                    s.sa_id AS sa_id,
                    s.rsm_id AS rsm_id,
                    s.nsm_id AS nsm_id,
                    s.ar_id AS ar_id,
                    s.is_rsm_approved AS is_rsm_approved,
                    s.date_rsm_approval AS date_rsm_approval,
                    s.is_nsm_approved AS is_nsm_approved,
                    s.date_nsm_approval AS date_nsm_approval,
                    s.is_certified_correct_ar AS is_certified_correct_ar,
                    s.date_ar_approval AS date_ar_approval,
                    s.is_coo_approved AS is_coo_approved,
                    s.date_coo_approval AS date_coo_approval,
                    s.remarks AS remarks,
                    s.request_type AS request_type,
                    s.date_created AS date_created 
            FROM
                    sbrf s 
                    LEFT JOIN users u 
            ON u.id = s.ema_id 
            WHERE 1 = 1 
                    AND s.status = 1 
                    AND s.is_deleted = 0
                    AND s.request_type = 2 
            ORDER BY s.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_donations(){
            $record = array();
            
            $query_string = "
            SELECT 
                    s.id AS id,
                    s.sbrf_no AS sbrf_no,
                    s.ema_id AS ema_id,
                    u.username AS ema_code,
                    s.revenue_net_markup AS revenue_net_markup,
                    s.selling_expenses AS selling_expenses,
                    s.sales_budget_guide AS sales_bugdet_guide,
                    s.unused_balance AS unused_balance,
                    s.sa_id AS sa_id,
                    s.rsm_id AS rsm_id,
                    s.nsm_id AS nsm_id,
                    s.ar_id AS ar_id,
                    s.is_rsm_approved AS is_rsm_approved,
                    s.date_rsm_approval AS date_rsm_approval,
                    s.is_nsm_approved AS is_nsm_approved,
                    s.date_nsm_approval AS date_nsm_approval,
                    s.is_certified_correct_ar AS is_certified_correct_ar,
                    s.date_ar_approval AS date_ar_approval,
                    s.is_coo_approved AS is_coo_approved,
                    s.date_coo_approval AS date_coo_approval,
                    s.remarks AS remarks,
                    s.request_type AS request_type,
                    s.date_created AS date_created 
            FROM
                    sbrf s 
                    LEFT JOIN users u 
            ON u.id = s.ema_id 
            WHERE 1 = 1 
                    AND s.status = 1 
                    AND s.is_deleted = 0
                    AND s.request_type = 3 
            ORDER BY s.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_seminar(){
            $record = array();
            
            $query_string = "
            SELECT 
                    s.id AS id,
                    s.sbrf_no AS sbrf_no,
                    s.ema_id AS ema_id,
                    u.username AS ema_code,
                    s.revenue_net_markup AS revenue_net_markup,
                    s.selling_expenses AS selling_expenses,
                    s.sales_budget_guide AS sales_bugdet_guide,
                    s.unused_balance AS unused_balance,
                    s.sa_id AS sa_id,
                    s.rsm_id AS rsm_id,
                    s.nsm_id AS nsm_id,
                    s.ar_id AS ar_id,
                    s.is_rsm_approved AS is_rsm_approved,
                    s.date_rsm_approval AS date_rsm_approval,
                    s.is_nsm_approved AS is_nsm_approved,
                    s.date_nsm_approval AS date_nsm_approval,
                    s.is_certified_correct_ar AS is_certified_correct_ar,
                    s.date_ar_approval AS date_ar_approval,
                    s.is_coo_approved AS is_coo_approved,
                    s.date_coo_approval AS date_coo_approval,
                    s.remarks AS remarks,
                    s.request_type AS request_type,
                    s.date_created AS date_created 
            FROM
                    sbrf s 
                    LEFT JOIN users u 
            ON u.id = s.ema_id 
            WHERE 1 = 1 
                    AND s.status = 1 
                    AND s.is_deleted = 0
                    AND s.request_type = 4 
            ORDER BY s.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_sbrf(){

            $record  = array();

            $query_string = "
            SELECT
                s.id as id,
                ps.name as process_status,
                s.sbrf_no as sbrf_no,
                s.ema_id as ema_id,
                u.username as ema_code,
                s.total_amount as total_amount,
                s.sa_id AS sa_id,
                s.rsm_id AS rsm_id,
                s.nsm_id AS nsm_id,
                s.ar_id As ar_id,
                s.is_rsm_approved as is_rsm_approved,
                s.date_rsm_approval as date_rsm_approval,
                s.is_nsm_approved as is_nsm_approved,
                s.date_nsm_approval as date_nsm_approval,
                s.is_certified_correct_ar as is_certified_correct_ar,
                s.date_ar_approval as date_ar_approval,
                s.is_coo_approved as is_coo_approved,
                s.date_coo_approval as date_coo_approval,
                s.is_check_processed as is_check_processed,
                s.date_check_process as date_check_process,
                s.check_no as check_no,
                s.voucher_no as voucher_no,
                s.is_one_percent_disc as is_one_percent_disc,
                s.coo_remarks as coo_remarks,
                s.remarks as remarks,
                s.ps_file as ps_file,
                s.donation_file as donation_file,
                s.erf_file as erf_file,
                s.request_type as request_type,
                s.date_created as date_created
            FROM
                sbrf s
            LEFT JOIN
                users u
            ON  u.id = s.ema_id
            LEFT JOIN
                process_status ps
            ON s.process_status = ps.id
            WHERE
                1 = 1                
                AND s.status = 1
                AND s.is_deleted = 0
            ORDER BY
                s.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_sbrf_by_no($sbrf_no = 0){

            $record  = array();

            $query_string = "
            SELECT
                s.id as id,
                ps.name as process_status,
                s.sbrf_no as sbrf_no,
                s.ema_id as ema_id,
                u.username as ema_code,
                CONCAT(i.firstname,' ',i.middlename,' ',i.lastname) as ema_name,
                s.total_amount as total_amount,                
                s.sa_id AS sa_id,
                s.rsm_id as rsm_id,
                s.nsm_id AS nsm_id,
                s.ar_id AS ar_id,
                s.is_rsm_approved as is_rsm_approved,
                s.date_rsm_approval as date_rsm_approval,
                s.is_nsm_approved as is_nsm_approved,
                s.date_nsm_approval as date_nsm_approval,
                s.is_certified_correct_ar as is_certified_correct_ar,
                s.date_ar_approval as date_ar_approval,
                s.is_coo_approved as is_coo_approved,
                s.date_coo_approval as date_coo_approval,
                s.is_check_processed as is_check_processed,
                s.date_check_process as date_check_process,
                s.check_no as check_no,
                s.voucher_no as voucher_no,
                s.is_one_percent_disc as is_one_percent_disc,
                s.coo_remarks as coo_remarks,
                s.remarks as remarks,
                s.ps_file as ps_file,
                s.donation_file as donation_file,
                s.erf_file as erf_file,
                s.request_type as request_type,
                s.date_created as date_created
            FROM
                sbrf s
            LEFT JOIN
                users u
            ON  u.id = s.ema_id
            LEFT JOIN 
                info i
            ON u.id = i.user_id
            LEFT JOIN
                process_status ps
            ON s.process_status = ps.id
            WHERE
                1 = 1                
                AND s.status = 1
                AND s.is_deleted = 0
                AND s.sbrf_no = '{$sbrf_no}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_sbrf_items_by_sbrf_no($item_id = 0){

            $record = array();           

            $query_string = "
            SELECT
                si.id as id,
                si.sbrf_no as sbrf_no,
                si.ema_id as ema_id,
                si.school_code as customer_code,
                cl.customer_name as customer_name,
                si.cytd_exp as cytd_exp,
                si.donation as donation,
                si.incentive as incentive,
                si.seminar as seminar,
                si.evaluate_process as evaluate_process,
                si.client_acct_type,
                si.pay_habit as pay_habit,
                si.payee as payee,
                si.designation as designation,                                
                si.process_remarks as process_remarks,
                si.evaluate_remarks as evaluate_remarks,
                si.colldept_remarks as colldept_reamarks,
                si.total_ar as total_ar,
                si.past_due_ar as past_due_ar,
                si.status as status,
                si.soa_file as soa_file
            FROM    
                sbrf_items si
            LEFT JOIN
                client_list cl
                ON cl.customer_code = si.school_code
            WHERE
                1 = 1
                AND si.is_deleted = 0
                AND si.sbrf_no = '{$item_id}'
            ORDER BY si.sbrf_no
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_sbrf_items_donation_total_by_sbrf_no($sbrf_no = 0){
            $record = array();

            $query_string = "
            SELECT
                SUM(si.donation) as total_donation
            FROM
                sbrf_items si
            WHERE
                1 = 1
                AND si.is_deleted = 0
                AND si.sbrf_no = '{$sbrf_no}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_sbrf_items_incentive_total_by_sbrf_no($sbrf_no = 0){
            $record = array();

            $query_string = "
            SELECT
                SUM(si.incentive) as total_incentive
            FROM
                sbrf_items si
            WHERE
                1 = 1
                AND si.is_deleted = 0
                AND si.sbrf_no = '{$sbrf_no}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_sbrf_items_seminar_total_by_sbrf_no($sbrf_no = 0){
            $record = array();

            $query_string = "
            SELECT
                SUM(si.seminar) as total_seminar
            FROM
                sbrf_items si
            WHERE
                1 = 1
                AND si.is_deleted = 0
                AND si.sbrf_no = '{$sbrf_no}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

    }

?>