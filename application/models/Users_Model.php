<?php

	class Users_Model extends MY_Model {

		function __construct(){
			parent::__construct();
        }

        public function get_user_password_by_id($user_id = 0){
            $record = array();

            $query_string = "
            SELECT
                u.password as password
            FROM
                users u
            WHERE 
                1 = 1
            AND u.id = '{$user_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_userinfo_by_id($user_id = 0){
            $record = array();

            $query_string = "
            SELECT
                i.firstname as firstname,
                i.lastname as lastname,
                p.name as position
            FROM
                users u
            LEFT JOIN info i ON i.user_id = u.id
            LEFT JOIN positions p ON u.position_id = p.id
            WHERE
            1 = 1
            AND u.id = '{$user_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_region_by_ema_id($id = 0){
            $record = array();

            $query_string = "
            SELECT 
                r.name as region
            FROM
                users u 
                LEFT JOIN region r 
                ON r.id = u.region_id 
            WHERE u.id = {$id} 
            ";

            $query = $this->db->query($query_string);
            $record = $query->row_array();

            return $record;
        }

        public function get_rsm_by_ema_id($id = 0){
            $record = array();

            $query_string = "
            SELECT 
                i.user_id AS rsm_id,
                CONCAT(i.firstname,' ',i.lastname) AS rsm_name,
                ua.user_id AS ema_id
            FROM
                users_approver ua 
                LEFT JOIN info i
                ON i.user_id = ua.approver_id 
            WHERE ua.user_id = '{$id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->row_array();

            return $record;
        }
        
        public function get_position_by_user_id($id = 0){
            $record = array();

            $query_string = "
            SELECT 
                u.position_id 
            FROM
                users u 
            WHERE u.id = '{$id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->row_array();

            return $record;
        }
		
		public function get_user_by_id($id = 0, $has_photo = FALSE) {
            if($id == 0){
                return array();
            }

            $query_string = "
            SELECT 
                u.id,
                u.username,
                u.password,
                i.firstname AS firstname,
                i.middlename,
                i.lastname AS lastname,
                u.email,
                u.position_id,
                p.code AS position_code,
                p.name AS position_name,
                u.status 
            FROM
                users u 
            LEFT JOIN positions p 
                ON p.id = u.position_id 
            LEFT JOIN info i
                ON i.user_id = u.id            
            WHERE u.id ='{$id}'";

            $query = $this->db->query($query_string);
            $record = $query->row_array();

            if(array_check($record)){
                $user_info = array();
                if($has_photo) {
                    $user_info = array('user_id' => $record['id'], 'firstname' => $record['firstname'], 'lastname' => $record['lastname']);
                }
            }

            return $record;

        }
		
	}

?>
