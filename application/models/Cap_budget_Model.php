<?php

    class Cap_budget_Model extends MY_Model {
        
        function __construct(){
            parent::__construct();            
        }

        public function get_cap_budget(){
            $record = array();

            $query_string = "
            SELECT 
                cb.id AS id,
                cb.ema_id AS ema_id,
                cb.budget_type AS budget_type,
                cb.budget_name AS budget_name,
                cb.budget_amount AS budget_amount,
                cb.remaining_budget AS remaining_budget,
                cb.year AS year
            FROM
                cap_budget cb 
            ";

            $query = $this->db->query($query_string);

            $record = $query->result_array();

            return $record;

        }

        public function get_remaining_markup_by_ema_id($id = 0){
            $record = array();

            $query_string = "
            SELECT
                cb.ema_id as ema_id,
                cb.budget_type as budget_type,
                cb.remaining_budget as remaining_budget,    
                cb.year as year
            FROM
                cap_budget cb 
            WHERE 1 = 1 
                AND cb.budget_type = 1 
                AND cb.ema_id = '{$id}' 
                AND cb.year = YEAR(NOW())
            ";

            $query = $this->db->query($query_string);

            $record = $query->result_array();

            return $record;

        }

        public function get_remaining_amount_by_ema_id($id = 0,$request_type = 0){
            $record = array();

            $query_string = "
            SELECT
                cb.ema_id as ema_id,
                cb.budget_type as budget_type,
                cb.remaining_budget as remaining_budget,    
                cb.year as year
            FROM
                cap_budget cb 
            WHERE 1 = 1 
                AND cb.budget_type = '{$request_type}' 
                AND cb.ema_id = '{$id}' 
                AND cb.year = YEAR(NOW())
            ";

            $query = $this->db->query($query_string);

            $record = $query->result_array();

            return $record;

        }

        

    }

?>