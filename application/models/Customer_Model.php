<?php

    class Customer_Model extends MY_Model {
        
        function __construct(){
            parent::__construct();
        }

        // getting EMA info by id (used in reports filename identification)

        public function get_ema_info_by_id($id = 0){
            $record = array();
            $query_string = "
            SELECT
                u.id as id,
                u.username as ema_code
            FROM
                users u
            WHERE
                1 = 1
                AND u.position_id = 2
                AND u.id = {$id}
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        // new functions for 'where' queries

        public function get_school_info_by_ema_id($id = 0){
            $record = array();
            $query_string = "
            SELECT
                cl.id as id,
                cl.customer_code as school_code,
                cl.customer_name as school_name,
                cl.address as address
            FROM
                client_list cl
            WHERE
                1 = 1
                AND cl.status = 1
                AND cl.is_deleted = 0
                AND cl.ema_id = {$id}
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }


        ////////////////////////////////////
        
        public function get_school_info(){
            $record = array();
            $query_string = "
            SELECT
                cl.id AS school_id,
                cl.customer_code AS school_code,                
                cl.customer_name AS school_name
            FROM
                client_list cl
            WHERE 
                1 = 1
                AND cl.status = 1
                AND cl.is_deleted = 0
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }



        public function get_sales_net_markup(){
            $record = array();
            $query_string = "
            SELECT 
                snm.school_code school_code,
                cl.customer_name school_name,
                cl.address AS address,
                cl.ema_id as ema_id,
                cl.remarks AS process_evaluate,
                snm.sales_net_markup AS sales_net_markup,
                snm.year AS year
            FROM
                client_list cl 
                INNER JOIN sales_net_markup snm 
                    ON snm.school_code = cl.customer_code 
            ";

            $query = $this->db->query($query_string);

            $record = $query->result_array();

            return $record;

        }

        public function get_client_list(){
            $record = array();

            $query_string = "
            SELECT 
                cl.id AS customer_id,
                cl.customer_code AS customer_code,
                cl.customer_name AS customer_name,
                cl.ema_id,  
                cl.address AS customer_address,                
                cl.remarks,
                cl.status AS STATUS,
                cl.is_deleted AS deleted   
            FROM
                client_list cl
                INNER JOIN users u 
                ON u.id = cl.ema_id   
            WHERE 1 = 1 
                AND cl.is_deleted = 0 
                AND cl.status = 1             
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;           

        }

        public function get_school_by_ema_id($id = 0){
            $recond = array();

            $query_string = "
            SELECT 
                cl.id AS customer_id,                
                cl.customer_name AS customer_name
                 
            FROM
                client_list cl 
                
            WHERE 1 = 1 
                AND cl.is_deleted = 0 
                AND cl.status = 1 
                AND cl.ema_id = {$id}
             
            ";

            //     select:
            //     cl.customer_code AS customer_code,
            //     cl.ema_id,
            //     cl.address AS customer_address,
            //     cl.remarks,
            //     cl.status AS STATUS,
            //     cl.is_deleted AS deleted
            //     from:
            //     INNER JOIN users u 
            //     ON u.id = cl.ema_id 



            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }

        public function get_school_by_ema_id_int($id = 0){
            $recond = array();

            $query_string = "
            SELECT 
                count(*) as int
                 
            FROM
                client_list cl 
                
            WHERE 1 = 1 
                AND cl.is_deleted = 0 
                AND cl.status = 1 
                AND cl.ema_id = {$id}
             
            ";


            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }

        public function get_client_by_id($id = 0){
            $record = array();

            $query_string = "
            SELECT 
                cl.id AS customer_id,
                cl.customer_code AS customer_code,
                cl.customer_name AS customer_name,
                cl.ema_id,  
                cl.address AS customer_address,            
                cl.remarks,
                cl.status AS STATUS,
                cl.is_deleted AS deleted   
            FROM
                client_list cl
                INNER JOIN users u 
                ON u.id = cl.ema_id   
            WHERE 1 = 1 
                AND cl.is_deleted = 0 
                AND cl.status = 1 
                    AND cl.id = {$id}             
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;           

        }

        public function get_client_by_ema_id($id = 0){
            $record = array();

            $query_string = "
            SELECT                
                cl.id as customer_id,
                cl.customer_code as customer_code,
                cl.customer_name as customer_name,
                cl.ema_id,
                cl.address as customer_address,
                cl.status as status,
                cl.is_deleted as deleted 
            FROM
                client_list cl
            INNER JOIN
                users u
            ON u.id = cl.ema_id
            WHERE 1 = 1 
                AND cl.is_deleted = 0 
                AND cl.status = 1
                AND cl.ema_id = {$id}
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;           

        }

         public function get_ema_list_by_id($id = 0){

            $record = array();

            $query_string = "
            SELECT 
                u.id AS ema_id,
                u.username AS ema_code,  
                i.firstname AS firstname,
                i.lastname AS lastname,
                ua.approver_id AS rsm_id,
                r.id AS region_id,
                r.name AS region_name
            FROM
                users u 
                LEFT JOIN info i 
                    ON i.user_id = u.id 
                LEFT JOIN users_approver ua 
                    ON u.id = ua.user_id 
                LEFT JOIN region r
                    ON r.id = u.region_id
            WHERE u.position_id = 2
            AND u.id = {$id}
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }

        public function get_customer_basic(){
            $record = array();
            $query_string = "
            SELECT
                c.id as id,
                c.customer_code as customer_code,
                c.customer_name as customer_name                
            FROM
                customer c
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }

        

        public function get_customer_by_ema_id($id = 0){

            $record = array();            

            $query_string = "
            SELECT                
                c.id as customer_id,
                c.customer_code as customer_code,
                c.customer_name as customer_name,
                c.ema_id,
                c.address as customer_address,
                c.status as status,
                c.is_deleted as deleted 
            FROM
                customer c              
            INNER JOIN
                users u
                ON u.id = c.ema_id
            WHERE 1 = 1 
                AND c.is_deleted = 0 
                AND c.status = 1
                AND c.ema_id = {$id}"            
            ;

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }

        public function get_customer_by_customer_id($id = 0){

            $record = array();            

            $query_string = "
            SELECT                
                c.id as customer_id,
                c.customer_code as customer_code,
                c.customer_name as customer_name,
                c.ema_id,
                c.address as customer_address,
                c.status as status,
                c.is_deleted as deleted 
            FROM
                customer c              
            INNER JOIN
                users u
                ON u.id = c.ema_id
            WHERE 1 = 1 
                AND c.is_deleted = 0 
                AND c.status = 1
                AND c.id = {$id}"            
            ;

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;
            
        }

        // public function get_customer_by_ema(){

        //     $record = array();            

        //     $query_string = "
        //     SELECT                             
        //         c.id as customer_id,
        //         c.customer_code as customer_code,
        //         c.customer_name as customer_name,
        //         c.ema_id,
        //         c.address as customer_address,
        //         c.status as status,
        //         c.is_deleted as deleted 
        //     FROM
        //         customer c              
        //     INNER JOIN
        //         users u
        //         ON u.id = c.ema_id
        //     WHERE 1 = 1 
        //         AND c.is_deleted = 0 
        //         AND c.status = 1
        //     ";

        //     $query = $this->db->query($query_string);
        //     $record = $query->result_array();
        //     return $record;

        // }

        //  public function get_customer(
        //      $id      = array(),
        //      $user_id = array()
        //  ){

        //     $record = array();

        //     $where = '';
            
        //     if(array_check($id)){
		// 		$where .=  ' AND c.id IN ('  .implode(',', $id) .') ';
        //     }
            
		// 	if(array_check($user_id)){
		// 		$where .=  ' AND c.ema_id IN ('  .implode(',', $user_id) .') ';
		// 	}

        //     $query_string = "
        //     SELECT
        //         c.id,
        //         u.id as user_id,                
        //         c.id as customer_id,
        //         c.customer_code as customer_code,
        //         c.customer_name as customer_name,
        //         c.ema_id,
        //         c.address,
        //         c.status,
        //         c.is_deleted 
        //     FROM
        //         customer c              
        //     LEFT JOIN
        //         users u
        //         ON u.id = c.ema_id
        //     WHERE 1 = 1 
        //         AND c.is_deleted = 0 
        //         AND c.status = 1". $where ."
        //     "
        //     ;

        //     $query = $this->db->query($query_string);
        //     $record = $query->result_array();
        //     return $record;

        // }

        public function get_ema_list(){

            $record = array();

            $query_string = "
            SELECT 
                u.id as ema_id,
                u.username as ema_code,
                i.firstname as firstname,
                i.lastname as lastname
            FROM
                users u
            LEFT JOIN
                info i
                ON u.id = i.user_id
            WHERE 1 = 1
                AND u.position_id = 2
                AND u.status = 1
                AND u.is_deleted = 0
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;

        }


    }

?>