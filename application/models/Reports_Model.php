<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_Model extends MY_Model{

    function __construct(){
        parent::__construct();        
    }

    public function cap_budget_markup_report($ema = 0,$year = 0){
        $query_string = "
        SELECT 
            ai.date_check_process,
            ai.total_amount,
            cl.customer_code,
            cl.customer_name,
            u.username,
            ai.voucher_no,
            IF(
                ai.is_one_percent_disc = 1,
                'yes',
                'no'
            ) 
        FROM
            additional_incentives ai 
            INNER JOIN client_list cl 
                ON cl.id = ai.school_id 
            INNER JOIN users u 
                ON u.id = ai.ema_id 
        WHERE 
        1 = 1
        AND ai.is_check_processed = 1 
        ";

        $query = $this->db->query($query_string);

    }

    public function get_mark_up_budget_report(
        $ema = '',
        $from_date = '',
        $to_date = '',
        $is_one_percent_disc = 0,
        $sql_file = ''
    ){
        $query_string = "
        SELECT 
            'MARK UP BUDGET REQUEST REPORT',
            '',
            '',
            '',
            '',
            '',
            '',    
            'DATE',
            NOW() 
        UNION
        ALL 
        SELECT 
            'DATE',
            'SCHOOL CODE',
            'SCHOOL NAME',
            'MARK UP AMOUNT',
            'PAYEE',
            'POSITION',            
            'CHECK NO',
            'VOUCHER NO',
            '1% DISC INC' 
        UNION
        ALL 
        SELECT 
            ai.date_check_process,
            c.customer_code AS customer_code,
            c.customer_name,
            FORMAT(aii.check_amt, 2),
            aii.contact_person,
            aii.position,
            ai.check_no,
            ai.voucher_no,
            IF(
                ai.is_one_percent_disc = 1,
                'yes',
                'no'
            )  
        FROM
            additional_incentives ai 
            INNER JOIN customer c 
                ON c.id = ai.school_id
            INNER JOIN additional_incentives_items aii
                ON aii.aicp_no = ai.aicp_no
        WHERE 1 = 1
            AND aii.is_deleted = 0
            AND ai.is_one_percent_disc = '{$is_one_percent_disc}'
            AND ai.ema_id = '{$ema}'
            AND ai.date_check_process BETWEEN '{$from_date}' AND '{$to_date}'
            
        INTO OUTFILE '".$sql_file."' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'        
        ";

        $query = $this->db->query($query_string);

    }

    public function get_incentive_budget_report(
        $ema = '',
        $from_date = '',
        $to_date = '',
        $is_one_percent_disc = 0,
        $sql_file = ''
    ){
        $query_string = "
        SELECT
            'INCENTIVE BUDGET REQUEST REPORT',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'DATE',
            NOW()
        UNION
        ALL 
        SELECT 
            'DATE',
            'SCHOOL CODE',
            'SCHOOL NAME',
            'INCENTIVE AMT',
            'PAYEE',
            'DESIGNATION',
            'PROCESS/EVALUATE',
            'CHECK NO',
            'VOUCHER NO',
            '1% DISC INC' 
        UNION
        ALL 
        SELECT 
            s.date_check_process,
            si.school_code,
            cl.customer_name,
            FORMAT(si.incentive, 2),
            si.payee,
            si.designation,
            IF(
                si.evaluate_process = 1,
                'process',
                'evaluate'
            ),
            s.check_no,
            s.voucher_no,
            IF(
                s.is_one_percent_disc = 1,
                'yes',
                'no'
            ) 
        FROM
            sbrf_items si 
            INNER JOIN sbrf s 
                ON si.sbrf_no = s.sbrf_no 
            INNER JOIN client_list cl 
                ON cl.id = si.school_id
        WHERE 1 = 1
            AND si.is_deleted = 0
            AND s.request_type = 2
            AND s.is_one_percent_disc = '{$is_one_percent_disc}'
            AND s.ema_id = '{$ema}'
            AND s.date_check_process BETWEEN '{$from_date}' AND '{$to_date}'

        INTO OUTFILE '".$sql_file."' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'        
        ";

        $query = $this->db->query($query_string);
    }

    public function get_donation_budget_report(
        $ema = '',
        $from_date = '',
        $to_date = '',
        $is_one_percent_disc = 0,
        $sql_file = ''
    ){
        $query_string = "
        SELECT
            'DONATION BUDGET REQUEST REPORT',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'DATE',
            NOW()
        UNION
        ALL
        SELECT 
            'DATE',
            'SCHOOL CODE',
            'SCHOOL NAME',
            'DONATION AMT',
            'PAYEE',
            'DESIGNATION',
            'PROCESS/EVALUATE',
            'CHECK NO',
            'VOUCHER NO',
            '1% DISC INC' 
        UNION
        ALL 
        SELECT 
            s.date_check_process,
            si.school_code,
            cl.customer_name,
            FORMAT(si.donation, 2),
            si.payee,
            si.designation,
            IF(
                si.evaluate_process = 1,
                'process',
                'evaluate'
            ),
            s.check_no,
            s.voucher_no,
            IF(
                s.is_one_percent_disc = 1,
                'yes',
                'no'
            ) 
        FROM
            sbrf_items si 
            INNER JOIN sbrf s 
                ON si.sbrf_no = s.sbrf_no 
            INNER JOIN client_list cl 
                ON cl.id = si.school_id
        WHERE 1 = 1
            AND si.is_deleted = 0
            AND s.request_type = 3
            AND s.is_one_percent_disc = '{$is_one_percent_disc}'
            AND s.ema_id = '{$ema}'
            AND s.date_check_process BETWEEN '{$from_date}' AND '{$to_date}'

        INTO OUTFILE '".$sql_file."' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'        
        ";

        $query = $this->db->query($query_string);
    }

    public function get_seminar_budget_report(
        $ema = '',
        $from_date = '',
        $to_date = '',
        $is_one_percent_disc = 0,
        $sql_file = ''
    ){
        $query_string = "
        SELECT
            'SEMINAR BUDGET REQUEST REPORT',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'DATE',
            NOW()
        UNION
        ALL
        SELECT 
            'DATE',
            'SCHOOL CODE',
            'SCHOOL NAME',
            'SEMINAR AMT',
            'PAYEE',
            'DESIGNATION',
            'PROCESS/EVALUATE',
            'CHECK NO',
            'VOUCHER NO',
            '1% DISC INC' 
        UNION
        ALL 
        SELECT 
            s.date_check_process,
            si.school_code,
            cl.customer_name,
            FORMAT(si.seminar, 2),
            si.payee,
            si.designation,
            IF(
                si.evaluate_process = 1,
                'process',
                'evaluate'
            ),
            s.check_no,
            s.voucher_no,
            IF(
                s.is_one_percent_disc = 1,
                'yes',
                'no'
            )
        FROM
            sbrf_items si 
            INNER JOIN sbrf s 
                ON si.sbrf_no = s.sbrf_no 
            INNER JOIN client_list cl 
                ON cl.id = si.school_id
        WHERE 1 = 1
            AND si.is_deleted = 0
            AND s.request_type = 4
            AND s.is_one_percent_disc = '{$is_one_percent_disc}'
            AND s.ema_id = '{$ema}'
            AND s.date_check_process BETWEEN '{$from_date}' AND '{$to_date}'

        INTO OUTFILE '".$sql_file."' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'        
        ";

        $query = $this->db->query($query_string);
    }

}