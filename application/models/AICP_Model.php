<?php

    class AICP_Model extends MY_Model {
        
        function __construct(){
            parent::__construct();            
        }

        public function get_markup_listing_for_cap_by_ema_id($id = 0){
            $record  = array();

            $query_string = "
            SELECT 
                ai.aicp_no as aicp_no,
                ai.date_check_process as date,
                ai.total_amount as amount,
                cl.customer_code school_code,
                cl.customer_name school_name,
                ai.voucher_no as voucher_no,
                ai.check_no as check_no,
                ai.is_one_percent_disc as one_perc_disc,
                ai.remarks as remarks,
                ai.date_created as dt_created
            FROM
                additional_incentives ai 
                LEFT JOIN client_list cl 
                    ON cl.id = school_id
                
            WHERE 1 = 1 
                AND ai.status = 1 
                AND ai.is_check_processed = 1
                AND ai.ema_id = '{$id}'
                AND YEAR(ai.date_check_process) = YEAR(NOW())
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }
        
        public function get_markup_total_with_budget_with_remaining_by_ema_id($id = 0){
            $record = array();

            $query_string = "
            SELECT 
                (SELECT 
                    SUM(ai.total_amount) 
                FROM
                    additional_incentives ai 
                    LEFT JOIN client_list cl 
                        ON cl.id = ai.school_id 
                WHERE 1 = 1 
                    AND ai.status = 1 
                    AND ai.is_check_processed = 1 
                    AND ai.ema_id = '{$id}' 
                    AND YEAR(ai.date_check_process) = YEAR(NOW())) AS mark_up_total,
                (SELECT 
                    SUM(mb.budget_amount) 
                FROM
                    markup_budget mb 
                WHERE 1 = 1 
                    AND mb.ema_id = '{$id}' 
                    AND YEAR(mb.date) = YEAR(NOW())) AS mark_up_budget,
                    (SELECT mark_up_budget-mark_up_total ) AS remaining_amount 
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_total_amount_by_aicp_no($aicp = 0){
            $record = array();

            $query_string = "
            SELECT 
                SUM(aii.check_amt) AS total_amount 
            FROM
                additional_incentives_items aii 
            WHERE 
                1 = 1
            AND aii.is_deleted = 0
            AND aii.aicp_no = '{$aicp}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->row_array();

            return $record;

        }

        public function get_aicp_check_processed(){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS aicp_check_processed
            FROM
                additional_incentives ai 
            WHERE (
                ai.is_cert_correct = 1 AND
                ai.is_rsm_approved = 1 AND
                ai.is_nsm_approved = 1 AND
                ai.is_approved = 1 AND
                ai.is_check_processed = 1
            )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_aicp_check_requests(){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS aicp_check_requests 
            FROM
                additional_incentives ai 
            WHERE (
                ai.is_cert_correct = 1 AND
                ai.is_rsm_approved = 1 AND
                ai.is_nsm_approved = 1 AND
                ai.is_approved = 1 AND
                ai.is_check_processed = 0
            )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function aicp_request_for_rsm_approval_by_rsm_id($rsm_id = 0){
            $record = array();

            $query_string = "
            SELECT 
                COUNT(*) AS aicp_pending_requests 
            FROM
                additional_incentives ai 
            WHERE (
                ai.is_cert_correct = 1 
                AND ai.is_rsm_approved = 0
                ) 
                AND ai.rsm_id = '{$rsm_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function aicp_request_for_approval_by_requestor($requestor = 0){
            $record = array();

            if($requestor == 1){
                $query_string = "
                SELECT 
                    COUNT(*) AS aicp_pending_requests 
                FROM
                    additional_incentives ai 
                WHERE (
                    ai.is_cert_correct = 0                     
                    )
                ";
            }
            
            
            if($requestor == 2){
                $query_string = "
                SELECT 
                    COUNT(*) AS aicp_pending_requests 
                FROM
                    additional_incentives ai 
                WHERE (
                    ai.is_cert_correct = 1 
                    AND ai.is_rsm_approved = 0
                    )
                ";
            }
            
            if($requestor == 3){
                $query_string = "
                SELECT 
                    COUNT(*) AS aicp_pending_requests 
                FROM
                    additional_incentives ai 
                WHERE (
                    ai.is_cert_correct = 1 
                    AND ai.is_rsm_approved = 1 
                    AND ai.is_nsm_approved = 0                     
                    )
                ";
            }

            if($requestor == 4){
                $query_string = "
                SELECT 
                    COUNT(*) AS aicp_pending_requests 
                FROM
                    additional_incentives ai 
                WHERE (
                    ai.is_cert_correct = 1 
                    AND ai.is_rsm_approved = 1 
                    AND ai.is_nsm_approved = 1 
                    AND ai.is_approved = 0
                    )
                ";
            }

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }



        public function all_aicp_pending_requests(){
            $record = array();
            $query_string = "
            SELECT 
                COUNT(*) AS aicp_pending_requests
            FROM 
                additional_incentives ai
            WHERE
                (
                    ai.is_rsm_approved != 2 AND
                    ai.is_nsm_approved !=2 AND
                    ai.is_approved = 0
                )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function all_aicp_approved_requests(){
            $record = array();
            $query_string = "
            SELECT 
                COUNT(*) AS aicp_approved_requests
            FROM 
                additional_incentives ai
            WHERE
                (
                    ai.is_rsm_approved = 1 AND
                    ai.is_nsm_approved = 1 AND
                    ai.is_approved = 1
                )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function all_aicp_disapproved_requests(){
            $record = array();
            $query_string = "
            SELECT 
                COUNT(*) AS aicp_disapproved_requests
            FROM 
                additional_incentives ai
            WHERE
                (
                    ai.is_rsm_approved = 2 OR
                    ai.is_nsm_approved = 2 OR
                    ai.is_approved = 2
                )
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_aicp(){

            $record  = array();

            $query_string = "
            SELECT
                ai.id as id,
                ps.name as process_status,
                ai.aicp_no as aicp_no,
                ai.ema_id as ema_id,
                ai.sa_id as sa_id,
                ai.rsm_id as rsm_id,
                u.username as ema_code,
                ai.school_id as school_id,
                ai.total_amount as total_amount,
                ai.is_rsm_approved as is_rsm_approved,
                ai.date_rsm_approval as date_rsm_approval,
                ai.is_nsm_approved as is_nsm_approved,
                ai.date_nsm_approval as date_nsm_approval,
                ai.is_checked as is_checked,
                ai.date_checked as date_checked,
                ai.is_cert_correct as is_cert_correct,
                ai.date_corrected as date_corrected,
                ai.is_approved as is_approved,
                ai.date_approved as date_approved,
                ai.is_check_processed as is_check_processed,
                ai.date_check_process as date_check_process,
                ai.check_no as check_no,
                ai.voucher_no as voucher_no,
                ai.is_one_percent_disc,
                ai.coo_remarks as coo_remarks,
                ai.remarks as remarks,                
                ai.status as status,
                ai.markup_file as markup_file,                
                ai.is_deleted as is_deleted,
                ai.date_created as date_created
            FROM
                additional_incentives ai
            LEFT JOIN users u
            ON u.id = ai.ema_id
            LEFT JOIN process_status ps
            ON ps.id = ai.process_status
            WHERE
                1 = 1
            ORDER BY
                ai.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_aicp_by_no($aicp_no = 0){

            $record  = array();

            $query_string = "
            SELECT
                ai.id as id,
                ps.name as process_status,
                ai.aicp_no as aicp_no,
                ai.ema_id as ema_id,
                u.username as ema_code,
                ai.sa_id as sa_id,                
                ai.rsm_id as rsm_id,
                ai.nsm_id as nsm_id,
                ai.ar_id as ar_id,
                ai.region_id as region_id,
                CONCAT(i.firstname,' ',i.lastname) as ema_name,
                cl.customer_code as customer_code,
                cl.customer_name as customer_name,                
                cl.address as address,
                ai.total_amount as total_amount,
                ai.is_rsm_approved as is_rsm_approved,
                ai.date_rsm_approval as date_rsm_approval,
                ai.is_nsm_approved as is_nsm_approved,
                ai.date_nsm_approval as date_nsm_approval,
                ai.is_checked as is_checked,
                ai.date_checked as date_checked,
                ai.is_cert_correct as is_cert_correct,
                ai.date_corrected as date_corrected,
                ai.is_approved as is_approved,
                ai.date_approved as date_approved,
                ai.is_check_processed as is_check_processed,
                ai.date_check_process as date_check_process,
                ai.check_no as check_no,
                ai.voucher_no as voucher_no,
                ai.is_one_percent_disc,
                ai.coo_remarks as coo_remarks,                
                ai.remarks as remarks,
                ai.status as status,
                ai.markup_file as markup_file,                
                ai.is_deleted as is_deleted,
                ai.date_created as date_created
            FROM
                additional_incentives ai
            LEFT JOIN users u            
            ON u.id = ai.ema_id
            LEFT JOIN process_status ps
            ON ps.id = ai.process_status
            LEFT JOIN client_list cl
            ON ai.school_id = cl.id
            LEFT JOIN info i
            ON ai.ema_id = i.user_id
            
            WHERE
                1 = 1
            AND ai.aicp_no = '{$aicp_no}'
            ORDER BY
                ai.date_created
            DESC
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_aicp_items_by_aicp_no($item_id = 0){

            $record = array();            

            $query_string = "
            SELECT
                aii.id as id,
                aii.aicp_no as aicp_no,
                aii.contact_person as contact_person,
                aii.position as position,
                aii.acct_no as acct_no,
                aii.bank_name as bank_name,
                aii.atm_no as atm_no,
                aii.check_amt as check_amt,
                aii.cash_amt as cash_amt,
                aii.cash_amt + aii.check_amt as total,
                aii.remarks as remarks
            FROM
                additional_incentives_items aii
            WHERE
                1 = 1
                AND aii.is_deleted = 0                
                AND aii.aicp_no = '{$item_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

        public function get_aicp_items_grand_total_amt_by_aicp_no($item_id = 0){
            $record = array();
            $query_string = "
            SELECT
                SUM(aii.check_amt) + SUM(aii.cash_amt) as grand_total
            FROM
                additional_incentives_items aii
            WHERE
                1 = 1
                AND aii.is_deleted = 0
                AND aii.aicp_no = '{$item_id}'
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;

        }

        public function get_aicp_items(){
            
            $record = array();
            
            $item_id = 0;
            
            $query_string = "
            SELECT
                aii.id as id,
                aii.aicp_no as aicp_no,
                aii.contact_person as contact_person,
                aii.position as position,
                aii.acct_no as acct_no,
                aii.bank_name as bank_name,
                aii.atm_no as atm_no,
                aii.check_amt as check_amt,
                aii.cash_amt as cash_amt,
                aii.remarks as remarks
            FROM
                additional_incentives_items aii
            WHERE
                1 = 1
                AND aii.is_deleted = 0                            
            ";
            
            $query = $this->db->query($query_string);
            $record = $query->result_array();
            return $record;
        }

    }

?>