<?php

    class PS_Model extends MY_Model {
        
        function __construct(){
            parent::__construct();
            
        }

        public function get_project_synopsis(){

            $record  = array();

            $query_string = "
            SELECT
                ps.id as id,
                ps.ema_code as ema_code,
                ps.region_id as region_id,
                ps.total_amount as total_amount,
                ps.rsm_id as rsm_id,
                ps.is_endorsed as is_endorsed,
                ps.date_approved as date_approved,
                ps.status as status,
                ps.date_created as date_created
            FROM
                project_synopsis ps
            WHERE
                1 = 1
            ";

        }

        public function get_project_synopsis_items($item_id){

            $record = array();

            $item_id = 0;

            $query_string = "
            SELECT
                psi.id as id,
                psi.project_synopsis_id as project_synopsis_id,
                psi.date_target as date_target,
                psi.head_count as head_count,
                psi.school_name as school_name,
                psi.recipient_client as recipient_client,
                psi.proposed_project as proposed_project,
                psi.amount as amount                
            FROM
                project_synopsis_items psi
            WHERE
                1 = 1                       
                AND psi.project_synopsis_id = {$item_id}
            ";

            $query = $this->db->query($query_string);
            $record = $query->result_array();

            return $record;
        }

    }

?>