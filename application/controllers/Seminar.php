<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seminar extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
		$this->load->helper(array('form','url'));
		date_default_timezone_set('Asia/Manila');
	}
	
	public function index(){
		$data = array();
		
		$user_id = $this->session->userdata('user_id');

		$data['user']	= $this->Users->get_user_by_id($user_id, TRUE);
		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);        
		$data['ema'] = $this->Customer->get_ema_list();        
		// $data['customer'] = $this->Customer->get_client_list();        
		$data['sbrf_no'] = 'SBRF-'.date('Y-md-His',time());
		$data['javascripts'] = array('seminar');
		$data['css'] = array('style');

		$options = array(
			'page'		=> 'seminar/index',
			'params'	=> $data,
			'page_title'=> 'add seminar request',
			'main_page' => 'create_request',
			'sub_page'  => 'seminar'
		);

		$this->render_page($options);

	}	

	// function in getting EMA information via dropdown event

	public function ema_details_by_id(){
        if($this->input->post('ema_id')){

            $data = array();
            $data['success'] = false;
            $ema_id = 0;

            if(is_numeric($this->input->post('ema_id'))){
                $ema_id = $this->input->post('ema_id');
            }

            $ema = $this->Customer->get_ema_list_by_id($ema_id);
            $data['ema_id'] = $ema_id;
            $rsm_id = $ema[0]['rsm_id'];
			$rsm = $this->Users->get_rsm_by_ema_id($ema_id);
			$data['rsm_id'] = $ema[0]['rsm_id'];
            $data['firstname'] = $ema[0]['firstname'];
            $data['lastname'] = $ema[0]['lastname'];
            $data['region_id'] = $ema[0]['region_id'];
			$data['region_name'] = $ema[0]['region_name'];
			$data['rsm_name'] = $rsm['rsm_name'];
			$data['success'] = true;
			
			$data['school'] = $this->Customer->get_school_by_ema_id($ema_id);

            echo json_encode($data);

        }else{
            show_404();
        }		
    }

	 // function in getting customer or school information via dropdown event

	 public function customer_details_by_id(){        
        if($this->input->post('customer_id')){

            $data = array();
            $data['success'] = false;
            $customer_id = 0;

            $client_list = array();
            
            if(is_numeric($this->input->post('customer_id'))){
				$customer_id = $this->input->post('customer_id');
			}

            $client_list = $this->Customer->get_client_by_id($customer_id);            
            $data['customer_code'] = $client_list[0]['customer_code'];
            $data['customer_name'] = $client_list[0]['customer_name'];
            $data['customer_address'] = $client_list[0]['customer_address'];            
            $data['remarks'] = $client_list[0]['remarks'];
            $data['success'] = true;

            echo json_encode($data);

        } else {
            show_404();
        }
	}
	
	// adding an item from sbrf item table

    public function add_sbrf_item(){
		// loading upload library for file uploading function
        $this->load->library('upload');
        if($this->input->post()){

            $user_id = $this->session->userdata('user_id');

            $i = array();            

            $i['sbrf_no'] = $this->input->post('sbrf_control_no');
			$i['ema_id'] = $this->input->post('ema_id');
			$i['school_id'] = $this->input->post('customer_id');
			$i['school_code'] = $this->input->post('customer_code');			
            $i['cytd_exp'] = 0;

            $i['donation'] = 0;
            $i['incentive'] = 0;
            $i['seminar'] = $this->input->post('seminar');

            $evaluate_process = $this->input->post('evaluate_process');

            if($evaluate_process == 'evaluate'){
                $i['evaluate_process'] = 0;
                $i['evaluate_remarks'] = $this->input->post('remarks');
            }

            if($evaluate_process == 'process'){
                $i['evaluate_process'] = 1;
                $i['process_remarks'] = $this->input->post('remarks');
            }
            
            $i['payee'] = $this->input->post('payee');
			$i['designation'] = $this->input->post('designation');
			
			if(isset($_FILES['soa_file']) AND $_FILES['soa_file']['size'] > 0){
				$path = './assets/soa_files/';
				$file = 'soa_file';
				$return['upload'] = $this->SBRF->upload_soa_file($file, $path);

				if(isset($return['upload']['error'])){
					$return['success'] = false;
					$return['message'] = $return['upload']['error'];

					echo json_encode($return);
					exit;
				}

				$i['soa_file'] = $return['upload']['data']['file_name'];

			} else {
				$i['soa_file'] = '';
			}


            $i['status'] = 0;
            $i['is_deleted'] = 0;

            $this->SBRF->insert('sbrf_items',$i);

            $return['success'] = true;
			echo json_encode($return);            
			
			// kprint($i);exit;

        } else {
            show_404();
        }
	}
	
	// deleting an item from sbrf item table

    public function delete_item_by_id($id = 0){
        $where['id'] = $id;
        $this->SBRF->update_where_many('sbrf_items',array('is_deleted' => 1),$where);
        $return['success'] = true;
        echo json_encode($return);
	}
	
	// saving sbrf seminar budget request

    public function save_seminar_budget_request(){
		// loading upload library for file uploading function
        $this->load->library('upload');
        if($this->input->post()){
            
            $user_id = $this->session->userdata('user_id');

            $i = array();
            $return = array();

			$sbrf_no = $this->input->post('sbrf_control_no');
			
			$total_amount = $this->SBRF->get_total_amount_by_sbrf_no($sbrf_no,2);

			$i['sa_id'] = $user_id;
			$i['rsm_id'] = $this->input->post('rsm_id');
			$i['region_id'] = $this->input->post('region_id');

			$i['process_status'] = $this->input->post('process_status');

			$i['nsm_id'] = 38;
			$i['ar_id'] = 100;

            $i['sbrf_no'] = $this->input->post('sbrf_control_no');
            $i['ema_id'] = $this->input->post('ema_id');
			$i['request_type'] = 4;
			
			$i['total_amount'] = $total_amount['total_amount'];

            $i['is_one_percent_disc'] = 0;

            $i['is_certified_correct_ar'] = 0;
            $i['is_rsm_approved'] = 0;
            $i['is_nsm_approved'] = 0;
			$i['is_coo_approved'] = 0;
			
			$i['is_check_processed'] = 0;

			if(isset($_FILES['erf_file']) AND $_FILES['erf_file']['size'] > 0){
				$path = './assets/erf_files/';
				$file = 'erf_file';
				$return['upload'] = $this->AICP->upload_erf_file($file, $path);

				if(isset($return['upload']['error'])){
					$return['success'] = false;
					$return['message'] = $return['upload']['error'];

					echo json_encode($return);
					exit;
				}

				$i['erf_file'] = $return['upload']['data']['file_name'];

			} else {
				$i['erf_file'] = '';
			}

            $i['status']  = 1;
            $i['is_deleted'] = 0;            
            $i['date_created'] = date('Y-m-d H:i:s',time());

            $this->SBRF->insert('sbrf',$i);            

            $where['sbrf_no'] = $sbrf_no;
            $where['is_deleted'] = 0;
            $this->SBRF->update_where_many('sbrf_items',array('status' => 1),$where);

            $return['success'] = true;
            echo json_encode($return);

        } else {
            show_404();
        }
        
    }

    // for seminar budget request approvals

	public function ar_cert_correct_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_certified_correct_ar'] = 1;
		$i['date_ar_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function rsm_approve_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_rsm_approved'] = 1;
		$i['date_rsm_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function rsm_disapprove_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_rsm_approved'] = 2;
		$i['remarks'] = $this->input->post('remarks');
		$i['date_rsm_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function nsm_approve_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_nsm_approved'] = 1;
		$i['date_nsm_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function nsm_disapprove_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_nsm_approved'] = 2;
		$i['remarks'] = $this->input->post('remarks');
		$i['date_nsm_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function coo_approve_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_coo_approved'] = 1;
		$i['coo_remarks'] = $this->input->post('coo-remarks');
		$i['date_coo_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	public function coo_disapprove_seminar($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');
		$i = array();
		$i['is_coo_approved'] = 2;
		$i['coo_remarks'] = $this->input->post('coo-remarks');
		$i['remarks'] = $this->input->post('remarks');
		$i['date_coo_approval'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;
		$this->SBRF->update_where_many('sbrf',$i,$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	// public function process_check($sbrf_no = 0){
	// 	date_default_timezone_set('Asia/Manila');		
	// 	$i = array();
	// 	$i['is_check_processed'] = 1;
	// 	$i['check_no'] = $this->input->post('check_no');
	// 	$i['voucher_no'] = $this->input->post('voucher_no');
	// 	$i['date_check_process'] = date('Y-m-d H:i:s',time());
	// 	$where['sbrf_no'] = $sbrf_no;
	// 	$this->SBRF->update_where_many('sbrf',$i,$where);
	// 	$return['success'] = true;
	// 	echo json_encode($return);
	// }

	public function process_check($sbrf_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$j = array();

		// for updating check status
		
		$i['is_check_processed'] = 1;
		$i['check_no'] = $this->input->post('check_no');
		$i['voucher_no'] = $this->input->post('voucher_no');
		$i['date_check_process'] = date('Y-m-d H:i:s',time());
		$where['sbrf_no'] = $sbrf_no;

		// for updating cap budget by ema id

		$requested_amount = $this->input->post('requested_amount');
		$current_budget = $this->input->post('remaining_budget');
		$remaining_budget = $current_budget - $requested_amount;
		$j['remaining_budget'] = $remaining_budget;
		
		$where_cap['ema_id'] = $this->input->post('ema_id');
		$where_cap['budget_type'] = $this->input->post('request_type');


		$this->SBRF->update_where_many('sbrf',$i,$where);

		$this->Cap_budget->update_where_many('cap_budget',$j,$where_cap);
		
		$return['success'] = true;
		echo json_encode($return);
	}
    
}