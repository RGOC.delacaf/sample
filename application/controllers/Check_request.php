<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check_request extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}

	public function index(){

		$data = array();	

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);
		
		$data['mark_up_budget'] = $this->AICP->get_aicp();
		$data['donation'] = $this->SBRF->get_sbrf();
		$data['incentive'] = $this->SBRF->get_sbrf();
		$data['seminar'] = $this->SBRF->get_sbrf();
		$data['discretionary'] =  $this->SBRF->get_sbrf();		

		$data['javascripts'] = array('check_request');

		$options = array(
			'page'		=> 'check_request/index',
			'params'	=> $data,
			'page_title'=> 'Check Request',
			'main_page' => 'check_request',
            'sub_page'  => ''
		);

		$this->render_page($options);

		// kprint($data);exit;

	}	

	public function mark_up_budget_request_view($aicp_no = 0){

		$data = array();

		$user_id = $this->session->userdata('user_id');		
		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$ema = $this->AICP->get_aicp_by_no($aicp_no);
		
		$data['rsm'] = $this->Users->get_rsm_by_ema_id($ema[0]['ema_id']);
		$data['region'] = $this->Users->get_region_by_ema_id($ema[0]['ema_id']);
		
		$data['mark_up_budget'] = $this->AICP->get_aicp_by_no($aicp_no);
		$mark_up = $this->AICP->get_aicp_by_no($aicp_no);

		$data['sa_info'] = $this->Users->get_userinfo_by_id($mark_up[0]['sa_id']);		
		$data['rsm_info'] = $this->Users->get_userinfo_by_id($mark_up[0]['rsm_id']);
		$data['nsm_info'] = $this->Users->get_userinfo_by_id($mark_up[0]['nsm_id']);
		$data['ar_info'] = $this->Users->get_userinfo_by_id($mark_up[0]['ar_id']);

		$data['mark_up_budget_item'] = $this->AICP->get_aicp_items_by_aicp_no($aicp_no);
		$data['mark_up_budget_grand_total_amt'] = $this->AICP->get_aicp_items_grand_total_amt_by_aicp_no($aicp_no);

		$data['cap_mark_up'] = $this->AICP->get_markup_listing_for_cap_by_ema_id($ema[0]['ema_id']);
		$data['total_remaining_mark_up'] = $this->AICP->get_markup_total_with_budget_with_remaining_by_ema_id($ema[0]['ema_id']);

		$data['ema_id'] = $ema[0]['ema_id'];
		$data['remaining_markup'] = $this->Cap_budget->get_remaining_markup_by_ema_id($ema[0]['ema_id']);

		$data['javascripts'] = array('mark_up_budget');
		$data['css'] = array('check_request');
		
		// kprint($data);exit;

		$options = array(
			'page' 			=>	'check_request/mark_up_budget_request_view',
			'params'		=>	$data,
			'page_title'	=>	'Check Request',
			'main_page'		=>	'check_request',
			'sub_page'		=>	$aicp_no
		);

		$this->render_page($options);

	}

	public function donation_request_view($sbrf_no = 0){

		$data = array();

		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$ema = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['rsm'] = $this->Users->get_rsm_by_ema_id($ema[0]['ema_id']);
		$data['region'] = $this->Users->get_region_by_ema_id($ema[0]['ema_id']);
		
		$data['sbrf'] = $this->SBRF->get_sbrf_by_no($sbrf_no);
		$sbrf = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['sa_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['sa_id']);
		$data['rsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['rsm_id']);
		$data['nsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['nsm_id']);
		$data['ar_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['ar_id']);
		
		$data['sbrf_item'] = $this->SBRF->get_sbrf_items_by_sbrf_no($sbrf_no);
		$data['donation_total'] = $this->SBRF->get_sbrf_items_donation_total_by_sbrf_no($sbrf_no);
		
		$data['cap_donation'] = $this->SBRF->get_total_sbrf_listing_for_cap_by_ema_id($ema[0]['ema_id'],3);
		$data['total_remaining_donation'] = $this->SBRF->get_total_sbrf_with_budget_with_remaining_by_ema_id($ema[0]['ema_id'],3);

		$data['ema_id'] = $ema[0]['ema_id'];
		$data['remaining_donation'] = $this->Cap_budget->get_remaining_amount_by_ema_id($ema[0]['ema_id'],3);

		$data['javascripts'] = array('donations');
		$data['css'] = array('check_request');	
		

		$options = array(
			'page' 			=> 'check_request/donation_request_view',
			'params' 		=> $data,
			'page_title'	=> 'Check Request',
			'main_page'		=> 'check_request',
			'sub_page'		=> $sbrf_no
		);		

		$this->render_page($options);
	}

	public function incentive_request_view($sbrf_no = 0){
		
		$data = array();

		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$ema = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['rsm'] = $this->Users->get_rsm_by_ema_id($ema[0]['ema_id']);
		$data['region'] = $this->Users->get_region_by_ema_id($ema[0]['ema_id']);

		
		$data['sbrf'] = $this->SBRF->get_sbrf_by_no($sbrf_no);
		$sbrf = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['sa_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['sa_id']);
		$data['rsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['rsm_id']);
		$data['nsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['nsm_id']);
		$data['ar_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['ar_id']);
		
		$data['sbrf_item'] = $this->SBRF->get_sbrf_items_by_sbrf_no($sbrf_no);
		$data['incentive_total'] = $this->SBRF->get_sbrf_items_incentive_total_by_sbrf_no($sbrf_no);
		
		$data['cap_incentive'] = $this->SBRF->get_total_sbrf_listing_for_cap_by_ema_id($ema[0]['ema_id'],2);
		$data['total_remaining_incentive'] = $this->SBRF->get_total_sbrf_with_budget_with_remaining_by_ema_id($ema[0]['ema_id'],2);

		$data['ema_id'] = $ema[0]['ema_id'];
		$data['remaining_incentive'] = $this->Cap_budget->get_remaining_amount_by_ema_id($ema[0]['ema_id'],2);

		$data['javascripts'] = array('incentives');
		$data['css'] = array('check_request');
		
		// kprint($data);exit;		

		$options = array(
			'page' 			=> 'check_request/incentive_request_view',
			'params' 		=> $data,
			'page_title'	=> 'Check Request',
			'main_page'		=> 'check_request',
			'sub_page'		=> $sbrf_no
		);		

		$this->render_page($options);
	}

	public function seminar_request_view($sbrf_no = 0){
		
		$data = array();

		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$ema = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['rsm'] = $this->Users->get_rsm_by_ema_id($ema[0]['ema_id']);
		$data['region'] = $this->Users->get_region_by_ema_id($ema[0]['ema_id']);
		
		$data['sbrf'] = $this->SBRF->get_sbrf_by_no($sbrf_no);
		$sbrf = $this->SBRF->get_sbrf_by_no($sbrf_no);

		$data['sa_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['sa_id']);
		$data['rsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['rsm_id']);
		$data['nsm_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['nsm_id']);
		$data['ar_info'] = $this->Users->get_userinfo_by_id($sbrf[0]['ar_id']);
		
		$data['sbrf_item'] = $this->SBRF->get_sbrf_items_by_sbrf_no($sbrf_no);
		$data['seminar_total'] = $this->SBRF->get_sbrf_items_seminar_total_by_sbrf_no($sbrf_no);
		
		$data['cap_seminar'] = $this->SBRF->get_total_sbrf_listing_for_cap_by_ema_id($ema[0]['ema_id'],4);
		$data['total_remaining_seminar'] = $this->SBRF->get_total_sbrf_with_budget_with_remaining_by_ema_id($ema[0]['ema_id'],4);

		$data['ema_id'] = $ema[0]['ema_id'];
		$data['remaining_seminar'] = $this->Cap_budget->get_remaining_amount_by_ema_id($ema[0]['ema_id'],4);

		$data['javascripts'] = array('seminar');
		$data['css'] = array('check_request');
		
		// kprint($data);exit;		

		$options = array(
			'page' 			=> 'check_request/seminar_request_view',
			'params' 		=> $data,
			'page_title'	=> 'Check Request',
			'main_page'		=> 'check_request',
			'sub_page'		=> $sbrf_no
		);		

		$this->render_page($options);
	}

}
