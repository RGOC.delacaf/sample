<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller {

    function __construct(){
        parent::__construct();
        parent::_verify_user_authentication();       
    }
}
