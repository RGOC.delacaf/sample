<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}

	public function index(){

		$data = array();	

		$user_id = $this->session->userdata('user_id');

        $data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

        // kprint($data);exit;
        
        $data['javascripts'] = array('user');
        $data['css'] = array('user');

		$options = array(
			'page'		=> 'user/index',
			'params'	=> $data,
			'page_title'=> 'User Settings',
			'main_page' => 'user',
            'sub_page'  => ''
		);	

		$this->render_page($options);

	}
	
	public function save_info(){

		if($this->input->post()){

			$return = array();
			$return['success']  = false;

			$user_id = $this->input->post('user_id');
			
			$user_info = $this->Users->get_user_password_by_id($user_id);

			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');

			$current_password = $user_info[0]['password'];
			
			if($current_password == md5($old_password)){
				$return['success']  = true;
				$u['password'] = md5($new_password);
				$this->Users->update('users',$user_id,$u);
			} else {
				$return['message'] = 'incorrect old password';
			}
			// $u['password'] = md5($new_paswword);

			// $this->Users->update('users', $user_id, $u);		

			// kprint($old_password);exit;

			echo json_encode($return);
		}else{
			show_404();
		}

	}

}