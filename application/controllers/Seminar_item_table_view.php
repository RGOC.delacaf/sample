<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seminar_item_table_view extends MY_Controller {
	
	public function index($id = 0)
	{
        $this->load->model('SBRF_Model');
        $data['sbrf_item'] =  $this->SBRF->get_sbrf_items_by_sbrf_no($id);
		$this->load->view('seminar_item_table_view/index',$data);
	}
}