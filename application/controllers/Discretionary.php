<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discretionary extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}
	
	public function index(){
		$data = array();
		
				$user_id = $this->session->userdata('user_id');
		
				$data['user']	= $this->Users->get_user_by_id($user_id, TRUE);
		
		
				$data['javascripts'] = array('discretionary');
		
				$options = array(
					'page'		=> 'discretionary/index',
					'params'	=> $data,
					'page_title'=> 'add %1 discretionary fund',
					'main_page' => 'create_request',
					'sub_page'  => 'discretionary'
				);
		
				$this->render_page($options);

	}
    
}