<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_list_view extends MY_Controller {
	
	public function index($id = 0)
	{
        $this->load->model('Customer_Model');
        $data['client_list'] =  $this->Customer->get_client_list_by_ema_id($id);
		$this->load->view('client_list_view/index',$data);
	}
}