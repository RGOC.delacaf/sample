<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aicp_item_table_view extends MY_Controller {
	
	public function index($id = 0)
	{
        $this->load->model('AICP_Model');
        $data['aicp_item'] =  $this->AICP->get_aicp_items_by_aicp_no($id);
		$this->load->view('aicp_item_table_view/index',$data);
	}
}