<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}

	public function index(){

		$data = array();	

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);
		
		$data['ema'] = $this->Customer->get_ema_list();
        
        $data['javascripts'] = array('reports');
        $data['css'] = array('reports');

		$options = array(
			'page'		=> 'reports/index',
			'params'	=> $data,
			'page_title'=> 'Reports',
			'main_page' => 'reports',
            'sub_page'  => ''
		);

		$this->render_page($options);

	}

	public function ema_details_by_id(){
        if($this->input->post('ema_id')){

            $data = array();
            $data['success'] = false;
            $ema_id = 0;

            if(is_numeric($this->input->post('ema_id'))){
                $ema_id = $this->input->post('ema_id');
            }

            $ema = $this->Customer->get_ema_list_by_id($ema_id);
            $data['ema_id'] = $ema_id;
            $rsm_id = $ema[0]['rsm_id'];
			$rsm = $this->Users->get_rsm_by_ema_id($ema_id);
			$data['rsm_id'] = $ema[0]['rsm_id'];
            $data['firstname'] = $ema[0]['firstname'];
            $data['lastname'] = $ema[0]['lastname'];
            $data['region_id'] = $ema[0]['region_id'];
			$data['region_name'] = $ema[0]['region_name'];
			$data['rsm_name'] = $rsm['rsm_name'];
            $data['success'] = true;

            echo json_encode($data);

        }else{
            show_404();
        }		
	}
	
	public function generate_report(){
		if($this->input->post()){
			$return = array();

			if($this->input->post('reports') == 1){
				$return = $this->mark_up_budget_report($this->input->post());
			}

			if($this->input->post('reports') == 2){
				$return = $this->incentive_budget_report($this->input->post());
			}

			if($this->input->post('reports') == 3){
				$return = $this->donation_budget_report($this->input->post());
			}

			if($this->input->post('reports') == 4){
				$return = $this->seminar_budget_report($this->input->post());
			}
			

			echo json_encode($return);

		}else{
			show_404();
		}

	}

	public function cap_budget_mark_up_report(){
		$return['sql_url'] = addslashes(FCPATH).'//assets//csv_files//';
		$return['file'] = 'cap_budget_markup_report_'.strtotime(date("y-m-d H:i:s")).".csv";
		$return['url']  = base_url().'assets/csv_files/'/$return['file'];

		$sql_file = $return['sql_url'].$return['file'];			
		$this->Reports->get_mark_up_budget_report();

		$return['success'] = true;

		return $return;
	}

	public function mark_up_budget_report($post){

		$ema		= $post['ema_id'];
		$from_date	= $post['from_date'];
		$to_date	= $post['to_date'];
		$is_one_percent_disc = $post['one_percent_disc'];

		$ema_info = $this->Customer->get_ema_info_by_id($ema);

		$return['success'] = false;

		$from	= date('Y-m-d',strtotime($from_date));
		$to		= date('Y-m-d',strtotime($to_date));

		if($from <= $to){
			$return['sql_url'] = addslashes(FCPATH).'//assets//csv_files//';
			// $return['sql_url'] = FCPATH.;
			$return['file'] = 'mark_up_budget_'.$ema_info[0]['ema_code'].'-'.strtotime(date("y-m-d H:i:s")).".csv";
			$return['url'] = base_url().'assets/csv_files/'.$return['file'];
	
			$sql_file = $return['sql_url'].$return['file'];			
			$this->Reports->get_mark_up_budget_report($ema,$from,$to,$is_one_percent_disc,$sql_file);

			$return['success'] = true;

		}

		return $return;

	}

	public function incentive_budget_report($post){

		$ema		= $post['ema_id'];
		$from_date	= $post['from_date'];
		$to_date	= $post['to_date'];
		$is_one_percent_disc = $post['one_percent_disc'];

		$return['success'] = false;

		$from	= date('Y-m-d',strtotime($from_date));
		$to		= date('Y-m-d',strtotime($to_date));

		if($from <= $to){
			$return['sql_url'] = addslashes(FCPATH).'//assets//csv_files//';			
			$return['file'] = 'incentive_budget_report_'.strtotime(date("y-m-d H:i:s")).".csv";
			$return['url'] = base_url().'assets/csv_files/'.$return['file'];
	
			$sql_file = $return['sql_url'].$return['file'];			
			$this->Reports->get_incentive_budget_report($ema,$from,$to,$is_one_percent_disc,$sql_file);

			$return['success'] = true;

		}
		
		return $return;

	}

	public function donation_budget_report($post){
		
		$ema		= $post['ema_id'];
		$from_date	= $post['from_date'];
		$to_date	= $post['to_date'];
		$is_one_percent_disc = $post['one_percent_disc'];

		$return['success'] = false;

		$from	= date('Y-m-d',strtotime($from_date));
		$to		= date('Y-m-d',strtotime($to_date));

		if($from <= $to){
			$return['sql_url'] = addslashes(FCPATH).'//assets//csv_files//';			
			$return['file'] = 'donation_budget_report_'.strtotime(date("y-m-d H:i:s")).".csv";
			$return['url'] = base_url().'assets/csv_files/'.$return['file'];
	
			$sql_file = $return['sql_url'].$return['file'];			
			$this->Reports->get_donation_budget_report($ema,$from,$to,$is_one_percent_disc,$sql_file);

			$return['success'] = true;

		}
		
		return $return;

	}

	public function seminar_budget_report($post){
		
		$ema		= $post['ema_id'];
		$from_date	= $post['from_date'];
		$to_date	= $post['to_date'];
		$is_one_percent_disc = $post['one_percent_disc'];

		$return['success'] = false;

		$from	= date('Y-m-d',strtotime($from_date));
		$to		= date('Y-m-d',strtotime($to_date));

		if($from <= $to){
			$return['sql_url'] = addslashes(FCPATH).'//assets//csv_files//';			
			$return['file'] = 'seminar_budget_report_'.strtotime(date("y-m-d H:i:s")).".csv";
			$return['url'] = base_url().'assets/csv_files/'.$return['file'];
	
			$sql_file = $return['sql_url'].$return['file'];			
			$this->Reports->get_seminar_budget_report($ema,$from,$to,$is_one_percent_disc,$sql_file);

			$return['success'] = true;

		}
		
		return $return;

	}



}
