<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School_info extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}

	public function add(){
		
		$data = array();	

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$data['school_info'] = $this->Customer->get_school_info();

		$data['ema'] = $this->Customer->get_ema_list();
		
		$data['sales_net_markup'] = $this->Customer->get_sales_net_markup();
		
		$data['javascripts'] = array('school_info');
		$data['css'] = array('school_info');

		$options = array(
			'page'		=> 'school_info/add',
			'params'	=> $data,
			'page_title'=> 'add',
			'main_page' => 'school_info',
			'sub_page'  => 'add'
		);

		// kprint(json_encode($data['school_info']));exit;	

		// kprint($data);exit;

		$this->render_page($options);

	}

	public function update(){

		$data = array();	

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);

		$data['school_info'] = $this->Customer->get_school_info();

		$data['ema'] = $this->Customer->get_ema_list();
		
		$data['sales_net_markup'] = $this->Customer->get_sales_net_markup();
        
        $data['javascripts'] = array('school_info');
        $data['css'] = array('school_info');

		$options = array(
			'page'		=> 'school_info/update',
			'params'	=> $data,
			'page_title'=> 'update',
			'main_page' => 'school_info',
            'sub_page'  => 'update'
		);

		// kprint(json_encode($data['school_info']));exit;	

		// kprint($data);exit;

		$this->render_page($options);

	}

	// function in getting ema information

	public function ema_details_by_id(){
		if($this->input->post('ema_id')){
			$data = array();			
			$data['success'] = false;
			$ema_id = 0;
			
			$ema = array();

			if(is_numeric($this->input->post('ema_id'))){
				$ema_id = $this->input->post('ema_id');
			}

			$ema = $this->Customer->get_ema_list_by_id($ema_id);
			$data['ema_id'] = $ema[0]['ema_id'];
			$data['ema_code'] = $ema[0]['ema_code'];
			$data['firstname'] = $ema[0]['firstname'];
			$data['lastname'] = $ema[0]['lastname'];			

			$data['success'] = true;

			echo json_encode($data);

			// kprint($data);exit;
		} else {
			show_404();
		}
		
	}


	// function in getting customer or school information via dropdown event

	public function customer_details_by_id(){        
        if($this->input->post('customer_id')){

            $data = array();
            $data['success'] = false;
            $customer_id = 0;

            $client_list = array();
            
            if(is_numeric($this->input->post('customer_id'))){
				$customer_id = $this->input->post('customer_id');
			}

			$client_list = $this->Customer->get_client_by_id($customer_id);
			$data['customer_id'] = $client_list[0]['customer_id'];
            $data['customer_code'] = $client_list[0]['customer_code'];
            $data['customer_name'] = $client_list[0]['customer_name'];
            $data['customer_address'] = $client_list[0]['customer_address'];            
            $data['remarks'] = $client_list[0]['remarks'];
            $data['success'] = true;

            echo json_encode($data);

        } else {
            show_404();
        }
	}

	// function for adding new school (on single entry)

	public function save_add_school(){
		if($this->input->post()){
			$i = array();
			$id = $this->input->post('ema_id');
			$i['customer_code'] = $this->input->post('school_code');
			$i['customer_name'] = $this->input->post('school_name');
			$i['ema_id'] = $this->input->post('ema_id');
			$i['address'] = $this->input->post('address');
			$i['remarks'] = 0;
			$i['status'] = 1;
			$i['is_deleted'] = 0;

			$this->Customer->insert('client_list',$i);

			$return['success'] = true;

			echo json_encode($return);

		} else {
			show_404();
		}
		
	}

	// function for changing evaluate / process status

	public function save_changes_school_info_by_id(){
		if($this->input->post()){			
			$i = array();
			$id = $this->input->post('customer_id');			
			$i['customer_name'] = $this->input->post('customer_name');
			$i['customer_code'] = $this->input->post('customer_code');
			$i['address'] = $this->input->post('customer_address');
			$i['remarks'] = $this->input->post('evaluate_process_remarks');

			// kprint($i['remarks']);exit;

			$where['id'] = $id;
			$this->Customer->update_where_many('client_list',$i,$where);
			$return['success'] = true;

			echo json_encode($return);
		} else {
			show_404();
		}
		
	}

	

}
