<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mark_up extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();		
		$this->load->helper(array('form','url'));
	}

	public function index(){

		
		date_default_timezone_set('Asia/Manila');

		$data = array();

		$user_id = $this->session->userdata('user_id');

		$data['user']	= $this->Users->get_user_by_id($user_id, TRUE);		

		$data['ema'] = $this->Customer->get_ema_list();	
		
		$data['aicp_item'] = $this->AICP->get_aicp_items();
		$data['aicp_no'] = 'AICP-'.date('Y-md-His',time());

		$data['total_amount'] = $this->AICP->get_total_amount_by_aicp_no($data['aicp_no']);


		$data['javascripts'] = array('mark_up_budget');
		$data['css'] = array('mark_up_budget');


		// kprint($data);exit;

		$options = array(
			'page'		=> 'mark_up/index',
			'params'	=> $data,
			'page_title'=> 'add mark-up budget request',
			'main_page' => 'create_request',
            'sub_page'  => 'mark_up_budget'
		);

		$this->render_page($options);

	}

	public function customer_details_by_id(){
        if($this->input->post('customer_id')){

            $data = array();
            $data['success'] = false;
            $customer_id = 0;

            $client_list = array();

            if(is_numeric($this->input->post('customer_id'))){
				$customer_id = $this->input->post('customer_id');
			}

			$client_list = $this->Customer->get_client_by_id($customer_id);            
			$data['customer_id'] = $customer_id;
            $data['customer_code'] = $client_list[0]['customer_code'];
            $data['customer_name'] = $client_list[0]['customer_name'];
            $data['customer_address'] = $client_list[0]['customer_address'];
            $data['success'] = true;

            // kprint($customer_id);exit;
            echo json_encode($data);

        } else {
            show_404();
        }
    }

	public function client_list_by_ema_id($id = 0){
		if($this->input->post()){

			$data = array();			
			$data['customer'] = $this->Customer->get_client_list_by_ema_id($id);
			$data['success'] = true;			
			echo json_encode($data);

		}else{
			show_404();
		}
	}

	// function of removing an item from the AICP item table

	public function remove_aicp_item($id = 0){
		$where['id'] = $id;		
		$this->AICP->update_where_many('additional_incentives_items',array('is_deleted' => 1),$where);
		$return['success'] = true;
		echo json_encode($return);
	}

	// function in getting EMA information via dropdown event

    public function ema_id(){
        if($this->input->post('ema_id')){

			$data = array();
			$data['success'] = false;
			$ema_id = 0;

			if(is_numeric($this->input->post('ema_id'))){
				$ema_id = $this->input->post('ema_id');
			}

			$ema = $this->Customer->get_ema_list_by_id($ema_id);
			$data['ema_id'] = $ema_id;
			$rsm_id = $ema[0]['rsm_id'];
			$rsm = $this->Users->get_rsm_by_ema_id($ema_id);
			$data['rsm_id'] = $ema[0]['rsm_id'];
			$data['firstname'] = $ema[0]['firstname'];
			$data['lastname'] = $ema[0]['lastname'];
			$data['region_id'] = $ema[0]['region_id'];
			$data['region_name'] = $ema[0]['region_name'];
			$data['rsm_name'] = $rsm['rsm_name'];
			$data['success'] = true;

			$data['school'] = $this->Customer->get_school_by_ema_id($ema_id);

			// $data['int'] = $this->Customer->get_school_by_ema_id_int($ema_id);

			// kprint($data);exit;

			// $data['school'] = $school[0];


			echo json_encode($data);		

		}else{
			show_404();
		}
		
    }

	public function add_aicp(){

		// loading upload library for file uploading function
		$this->load->library('upload');			

		if($this->input->post()){			
			date_default_timezone_set('Asia/Manila');

			$user_id = $this->session->userdata('user_id');
		   
			$return = array();

			$i = array();

			$aicp_no = $this->input->post('aicp_no');
			$total_amount = $this->AICP->get_total_amount_by_aicp_no($aicp_no);
			
            $i['aicp_no'] = $this->input->post('aicp_no');
			$i['ema_id'] = $this->input->post('ema_id');
			$i['school_id'] = $this->input->post('school_id');

			$i['process_status'] = $this->input->post('process_status');				
			
			$i['sa_id'] = $user_id;			
			$i['rsm_id'] = $this->input->post('rsm_id');
			$i['region_id'] = $this->input->post('region_id');
			
			$i['nsm_id'] = 38;
			$i['ar_id'] = 100;

			$i['total_amount'] = $total_amount['total_amount'];
			
			$i['is_one_percent_disc'] = $this->input->post('one_percent_discretionary_fund');

			$i['is_cert_correct'] = 0;
			$i['is_rsm_approved'] = 0;
			$i['is_nsm_approved'] = 0;
			$i['is_approved'] = 0;

			$i['is_check_processed'] = 0;
			
			$i['is_checked'] = 0;
			$i['status'] = 1;
			$i['is_deleted'] = 0;

            // kprint($i['ema_id']);exit;

			$i['status'] = 1;

			if(isset($_FILES['markup_file']) AND $_FILES['markup_file']['size'] > 0){
				$path = './assets/markup_files/';
				$file = 'markup_file';
				$return['upload'] = $this->AICP->upload_markup_file($file, $path);

				if(isset($return['upload']['error'])){
					$return['success'] = false;
					$return['message'] = $return['upload']['error'];

					echo json_encode($return);
					exit;
				}

				$i['markup_file'] = $return['upload']['data']['file_name'];

			} else {
				$i['markup_file'] = '';
			}

						

            $i['is_deleted'] = 0;
            $i['date_created'] = date('Y-m-d H:i:s',time());

			$this->AICP->insert('additional_incentives',$i);
			
			$where['aicp_no'] = $aicp_no;
			$where['is_deleted'] = 0;
            $this->AICP->update_where_many('additional_incentives_items',array('status' => 1),$where);

            $return['success'] = true;
			echo json_encode($return);
						
		} else {
			show_404();
		}
	}

	// deleting an item from sbrf item table

    public function delete_item_by_id($id = 0){
        $where['id'] = $id;
        $this->AICP->update_where_many('additional_incentives_items',array('is_deleted' => 1),$where);
        $return['success'] = true;
        echo json_encode($return);
    }

	public function add_aicp_item(){
		if($this->input->post()){

			$user_id = $this->session->userdata('user_id');
            // date_default_timezone_set('Asia/Manila');

            $i = array();
			$i['aicp_no'] = $this->input->post('aicp_no');
			$i['contact_person'] = $this->input->post('contact_person');
			$i['position'] = $this->input->post('position');
			$i['bank_name'] = $this->input->post('bank_name');
			$i['acct_no'] = $this->input->post('acct_no');
			$i['atm_no'] = $this->input->post('atm_no');
			$i['check_amt'] = $this->input->post('check_amt');
			$i['cash_amt'] = $this->input->post('cash_amt');
			$i['remarks'] = $this->input->post('remarks');
			$i['status'] = 0;
            $i['is_deleted'] = 0;

			$this->AICP->insert('additional_incentives_items',$i);
			
			// kprint($i);exit;

            $return['success'] = true;
			echo json_encode($return);
						
		} else {
			show_404();
		}
	}

	// for mark up budget request approvals

	public function ar_cert_correct_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_cert_correct'] = 1;
		$i['date_corrected'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function rsm_approve_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_rsm_approved'] = 1;
		$i['date_rsm_approval'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function rsm_disapprove_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_rsm_approved'] = 2;
		$i['remarks'] = $this->input->post('remarks');
		$i['date_rsm_approval'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function nsm_approve_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_nsm_approved'] = 1;
		$i['date_nsm_approval'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function nsm_disapprove_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_nsm_approved'] = 2;
		$i['remarks'] = $this->input->post('remarks');
		$i['date_nsm_approval'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function coo_approve_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_approved'] = 1;
		$i['coo_remarks'] = $this->input->post('coo-remarks');
		$i['date_approved'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);		
	}

	public function coo_disapprove_mark_up($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$i['is_approved'] = 2;
		$i['coo_remarks'] = $this->input->post('coo-remarks');
		$i['remarks'] = $this->input->post('remarks');
		$i['date_approved'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;
		$this->AICP->update_where_many('additional_incentives',$i,$where);
		$return['success'] = true;
		echo json_encode($return);			
	}

	public function process_check($aicp_no = 0){
		date_default_timezone_set('Asia/Manila');		
		$i = array();
		$j = array();

		// for updating check status
		
		$i['is_check_processed'] = 1;
		$i['check_no'] = $this->input->post('check_no');
		$i['voucher_no'] = $this->input->post('voucher_no');
		$i['date_check_process'] = date('Y-m-d H:i:s',time());
		$where['aicp_no'] = $aicp_no;

		// for updating cap budget by ema id

		$requested_amount = $this->input->post('requested_amount');
		$current_budget = $this->input->post('remaining_budget');
		$remaining_budget = $current_budget - $requested_amount;
		$j['remaining_budget'] = $remaining_budget;
		
		$where_cap['ema_id'] = $this->input->post('ema_id');
		$where_cap['budget_type'] = $this->input->post('request_type');


		$this->AICP->update_where_many('additional_incentives',$i,$where);

		$this->Cap_budget->update_where_many('cap_budget',$j,$where_cap);
		
		$return['success'] = true;
		echo json_encode($return);
	}

	

}