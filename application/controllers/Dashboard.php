<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct(){
		parent::__construct();
		parent::_verify_user_authentication();
	}

	public function index(){

		$data = array();	

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->Users->get_user_by_id($user_id, TRUE);
		
		$data['mark_up_budget'] = $this->AICP->get_aicp();
		$data['donation'] = $this->SBRF->get_sbrf();
		$data['sbrf'] = $this->SBRF->all_sbrf_pending_requests();

		// data for pending requests

		$data['pending_mark_up_request'] = $this->AICP->all_aicp_pending_requests();		
		$data['pending_incentive_request'] = $this->SBRF->sbrf_pending_requests_by_type(2);
		$data['pending_donation_request'] = $this->SBRF->sbrf_pending_requests_by_type(3);
		$data['pending_seminar_request'] = $this->SBRF->sbrf_pending_requests_by_type(4);

		// data for approved requests

		$data['approved_mark_up_request'] = $this->AICP->all_aicp_approved_requests();		
		$data['approved_incentive_request'] = $this->SBRF->sbrf_approved_requests_by_type(2);
		$data['approved_donation_request'] = $this->SBRF->sbrf_approved_requests_by_type(3);
		$data['approved_seminar_request'] = $this->SBRF->sbrf_approved_requests_by_type(4);

		// data for disapproved requests

		$data['disapproved_mark_up_request'] = $this->AICP->all_aicp_disapproved_requests();		
		$data['disapproved_incentive_request'] = $this->SBRF->sbrf_disapproved_requests_by_type(2);
		$data['disapproved_donation_request'] = $this->SBRF->sbrf_disapproved_requests_by_type(3);
		$data['disapproved_seminar_request'] = $this->SBRF->sbrf_disapproved_requests_by_type(4);
				
		$data['javascripts'] = array('dashboard');

		$options = array(
			'page'		=> 'dashboard/index',
			'params'	=> $data,
			'page_title'=> 'Dashboard',
			'main_page' => 'dashboard',
            'sub_page'  => ''
		);

		$this->render_page($options);

	}

}
