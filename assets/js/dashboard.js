function CDashboard(){
    var lib = this;
    this.bind_events = function(){

        $(document).ready(function(){

            $('.menu .item').tab();
            
            $('.mark_up_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                window.location.href = sBaseURL + 'dashboard/mark_up_budget_request_view/' + sID;
            });

            $('.donation_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                // alert(sID);
                window.location.href = sBaseURL + 'dashboard/donation_request_view/' + sID;
            });

        });      

    }
}