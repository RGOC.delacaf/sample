function CSeminar(){
    var lib = this;
    this.bind_events = function(){
        $(document).ready(function(){

            $('.process-status').dropdown({

            });

            $('.upload_erf_file').click(function(){
                $(this).parent().find('input[type=file]').click();
            });

            $('.erf_file').on('change',this,function(){

                if($(this).val()!=''){
                    var file = ($(this).val()).split("\\").pop();
                    var extension = file.split(".").pop();
                    var filename = file.substring(0, file.indexOf('.'));
    
                    if(filename.length > 10) {
                        filename = filename.substring(0,10) + '..';
                    }

                    output = filename + '.' + extension;

                }

                $('.upload_erf_file').html(output);

            });

            $('.upload_soa_file').click(function(){
                $(this).parent().find('input[type=file]').click();
            });

            $('.soa_file').on('change',this,function(){

                if($(this).val()!=''){
                    var file = ($(this).val()).split("\\").pop();
                    var extension = file.split(".").pop();
                    var filename = file.substring(0, file.indexOf('.'));
    
                    if(filename.length > 10) {
                        filename = filename.substring(0,10) + '..';
                    }

                    output = filename + '.' + extension;

                }

                $('.upload_soa_file').html(output);

            });

            // require function

            function required(uiInputs){
                var bClean = 1;
                for(x in uiInputs){
                    if(uiInputs[x].val() == ''){
                        uiInputs[x].parent().addClass('error');
                        bClean = 0;
                    }else{
                        uiInputs[x].parent().removeClass('error');
                    }
                    
                    if($.isNumeric(uiInputs[2].val()) != true){
                        uiInputs[2].parent().addClass('error');
                        bClean = 0;
                    }else{
                        uiInputs[2].parent().removeClass('error');
                    }
                }

                return bClean;
            }

            // disapprove button for enabling textbox for the remarks and respective actions for disapproval

            $('.confirm-coo-approve').hide();
            $('.confirm-coo-disapprove').hide();
            $('.cancel-coo-approve').hide();
            $('.cancel-coo-disapprove').hide();

            $('.coo-approve').click(function(){
                $(this).hide();
                $('.confirm-coo-approve').show();
                $('.cancel-coo-approve').show();
                $('.coo-disapprove').hide();
            });

            $('.cancel-coo-approve').click(function(){
                $(this).hide();
                $('.confirm-coo-approve').hide();
                $('.coo-approve').show();
                $('.coo-disapprove').show();
            });

            $('.coo-disapprove').click(function(){
                $(this).hide();
                $('.confirm-coo-disapprove').show();
                $('.cancel-coo-disapprove').show();
                $('.coo-approve').hide();
            });

            $('.cancel-coo-disapprove').click(function(){
                $(this).hide();
                $('.confirm-coo-disapprove').hide();
                $('.coo-disapprove').show();
                $('.coo-approve').show();
            });

            $('.disapprove-textarea').click(function(){                
                $('.field.text-remarks').show();
                $('.disapprove-textarea').hide();
                $('.rsm-approve-btn').addClass('disabled');
                $('.nsm-approve-btn').addClass('disabled');
                $('.coo-approve-btn').addClass('disabled');
            });

            $('.cancel-disapprove-btn').click(function(){
                $('.rsm-approve-btn').removeClass('disabled');
                $('.nsm-approve-btn').removeClass('disabled');
                $('.coo-approve-btn').removeClass('disabled');
                $('.field.text-remarks').hide();
                $('.disapprove-textarea').show();
            });

            // datatables

            $('.ui.very.compact.small.celled.table.cap-budget-table').DataTable();

            // cap budget modal

            $('.cap-budget-info-modal').click(function(){
                $('.long.fullscreen.modal.cap-budget-info').modal('show');
            });

            // for check processing

            $('.process-check').click(function(){
                $('.process-check-textfield').show();
                $('.process-check').addClass('disabled');
            });

            $('.cancel-process-check').click(function(){                
                $('.process-check').removeClass('disabled');
                $('.process-check-textfield').hide();
            });

            // confirm check processing

            $('.confirm-process-check').click(function(){
                uiButton = $(this);
                process_check_btn = $('.process-check');                
                cancel_check_process_btn = $('.cancel-process-check');
                sID = $('.four.wide.column.sbrf-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/process_check/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            process_check_btn.hide();
                            cancel_check_process_btn.hide();
                            swal({
                                title: 'Check Processed',
                                text: 'successfully processed a check',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            process_check_btn.hide();
                            cancel_check_process_btn.hide();
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        cancel_check_process_btn.hide();
                        process_check_btn.hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // ui dropdown for ema list with ajax functions

            $('.ui.green.button.save_sbrf_form').addClass('disabled');

            // accounts receivable approval area

            $('.cancel-certify-btn').hide();
            $('.ar-cert-correct-btn').hide();

            $('.certify-btn').click(function(){
                $(this).hide();
                $('.ar-cert-correct-btn').show();
                $('.cancel-certify-btn').show();
            });

            $('.cancel-certify-btn').click(function(){
                $(this).hide();
                $('.ar-cert-correct-btn').hide();
                $('.certify-btn').show();
            });

            $('.ar-cert-correct-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/ar_cert_correct_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.accounts-receivable-label').hide();
                            swal({
                                title: 'Certified Correct',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.accounts-receivable-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // rsm approval area

            $('.rsm-approve-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/rsm_approve_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.rsm-approval-label').hide();
                            swal({
                                title: 'approved and recommended',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.rsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // rsm disapprove area

            $('.rsm-disapprove-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/rsm_disapprove_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.rsm-approval-label').hide();
                            swal({
                                title: 'request disapproved',
                                text: 'you have disapproved a request',
                                type: 'info'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.rsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // nsm approve area

            $('.nsm-approve-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/nsm_approve_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.nsm-approval-label').hide();
                            swal({
                                title: 'approved and recommended',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.nsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // nsm disapprove area

            $('.nsm-disapprove-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/nsm_disapprove_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.nsm-approval-label').hide();
                            swal({
                                title: 'request disapproved',
                                text: 'you have disapproved a request',
                                type: 'info'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.nsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // coo approve area

            $('.confirm-coo-approve').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/coo_approve_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.coo-approval-label').hide();
                            swal({
                                title: 'approved and recommended',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.coo-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // coo disapproce area

            $('.confirm-coo-disapprove').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.sbrf-no').html();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.seminar_request_approvals').ajaxSubmit({
                    url: sBaseURL + 'seminar/coo_disapprove_seminar/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.coo-approval-label').hide();
                            swal({
                                title: 'request disapproved',
                                text: 'you have disapproved a request',
                                type: 'info'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.coo-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            $('.customer_name').dropdown({
                transition: 'scale',
                allowAdditions: true,
                'onChange' : function(value,text,$selectedItem){                    
                    var data = {
                        'customer_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };
                    $.ajax({
                        url: sBaseURL + 'seminar/customer_details_by_id',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            $('.ui.selection.dropdown.customer_name').addClass('loading disabled');                            
                        },
                        success: function(oData){
                            if(oData.success){
                                $('.customer_id').val(oData.customer_id);                                
                                $('.customer_code').val(oData.customer_code);
                                $('.customer_name').val(oData.customer_name);
                                $('.sales_net_markup').val(oData.sales_net_markup);
                                if(oData.remarks == 1){
                                    $('.evaluate_process').val('process');
                                    $('.add_item').removeClass('disabled');
                                } else {
                                    $('.evaluate_process').val('evaluate');
                                    $('.add_item').addClass('disabled');
                                }
                                $('.customer_address').val(oData.customer_address);                                
                            }
                        },
                        error: function(){

                        },
                        complete: function (){
                            $('.ui.selection.dropdown.customer_name').removeClass('loading disabled');
                        }
                    })
                }
            });

            // ema code dropdown

            $('.ema_code').dropdown({
                transition: 'scale',
                allowAdditions: false,
                'onChange' : function(value,text,$selectedItem){                    
                    var data = {
                        'ema_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };
                    $.ajax({
                        url: sBaseURL + 'seminar/ema_details_by_id',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            $('.ui.selection.dropdown.ema_code').addClass('loading disabled');
                        },
                        success: function(oData){
                            if(oData.success){

                                $('.ui.search.selection.dropdown.customer_name')
                                        .find('.menu').empty();

                                $('.ui.search.selection.dropdown.customer_name').dropdown('clear');
                                $('.customer_code').val("");
                                $('.customer_id').val("");
                                $('.customer_address').val("");
                                $('.evaluate_process').val("");

                                $('.ema_id').val(oData.ema_id);                                
                                $('.ema_firstname').val(oData.firstname);
                                $('.ema_lastname').val(oData.lastname);
                                $('.region_id').val(oData.region_id);
                                $('.region_name').val(oData.region_name);
                                $('.rsm_id').val(oData.rsm_id);
                                $('.rsm_name').val(oData.rsm_name);

                                $.each(oData.school, function(id,value){                                    

                                    $('.ui.search.selection.dropdown.customer_name')
                                        .find('.menu')
                                            .append("<div class='item' data-value='" + oData.school[id].customer_id + "'>"+ oData.school[id].customer_name + "</div>");
                                    
                                });
                                
                            }
                        },
                        error: function(){

                        },
                        complete: function (){
                            $('.ui.selection.dropdown.ema_code').removeClass('loading disabled');
                        }
                    })
                }
            });

            // add incentive item into table

            $('.add_seminar_item').click(function(){                               

                uiButton = $(this);
                uiForm = $('.ui.form.add_sbrf');

                sID = $('.sbrf_control_no').val();                
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                var bClean = true;

                var aRequired = [uiForm.find('.payee'), uiForm.find('.designation'), uiForm.find('.seminar'), uiForm.find('.remarks'),uiForm.find('.customer_code'),uiForm.find('.ema_id'),uiForm.find('.customer_id'),uiForm.find('.process_status')];

                bClean = required(aRequired);

                if(bClean){

                    $('.ui.form.add_sbrf').ajaxSubmit({
                        url: sBaseURL + 'seminar/add_sbrf_item',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            uiButton.addClass('loading disabled');
                        },
                        success: function(oData){
                            if(oData.success){                            
                                uiButton.removeClass('loading disabled');                            
                                //supposedly reload the table without reloading the whole page...
                                $('.seminar-sbrf-item-table').load(sBaseURL+'seminar_item_table_view/index/'+sID);
                                // enabling sbrf save button upon adding an item
                                $('.ui.green.button.save_sbrf_form').removeClass('disabled');
    
                                // clearing all fields upon adding item data
    
                                // $('.customer_name').dropdown('clear');
                                $('.payee').val('');
                                $('.designation').val('');
                                $('.seminar').val('');
                                $('.remarks').val('');
                                $('.customer_code').val('');
                                $('.customer_id').val('');

                            } else {
                                uiButton.removeClass('loading disabled');                            
                            }
                        },
                        error: function () {
                            swal('An error occured','Please contact your administrator','error');
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');                        
                        },
                        complete: function () {
                            uiButton.removeClass('loading');
                        }
                    });

                } else {

                }

                
                
            });

            // remove seminar item from table

            $('.ui.red.button.remove_seminar_item').click(function(){
                uiButton = $(this);
                $('.ui.celled.table.seminar-sbrf-item-table').find('input[name="sbrf_item"]').each(function(){                    
                    if($(this).is(":checked")){
                        sID = $(this).parents("div").data("id");                        
                        var data = {
                            value : sID,
                            'sbrf_form_token': $.cookie('sbrf_form_cookie')                            
                        }
                        $(this).parents("tr").remove();
                        $.ajax({
                            url: sBaseURL + 'seminar/delete_item_by_id/' + sID,
                            type: 'post',
                            dataType: 'json',
                            data: data,
                            beforeSend: function () {
                                uiButton.addClass('loading disabled');
                            },
                            success: function(oData){                                
                            },
                            error: function () {                                
                            },
                            complete: function () {
                                uiButton.removeClass('loading disabled');
                            }
                        })
                    }
                });
            });

            // save seminar budget data into sbrf form

            $('.save_sbrf_form').click(function(){
                uiButton = $(this);
                addItemButton = $('.ui.blue.button.add_incentive_item');
                removeItemButton = $('.ui.red.button.remove_incentive_item');
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.add_sbrf').ajaxSubmit({
                    url: sBaseURL + 'seminar/save_seminar_budget_request',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function () {
                        uiButton.addClass('loading disabled');
                    },
                    success: function () {
                        swal('Request Saved!','you have successfully saved a request','success');
                        uiButton.hide();
                        addItemButton.hide();
                        removeItemButton.hide();
                        $('.add_seminar_item').hide();
                        $('.remove_seminar_item').hide();
                    },
                    error: function () {
                        swal('An error occured','Please contact your administrator','error');

                    },
                    complete: function () {
                        uiButton.removeClass('loading disabled');
                    }
                })
                
            });

           
        });
    }
}