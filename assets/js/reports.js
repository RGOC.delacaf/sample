function CReports(){
    var lib = this;
    this.bind_events = function(){

        $(document).ready(function(){           

            $('.ema-details').hide();
            $('.report-request').hide();
            $('.disc-incentive-selection').hide();

            $('.from-date').datetimepicker({
                timepicker: false,
                format: 'Y-m-d'
            });

            $('.to-date').datetimepicker({
                timepicker: false,
                format: 'Y-m-d'
            });
            
            $('.ui.dropdown').dropdown('set selected','0');

            $('.ui.selection.dropdown.reports').dropdown({
                allowAdditions: false,
                transitions: 'scale',
                'onChange': function(value,text,$selectedItem){
                    if(value == 1 || value == 2){
                        $('.disc-incentive-selection').show();
                    }else{
                        $('.disc-incentive-selection').hide();
                    }

                }
            });

            $('.generate-excel').click(function(e){
                var uiButton = $(this);
                var data = {

                    'ema_id'            : $('input[name=ema_id]').val(),
                    'reports'           : $('input[name=reports]').val(),
                    'one_percent_disc'  : $('input[name=one-percent-disc]').val(),
                    'from_date'         : $('input[name=from-date]').val(),
                    'to_date'           : $('input[name=to-date]').val(),
                    'sbrf_form_token'   : $.cookie('sbrf_form_cookie')
                }                
                $.ajax({
                    url: sBaseURL + 'reports/generate_report',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function (){
                        uiButton.addClass('loading disabled');
                    },
                    complete: function (){
                        uiButton.removeClass('loading disabled');
                    },
                    success: function (oData) {
						if(oData.success){
							var link = document.createElement('a');
	                        link.href = oData.url;
	                        link.download = oData.file;
	                        document.body.appendChild(link);
	                        link.click();
						}else{
							//show error
						}
                    }
                });
            });

            $('.ema').dropdown({
                allowAdditions : false,
                transitions: 'scale',
                'onChange': function(value,text){
                    var data = {
                        'ema_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };
                    $.ajax({
                        url: sBaseURL + 'reports/ema_details_by_id',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            $('.ui.selection.dropdown.ema_code').addClass('loading disabled');
                        },
                        success: function(oData){
                            if(oData.success){
                                $('.ema-details').show();
                                $('.report-request').show();
                                $('.ema_id').val(oData.ema_id);                                
                                $('.ema_firstname').val(oData.firstname);
                                $('.ema_lastname').val(oData.lastname);
                                $('.region_id').val(oData.region_id);
                                $('.region_name').val(oData.region_name);
                                $('.rsm_id').val(oData.rsm_id);
                                $('.rsm_name').val(oData.rsm_name);
                            }
                        },
                        error: function(){

                        },
                        complete: function (){
                            $('.ui.selection.dropdown.ema_code').removeClass('loading disabled');
                        }
                    }) 
                }
            });

        });      

    }
}