$(document).ready(function(){

	$('.ui.message .close.icon').click(function(){
		$(this).closest('.ui.message').fadeOut(500);
	});

	$('body').on('mouseover', '.sidebar-all a', function(){
		var uiItem = $(this);
		uiItem.find('.notif').transition('pulse');
	});

	$('.header-settings').click(function(){

		var uiThis = $(this);

		uiThis.find('i.setting.icon').addClass('loading');

		setTimeout(function(){
			uiThis.find('i.setting.icon').removeClass('loading');
		}, 200);
	});

	$('.header-notification').click(function(){

		var uiThis = $(this);

		uiThis.find('.icon-notif').transition({
			animation  : 'pulse',
			duration   : '0.2s'
		});
	});

	$('.header-notification .load-more').click(function(){

		var uiThis = $(this);

		var data = {
			'offset'		 : uiThis.closest('.menu').find('.menu.scrolling .item').length,
			'sbrf_form_token' : $.cookie('sbrf_form_cookie')
		};

		$.ajax({
			url : sBaseURL +'notifications/more',
			type : 'post',
			dataType : 'json',
			data : data,
			beforeSend : function(){
				uiThis.parent().find('.icon.circle').show();
			},
			success : function(oData){
				if(oData.success){

					for(x in oData.notification){
						var uiTile = $('.notification-template').clone();

						if(oData.notification[x]['is_seen']){
							uiTile.addClass('seen')
						}

						uiTile
							.removeClass('notification-template')
							.data('id', oData.notification[x]['id'])
							.find('.ui.feed .event')
								.find('.label img')
									.attr('src', oData.notification[x]['photo'])
									.end()
								.find('.content .summary')
									.find('.notification-title')
										.html(trancateTitle(oData.notification[x]['title']))
										.end()
									.find('.date')
										.html(oData.notification[x]['time_elapsed'])
										.end()
									.end()
								.end()
							.show()
						;

						uiThis.closest('.menu').find('.menu.scrolling').append(uiTile);
					}

					if(oData.notification.length < 10){
						uiThis
							.closest('.menu')
								.addClass('border')
								.find('.load-more')
									.hide()
									.end()
								.end()
						;
					}
				}else{

				}

			},
			error : function(){
				alert('An error occured. Please contact your administrator.')
			},
			complete : function(){
				uiThis.parent().find('.icon.circle').hide();
			}
		});
	});

	function trancateTitle (title) {
		var length = 70;
		if (title.length > length) {
			title = title.substring(0, length)+'...';
		}
		return title;
	}

}).one('click','.sign-out', function(){
	window.location = sBaseURL +'login/logout';
}).one('click','.my-account', function(){
	window.location = sBaseURL +'user';
}).on('click', '.header-notification .menu .item', function(){

	var uiItem = $(this);


	var data = {
		'id' 			 : uiItem.data('id'),
		'sbrf_form_token' : $.cookie('sbrf_form_cookie')
	};

	$('.ui.modal.notifications').modal({
		'transition' : 'horizontal flip',
		'onShow' : function(){

			var uiModal = $(this);
			uiModal.find('.header:not(.message-header)').html(uiItem.find('.notification-title').html());

			$.ajax({
				url : sBaseURL +'notifications/get_notfication',
				type : 'post',
				dataType : 'json',
				data : data,
				beforeSend : function(){
					if(!uiModal.find('.modal-loader').hasClass('active')){
						uiModal.find('.modal-loader').addClass('active');
					}
				},
				success : function(oData){
					if(oData.success){
						uiModal.find('.content .ui.message').hide();
						uiModal.find('.content .description').show();

						uiModal.find('.content .message:not(.ui.negative)').html(oData.notification[0].message);
						uiModal.modal('refresh');

						if(!uiItem.hasClass('seen')){
							uiItem.addClass('seen');

							var iNotifCount = parseInt($('.header-notification').find('.icon-notif .ui.circular.label').html());

							if( (iNotifCount-1) > 0 ){
								$('.header-notification').find('.icon-notif .ui.circular.label').html(iNotifCount-1);
							}else{
								$('.header-notification').find('.icon-notif .ui.circular.label').hide();
							}
						}
					}else{
						uiModal.find('.content .ui.message').show();
						uiModal.find('.content .description').hide();
					}

				},
				error : function(){
					uiModal.find('.content .ui.message').show();
					uiModal.find('.content .description').hide();
				},
				complete : function(){
					uiModal.find('.modal-loader').removeClass('active');
				}
			});


		}
	}).modal('show');



});
