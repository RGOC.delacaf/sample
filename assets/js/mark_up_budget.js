function CMU_Budget(){
    var lib = this;
    this.bind_events = function(){
        $(document).ready(function(){
            

            $('.process-status').dropdown({

            });

            $('.upload_markup_file').click(function(){                
                $(this).parent().find('input[type=file]').click();
            });
            

            $('.markup_file').on('change',this,function(){

                if($(this).val()!=''){
                    var file = ($(this).val()).split("\\").pop();
                    var extension = file.split(".").pop();
                    var filename = file.substring(0, file.indexOf('.'));
    
                    if(filename.length > 10) {
                        filename = filename.substring(0,10) + '..';
                    }

                    output = filename + '.' + extension;

                }

                $('.upload_markup_file').html(output);

            });            
            

            // $('.add_ema_to_aicp').click(function(){
            //     var iID = $(this).closest('.form').find("input[name='ema_id']").val();
            //     window.location.href = sBaseURL + 'mark_up/go_to_aicp_request_form/' + iID;
            // });

            // loading values for cap budget computation

            // var ema_id = $('.ema_id').text();
            // var request_type = $('.request_type').text();
            // var remaining_budget = $('td.remaining_budget').text();
            // var requested_amount = $('td.grand_total').text();

            // $('.ema_id').val(ema_id);
            // $('.request_type').val(request_type);
            // $('.remaining_budget').val(remaining_budget);
            // $('.requested_amount').val(requested_amount);

            
            // enable upload button for ps_file when 1% disc incentive is checked 
            // update: does not require to tag 1% to mark up 

            // $('.ui.button.upload_ps_file').hide();

            // $('.one_percent').checkbox({
            //     onChecked : function(){                    
            //         $('.ui.button.upload_ps_file').show();
            //     },
            //     onUnchecked : function(){                    
            //         $('.ui.button.upload_ps_file').hide();
            //     }
            // })

            // require function

            function required(uiInputs){
                var bClean = 1;               

                for(x in uiInputs){
                    if(uiInputs[x].val().trim() == ''){
                        uiInputs[x].parent().addClass('error');
                        bClean = 0;
                    }else{
                        uiInputs[x].parent().removeClass('error');
                    }
                    
                    if($.isNumeric(uiInputs[2].val()) != true){
                        uiInputs[2].parent().addClass('error');
                        bClean = 0;
                    }else{
                        uiInputs[2].parent().removeClass('error');
                    }
                }                

                return bClean;
            }

            // datatables

            $('.ui.very.compact.small.celled.table.cap-budget-table').DataTable();            

            // cap budget modal

            $('.cap-budget-info-modal').click(function(){
                $('.long.fullscreen.modal.cap-budget-info').modal('show');
            });
            
             // for check processing

             $('.process-check').click(function(){
                $('.process-check-textfield').show();                
                $('.process-check').addClass('disabled');
            });

            $('.cancel-process-check').click(function(){                
                $('.process-check').removeClass('disabled');
                $('.process-check-textfield').hide();
            });

            // confirm check processing

            $('.confirm-process-check').click(function(){
                uiButton = $(this);
                process_check_btn = $('.process-check');                
                cancel_check_process_btn = $('.cancel-process-check');
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/process_check/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            process_check_btn.hide();
                            cancel_check_process_btn.hide();
                            swal({
                                title: 'Check Processed',
                                text: 'successfully processed a check',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            process_check_btn.hide();
                            cancel_check_process_btn.hide();
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        cancel_check_process_btn.hide();
                        process_check_btn.hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // disable submit form button

            $('.ui.green.button.submit-aicp').addClass('disabled');

            // accounts receivable approval area

            $('.cancel-certify-btn').hide();
            $('.ar-cert-correct-btn').hide();

            $('.certify-btn').click(function(){
                $(this).hide();
                $('.ar-cert-correct-btn').show();
                $('.cancel-certify-btn').show();
            });

            $('.cancel-certify-btn').click(function(){
                $(this).hide();
                $('.ar-cert-correct-btn').hide();
                $('.certify-btn').show();
            });

            $('.ar-cert-correct-btn').click(function(){
                uiButton = $(this);
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/ar_cert_correct_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            $('.accounts-receivable-label').hide();
                            swal({
                                title: 'Certified Correct',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.accounts-receivable-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // rsm approve click function

            $('.rsm-approve-btn').click(function(){
                uiButton = $(this);
                disapproveBtn = $('.rsm-disapprove-btn');
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/rsm_approve_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');                            
                            uiButton.hide();
                            disapproveBtn.hide();
                            $('.rsm-approval-label').hide();
                            swal({
                                title: 'Approved and Noted',
                                text: 'successfully approve a request',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });                            
                            // swal('Request Noted!','you have successfully approve a request','success');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        disapproveBtn.hide();
                        $('.rsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // rsm disapprove click function

            $('.rsm-disapprove-btn').click(function(){
                uiButton = $(this);
                approveBtn = $('.rsm-approve-btn');
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/rsm_disapprove_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            approveBtn.hide();
                            $('.cancel-disapprove-btn').hide();
                            $('.rsm-approval-label').hide();  
                            swal('Request Disapproved','you have disapproved a request','info');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        approveBtn.hide();
                        $('.rsm-approval-label').hide();                        
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // nsm approval click function

            $('.nsm-approve-btn').click(function(){
                uiButton = $(this);
                disapproveBtn = $('.nsm-disapprove-btn');
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/nsm_approve_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');                            
                            uiButton.hide();
                            disapproveBtn.hide();
                            $('.nsm-approval-label').hide();
                            swal({
                                title: 'Approved and Noted',
                                text: 'you have successfully approve a request',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                            // swal('Request Noted!','you have successfully approve a request','success');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        disapproveBtn.hide();
                        $('.nsm-approval-label').hide();
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // nsm disapprove click function

            $('.nsm-disapprove-btn').click(function(){
                uiButton = $(this);
                approveBtn = $('.nsm-approve-btn');
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/nsm_disapprove_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            approveBtn.hide();
                            $('.cancel-disapprove-btn').hide();
                            $('.nsm-approval-label').hide();  
                            swal('Request Disapproved','you have disapproved a request','info');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        approveBtn.hide();
                        $('.nsm-approval-label').hide();                        
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });


            // coo approve button click function

            $('.confirm-coo-approve').click(function(){
                uiButton = $(this);
                disapproveBtn = $('.coo-disapprove-btn');                
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/coo_approve_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            disapproveBtn.hide();
                            $('.coo-approval-label').hide();
                            swal({
                                title: 'Request Approved!',
                                text: 'you have successfully approved a request',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                            // swal('Request Approved!','you have successfully approved a request','success');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        $('.coo-approval-label').hide();                        
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });            
            
            // coo disapprove button click function

            $('.confirm-coo-disapprove').click(function(){
                uiButton = $(this);
                approveBtn = $('.coo-approve-btn');                
                sID = $('.four.wide.column.aicp-no').html();
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.approval-form').ajaxSubmit({
                    url: sBaseURL + 'mark_up/coo_disapprove_mark_up/' + sID,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            approveBtn.hide();
                            $('.cancel-disapprove-btn').hide();
                            $('.coo-approval-label').hide();  
                            swal('Request Disapproved','you have disapproved a request','info');
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        approveBtn.hide();
                        $('.coo-approval-label').hide();                        
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });            

            // disapprove button for enabling textbox for the remarks and respective actions for disapproval

            $('.confirm-coo-approve').hide();
            $('.confirm-coo-disapprove').hide();
            $('.cancel-coo-approve').hide();
            $('.cancel-coo-disapprove').hide();

            $('.coo-approve').click(function(){
                $(this).hide();
                $('.confirm-coo-approve').show();
                $('.cancel-coo-approve').show();
                $('.coo-disapprove').hide();
            });

            $('.cancel-coo-approve').click(function(){
                $(this).hide();
                $('.confirm-coo-approve').hide();
                $('.coo-approve').show();
                $('.coo-disapprove').show();
            });

            $('.coo-disapprove').click(function(){
                $(this).hide();
                $('.confirm-coo-disapprove').show();
                $('.cancel-coo-disapprove').show();
                $('.coo-approve').hide();
            });

            $('.cancel-coo-disapprove').click(function(){
                $(this).hide();
                $('.confirm-coo-disapprove').hide();
                $('.coo-disapprove').show();
                $('.coo-approve').show();
            });

            $('.disapprove-textarea').click(function(){                
                $('.field.text-remarks').show();
                $('.disapprove-textarea').hide();
                $('.rsm-approve-btn').addClass('disabled');
                $('.nsm-approve-btn').addClass('disabled');
                $('.coo-approve-btn').addClass('disabled');
            });

            $('.cancel-disapprove-btn').click(function(){
                $('.rsm-approve-btn').removeClass('disabled');
                $('.nsm-approve-btn').removeClass('disabled');
                $('.coo-approve-btn').removeClass('disabled');
                $('.field.text-remarks').hide();
                $('.disapprove-textarea').show();
            });          


            // submit aicp form

            $('.submit-aicp').click(function(){
                uiButton = $(this);
                uiAddItemButton = $('.add_aicp_item');
                uiRemoveItemButton = $('.remove_aicp_item');
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.add-aicp').ajaxSubmit({
                    url: sBaseURL + 'mark_up/add_aicp',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');
                    },
                    success: function(oData){
                        if(oData.success){
                            swal('Request Saved!','you have successfully saved a request','success');
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.hide();
                            uiAddItemButton.hide();
                            uiRemoveItemButton.hide();
                            $('.upload_ps_file').hide();
                            $('.add_aicp_item').hide();
                            $('.remove_aicp_item').hide();
                        } else {
                            $('.upload_ps_file').addClass('error');
                            swal('file upload error','.PNG, .JPG, .GIF, .PDF, .XLS/XLSX, .DOC/DOCX are only allowed','error');
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {
                        swal('An error occured','Please contact your administrator','error');
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.hide();
                        uiAddItemButton.hide();
                        uiRemoveItemButton.hide();
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });
            });

            // adding ema to aicp

            $('.add_ema_to_aicp').click(function(){
                uiButton  = $(this);
                sEMAID = $('.ema_id').val();
                uiDropdown = $('.ui.dropdown.add-ema');
                uiTextBox = $('.school_name');
                if(sEMAID != ''){
                    uiDropdown.addClass('disabled');
                    uiTextBox.addClass('readonly');
                    uiButton.hide();
                    $('.ui.form.add-aicp-item').show();
                }
                
            });

            // adding an item into aicp

            $('.add_aicp_item').click(function(){
                uiButton = $(this);
                uiForm = $('.ui.form.add-aicp');

                sID = $('.aicp_no').val();               
                
                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                var bClean = true;

                var aRequired = [uiForm.find('.contact_person'), uiForm.find('.position'), uiForm.find('.check_amt'), uiForm.find('.remarks'),uiForm.find('.school_code'),uiForm.find('.ema_id'),uiForm.find('.process_status')];

                bClean = required(aRequired);

                if(bClean){
                    $('.ui.form.add-aicp').ajaxSubmit({
                        url: sBaseURL + 'mark_up/add_aicp_item',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            uiButton.addClass('loading disabled');
                        },
                        success: function(oData){
                            if(oData.success){
                                // swal('Request Saved!','you have successfully saved a request','success');
                                // $("html, body").animate({ scrollTop: 0 }, 600);
    
                                uiButton.removeClass('loading disabled');
                                
                                //supposedly reload the table without reloading the whole page...
                                $('.aicp-item-table').load(sBaseURL+'aicp_item_table_view/index/'+sID);
                                
                                // enable save form button once a item is being added
                                $('.ui.green.button.submit-aicp').removeClass('disabled');
    
                                // clearing all textbox items from the item form
    
                                $('.contact_person').val('');
                                $('.position').val('');
                                $('.bank_name').val('');
                                $('.acct_no').val('');
                                $('.atm_no').val('');
                                $('.check_amt').val('');
                                $('.cash_amt').val('');
                                $('.remarks').val('');
    
                            } else {
                                uiButton.removeClass('loading disabled');                            
                            }
                        },
                        error: function () {
                            swal('An error occured','Please contact your administrator','error');
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');                        
                        },
                        complete: function () {
                            uiButton.removeClass('loading');
                        }
                    });

                }else{
                    
                }
                
                

            });

            // dropdown selection list of customers / school

            $('.ui.selection.dropdown.school_name').dropdown({
                allowAdditions: true,
                'onChange' : function(value,text,$selectedItem){                 
                    
                    var data = {
                        'customer_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };

                    $.ajax({
                        url: sBaseURL + 'mark_up/customer_details_by_id',
                        type : 'post',
                        dataType : 'json',
                        data : data,
                        beforeSend : function(){                            
                            $('.ui.selection.dropdown.school_id').addClass('disabled loading');                            
                        },
                        success : function(oData){
                            if(oData.success){
                                // $('.ema-controls').show();
                                // $('.ema_firstname').val(oData.firstname);
                                // $('.ema_lastname').val(oData.lastname);    
                                // $('.aicp_no').val(oData.aicp_no);
                                $('.school_id').val(oData.customer_id);
                                $('.school_code').val(oData.customer_code);
                                $('.school_add').val(oData.customer_address);                               
                            }                            
                        },
                        error : function(){

                        },
                        complete : function(){
                            $('.ui.selection.dropdown.school_name').removeClass('disabled loading');
                        }
                    })
                }                
            });

            // dropdown selection list of ema

            $('.ui.selection.dropdown.add-ema').dropdown({
                transition: 'scale',
                allowAdditions : true,
                'onChange' : function(value,text,$selectedItem){
                    var data = {
                        'ema_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };
                    $.ajax({
                        url: sBaseURL + 'mark_up/ema_id',
                        type : 'post',
                        dataType : 'json',
                        data : data,
                        beforeSend : function(){
                            $('.ui.selection.dropdown.add-ema').addClass('loading disabled');                            
                        },
                        success : function(oData){
                            if(oData.success){

                                $('.ui.search.selection.dropdown.school_name')
                                        .find('.menu').empty();

                                $('.ui.search.selection.dropdown.school_name').dropdown('clear');
                                $('.school_code').val("");
                                $('.school_add').val("");


                                $('.ema-controls').show();
                                $('.ema_id').val(oData.ema_id);    
                                $('.ema_firstname').val(oData.firstname);
                                $('.ema_lastname').val(oData.lastname);
                                $('.region_id').val(oData.region_id);
                                $('.region_name').val(oData.region_name);
                                $('.rsm_id').val(oData.rsm_id);
                                $('.rsm_name').val(oData.rsm_name);

                                // if(oData.school.length != 0){
                                    
                                $.each(oData.school, function(id,value){                                    

                                    $('.ui.search.selection.dropdown.school_name')
                                        .find('.menu')
                                            .append("<div class='item' data-value='" + oData.school[id].customer_id + "'>"+ oData.school[id].customer_name + "</div>");
                                    
                                });

                                // }


                                // for (var i = 0; i < oData.school.length; i++){
                                //     oData.school[i]
                                //     $('school_id').find('.menu.school').append(item);
                                // }


                                // for(var i=0;i<oData.length;i++){
                                //     $.each(oData.school[i], function(index,value){
                                //     var item = $('.item.school_list');
                                //     item.val(index);
                                //     item.text(value);
                                //     $('.school_id').find('.menu.school').append(item);
                                //     });
                                // }

                                // $('.aicp_no').val(oData.aicp_no);
                                // $('add-school').val(oData.customer);
                                // $('.school-id').load(sBaseURL+'client_list_view/index/'+sID);                               
                            }                           

                        },
                        error : function(){

                        },
                        complete : function(){
                            $('.ui.selection.dropdown.add-ema').removeClass('loading disabled');
                        }
                    })
                }               
                
            });

            // removing an item from the table

            $('.ui.red.button.remove_aicp_item').click(function(){
                uiButton = $(this);
                $('.ui.celled.table.aicp-item-table').find('input[name="aicp_item"]').each(function(){                    
                    if($(this).is(":checked")){
                        sID = $(this).parents("div").data("id");                        
                        var data = {
                            value : sID,
                            'sbrf_form_token': $.cookie('sbrf_form_cookie')                            
                        }
                        $(this).parents("tr").remove();
                        $.ajax({
                            url: sBaseURL + 'mark_up/delete_item_by_id/' + sID,
                            type: 'post',
                            dataType: 'json',
                            data: data,
                            beforeSend: function () {
                                uiButton.addClass('loading disabled');
                            },
                            success: function(oData){
                                // alert('item deleted');
                            },
                            error: function () {
                                alert('error on delete item');
                            },
                            complete: function () {
                                uiButton.removeClass('loading disabled');                                
                            }
                        })
                    }
                });
            });

            // i forgot the purpose of this one and what did i write it for

        	$('.ui.selection.dropdown.add-request').dropdown({
                allowAdditions : true,
                'onChange' : function(value,text,$selectedItem){
                    var data = {
                        'customer_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };
                    $.ajax({
                        url: sBaseURL + 'mark_up/customer_info',
                        type : 'post',
                        dataType : 'json',
                        data : data,
                        beforeSend : function(){
                            $('.ui.selection.dropdown.add-request').addClass('.active.loader');
                        },
                        success : function(oData){
                            if(oData.success){
                                $('.customer-controls').show();

                                if(typeof oData.customer != "undefined"){
                                    $('.ui.form .customer_address').val(oData.customer['address']);
                                }
                            }
                        },
                        error : function(){

                        },
                        complete : function(){
                            $('.ui.selection.dropdown.add-request').removeClass('loading');
                        }
                    })
                }

            });
            
        });
    }
}