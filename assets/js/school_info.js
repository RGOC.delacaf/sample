function CSchool_info(){
    var lib = this;
    this.bind_events = function(){

        $(document).ready(function(){

            $('.save-changes').addClass('disabled');
            $('.add_school_save').addClass('disabled');
            $('.ui.selection.dropdown.evaluate_process_selection').addClass('disabled');

            // $('.ui.red.button.add_school_save').addClass('disabled');

            $('.ui.red.button.add_school_save').click(function(){
                uiButton = $(this);
                var data = {
                    'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                };
                $('.ui.form.add_school').ajaxSubmit({
                    url : sBaseURL + 'school_info/save_add_school',
                    type : 'post',
                    dataType : 'json',
                    data : data,
                    beforeSend : function(){
                        uiButton.addClass('loading disabled');
                    },
                    success : function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            
                            swal({
                                title: 'saved info',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }

                    },
                    error : function(){
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        
                        swal('An error occured','Please contact your administrator','error');

                    },
                    complete : function(){
                        uiButton.removeClass('loading');

                    }

                })
            });

            // dropdown selection for ema

            $('.ema').dropdown({
                allowAdditions : false,
                transition : 'scale',
                'onChange' : function(value,text,$selectedItem){
                    var data = {
                        'ema_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };                    

                    $.ajax({
                        url : sBaseURL + 'school_info/ema_details_by_id',
                        type : 'post',
                        dataType: 'json',
                        data : data,
                        beforeSend : function(){
                            $('.add_school_save').removeClass('disabled');
                        },
                        success: function(oData){
                            
                            if(oData.success){
                                $('.ema_id').val(oData.ema_id);
                                $('.firstname').val(oData.firstname);
                                $('.lastname').val(oData.lastname);
                            }
                            
                        },
                        error: function(){                            

                        },
                        complete: function(){
                            
                        }
                    })

                }
            })
            
            // dropdown selection for school

            $('.school').dropdown({
                allowAdditions : false,
                transition : 'scale',
                'onChange' : function(value,text,$selectedItem){
                    var data = {
                        'customer_id' : value,
                        'sbrf_form_token' : $.cookie('sbrf_form_cookie')
                    };

                    $('.save-changes').removeClass('disabled');
                    $('.ui.selection.dropdown.evaluate_process_selection').removeClass('disabled');

                    $.ajax({
                        url: sBaseURL + 'school_info/customer_details_by_id',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function(){
                            
                        },
                        success: function(oData){
                            if(oData.success){
                                $('.customer_id').val(oData.customer_id);
                                $('.customer_code').val(oData.customer_code);
                                $('.customer_name').val(oData.customer_name);
                                $('.sales_net_markup').val(oData.sales_net_markup);
                                if(oData.remarks == 1){
                                    $('.evaluate_process').val('process');
                                    $('.evaluate_process_selection').val('process')
                                    
                                } else {
                                    $('.evaluate_process').val('evaluate');
                                    $('.evaluate_process_remarks').val('0');                                    
                                }
                                $('.customer_address').val(oData.customer_address);                                
                            }
                        },
                        error: function(){

                        },
                        complete: function (){
                            
                        }

                    })
                }
            });

            $('.ui.selection.dropdown.evaluate_process_selection').dropdown({
                allowAdditions : false,
                transition : 'scale'
            });

            $('.save-changes').click(function(){
                uiButton = $(this);

                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }

                $('.ui.form.school_info').ajaxSubmit({
                    url: sBaseURL + 'school_info/save_changes_school_info_by_id',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(){
                        uiButton.addClass('loading disabled');                        
                    },
                    success: function(oData){
                        if(oData.success){
                            $("html, body").animate({ scrollTop: 0 }, 600);
                            uiButton.removeClass('loading disabled');
                            uiButton.hide();
                            
                            swal({
                                title: 'saved info',
                                text: 'successfully processed an action',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            uiButton.removeClass('loading disabled');
                        }
                    },
                    error: function () {                                                
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        uiButton.removeClass('loading disabled');
                        uiButton.hide();
                        
                        swal('An error occured','Please contact your administrator','error');
                    },
                    complete: function () {
                        uiButton.removeClass('loading');
                    }
                });

            });
            
            

            
        });      

    }
}