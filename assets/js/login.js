$(document).ready(function(){
    
    $('body').on('keypress', function(e){
        if(e.which == 13){
            login($('.login-btn.button'));
        }
    });

    $('.login-btn.button').click(function(e){
        e.preventDefault();
        login($(this));
        return false;
    });

    $('.message .close').on('click', function(){
        $(this).closest('.message').transition('fade');
    });

    $('.login .negative.message').hide();

    function login(button){
        var data = {};

        data.username = $('.login input[name="username"]').val();
        data.password = $('.login input[name="password"]').val();
        data.sbrf_form_token = $.cookie('sbrf_form_cookie');

        $.ajax({
            url :'login/check_user_login',
            type : 'post',
            dataType : 'json',
            data : data,
            beforeSend : function(){
                button.addClass('disabled loading');
            },
            success : function(oData){
                if(oData.success){
                    if(oData.referrer != ''){
                        window.location = oData.referrer;
                    }else{
                        window.location = sBaseURL +"dashboard";
                    }

                }else{
                    $('.login .negative.message').removeClass('hidden');
                    $('.login .negative.message .header').html(oData.message);
                    $('.login .negative.message').show();
                }

            },
            error : function(){
                //alert('An error occured. Please contact your administrator.')
            },
            complete : function(){
                button.removeClass('disabled loading');
            }
        });

    }
});
    