function CPending_request(){
    var lib = this;
    this.bind_events = function(){

        $(document).ready(function(){

            $('.menu .item').tab();
            
            $('.incentive_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                window.location.href = sBaseURL + 'pending_request/incentive_request_view/' + sID;
            });

            $('.mark_up_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                window.location.href = sBaseURL + 'pending_request/mark_up_budget_request_view/' + sID;
            });

            $('.donation_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                // alert(sID);
                window.location.href = sBaseURL + 'pending_request/donation_request_view/' + sID;
            });

            $('.seminar_view_details').click(function(){
                uiButton = $(this);
                sID = uiButton.data('id');
                // alert(sID);
                window.location.href = sBaseURL + 'pending_request/seminar_request_view/' + sID;
            });

        });      

    }
}