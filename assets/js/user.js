function CUser(){
    var lib = this;
    this.bind_events = function(){

        $(document).ready(function(){

            function required(uiInputs){
                var bClean = 1;
                for(x in uiInputs){
                    if(uiInputs[x].val().trim() == ''){
                        uiInputs[x].parent().addClass('error');
                        bClean = 0;
                    }else{
                        uiInputs[x].parent().removeClass('error');
                    }
                }

                return bClean;
            }

            $('.ui.negative.message').hide();
            $('.ui.positive.message').hide();
            
            $('.ui.button.save_password').click(function(){
                uiButton = $(this);
                uiForm = $('.ui.form.user_info');                

                var data = {
                    'sbrf_form_token': $.cookie('sbrf_form_cookie')
                }
                
                var bClean = true;
                var errors = [];

                var aRequired = [uiForm.find('.old_password'), uiForm.find('.new_password'), uiForm.find('.confirm_new_password')];

                // bClean = TRUE, if all data in fields are present

                bClean = required(aRequired);

                if(!bClean){
                    errors.push("all fields required");
                }

                if(uiForm.find('.new_password').val() != uiForm.find('.confirm_new_password').val()){
                    bClean = false;
                    errors.push("password not match");
                    uiForm.find('.new_password').addClass('error');
                    uiForm.find('.confirm_new_password').addClass('error');
                }

                if(bClean){
                    $('.ui.form.user_info').ajaxSubmit({
                        url: sBaseURL + 'user/save_info',
                        type: 'post',
                        dataType: 'json',
                        data: data,
                        beforeSend: function () {
                            uiButton.addClass('loading disabled');
                        },
                        success: function (oData) {
                            if (oData.success){
                                uiForm.find('.old_password').val('');
                                uiForm.find('.new_password').val('');
                                uiForm.find('.negative.message').hide();
                                uiForm.find('.confirm_new_password').val('');swal('password changed','','info');


                            } else {
                                uiForm.find('.postive.message').hide();
                                uiForm.find('.negative.message').show().find('.header').html(oData.message)
                                uiForm.find('.negative.message .list').html('');

                            }
                            // swal('password changed','user password has changed','success');
                            // alert("success");
                            // uiButton.hide();
                        },
                        error: function () {
                            // swal('An error occured','Please contact your administrator','error');                            
                        },
                        complete: function () {
                            uiButton.removeClass('loading disabled');
                        }
                    })
                } else {
                    uiForm.find('.ui.negative.message').addClass('error').show().find('.header').html('error as occured');
                    $('.user_form .negative.message .list').html('');
                    for(x in errors){
                        var uiTile = $('.li-template li').clone();
                        uiTile.html(errors[x]);
                        $('.list').append(uiTile);
                    }

                    $('.ui.form.user_info .negative.message').show();
                }

                


                

            });
            

        });      

    }
}